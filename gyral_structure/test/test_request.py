from .. import request
import scipy as sp
from .. import basis
from numpy.testing import assert_allclose


def test_sum_split():
    pos1 = sp.rand(100, 3)
    for nsplit in (1, 2, 4):
        for req in (
            request.PositionRequest(pos1),
            request.VoxelRequest(pos1, size=sp.rand(100), nsplit=nsplit),
            request.VertexRequest(sp.randn(100, 3, 3), nsplit=nsplit),
        ):
            assert_allclose(sp.sum([var for _, var in req.split()], 0), 1)
    for nder in [
        (0, 0, 1),
        (1, 0, 0),
        (2, 0, 0),
        (1, 1, 0),
        (1, 1, 3),
    ]:
        assert_allclose(sp.sum([var for _, var in request.DerivativeRequest(pos1, nder).split()], 0), 0)


def test_boundingbox():
    pos1 = sp.rand(1000, 3)

    preq = request.PositionRequest(pos1)
    assert preq.ndim == 3

    assert (preq.bounding_box()[:, 0] >= 0).all()
    assert (preq.bounding_box()[:, 1] <= 1).all()

    assert (preq.bounding_box()[:, 0] == pos1.min(0)).all()
    assert (preq.bounding_box()[:, 1] == pos1.max(0)).all()

    assert (preq.bounding_box(extent=1)[:, 0] == pos1.min(0) - 1).all()
    assert (preq.bounding_box(extent=1)[:, 1] == pos1.max(0) + 1).all()

    size = sp.rand(1000)
    vreq = request.VoxelRequest(pos1, size=size)
    assert (vreq.bounding_box()[:, 0] >= -0.5).all()
    assert (vreq.bounding_box()[:, 1] <= 1.5).all()

    assert (vreq.bounding_box()[:, 0] == (pos1 - size[:, None] / 2).min(0)).all()
    assert (vreq.bounding_box()[:, 1] == (pos1 + size[:, None] / 2).max(0)).all()

    assert (vreq.bounding_box(extent=1)[:, 0] == (pos1 - size[:, None] / 2).min(0) - 1).all()
    assert (vreq.bounding_box(extent=1)[:, 1] == (pos1 + size[:, None] / 2).max(0) + 1).all()

    size = sp.rand(3)
    vreq = request.VoxelRequest(pos1, size=size)
    assert (vreq.bounding_box()[:, 0] >= -0.5).all()
    assert (vreq.bounding_box()[:, 1] <= 1.5).all()

    assert (vreq.bounding_box()[:, 0] == pos1.min(0) - size / 2).all()
    assert (vreq.bounding_box()[:, 1] == pos1.max(0) + size / 2).all()

    assert (vreq.bounding_box(extent=1)[:, 0] == pos1.min(0) - size / 2 - 1).all()
    assert (vreq.bounding_box(extent=1)[:, 1] == pos1.max(0) + size / 2 + 1).all()

    triang = sp.randn(1000, 2, 3)
    treq = request.VertexRequest(triang)
    assert treq.ndim == 2

    triang = sp.randn(1000, 3, 3)
    treq = request.VertexRequest(triang)
    assert treq.ndim == 3
    assert (treq.bounding_box(extent=1)[:, 0] == triang.min((0, 2)) - 1).all()
    assert (treq.bounding_box(extent=1)[:, 1] == triang.max((0, 2)) + 1).all()

    preq = request.PositionRequest(pos1)
    mreq = request.MultRequest([request.PositionRequest(pos1[ix::3]) for ix in range(3)])
    assert (mreq.bounding_box() == preq.bounding_box()).all()
    assert (mreq.bounding_box(extent=0.42) == preq.bounding_box(extent=0.42)).all()


def test_radius_center():
    sp.random.seed(1234)
    other_points = sp.array([[-100, 0, 0], [0, 0, 0]])
    pos1 = sp.rand(1000, 3)
    req = request.PositionRequest(pos1)
    assert (req.radius() == 0).all()
    assert (req.center() == pos1).all()
    min_dist = req.min_dist(other_points)
    print(min_dist)
    assert min_dist[0] > 100.
    assert min_dist[0] < 101.
    assert min_dist[1] > 0
    assert min_dist[1] < 1.

    req = request.VoxelRequest(pos1, size=37., nsplit=5)
    assert (req.radius() == 37 * sp.sqrt(3) / 2).all()
    assert (req.center() == pos1).all()
    min_dist = req.min_dist(other_points)
    print(min_dist)
    assert min_dist[0] > 100 - 37. / 2.
    assert min_dist[0] < 95.
    assert min_dist[1] < 1

    sz = sp.rand(1000)
    req = request.VoxelRequest(pos1, size=sz)
    assert (req.radius() == sz * sp.sqrt(3) / 2).all()
    assert (req.center() == pos1).all()

    sz = sp.rand(1000, 3)
    req = request.VoxelRequest(pos1, size=sz)
    assert (req.radius() == sp.sqrt((sz ** 2).sum(-1)) / 2).all()
    assert (req.center() == pos1).all()

    req = request.DerivativeRequest(pos1, (1, 0, 0), epsilon=1e-3)
    assert (req.radius() == 1e-3).all()
    assert (req.center() == pos1).all()

    tria = sp.rand(1000, 3, 3)
    req = request.VertexRequest(tria)
    assert (req.center() == tria.mean(-1)).all()
    assert req.radius() <= sp.sqrt(3) / 2

    tria = sp.zeros((1, 3, 3))
    assert request.VertexRequest(tria).radius() == 0.
    tria[0, 1:, :] = 1.
    assert request.VertexRequest(tria).radius() == 0.
    tria[0, :, 0] = 1.
    assert_allclose(request.VertexRequest(tria).radius(), 2/3.)

    req = request.MultRequest([request.PositionRequest(pos1),
                               request.VoxelRequest(pos1 * 2, size=37., nsplit=5)])
    assert (req.center() == sp.append(pos1, pos1 * 2, 0)).all()
    assert (req.radius() <= sp.append(sp.zeros(1000), sp.zeros(1000) + 37.)).all()
    min_dist_alt = req.min_dist(other_points)
    print(min_dist - min_dist_alt)
    assert abs(min_dist - min_dist_alt).max() < 2e-2


def test_subdiv():
    pos = sp.rand(100, 3)
    tria = sp.rand(100, 3, 3)
    sz = sp.rand(100, 3)
    bf = basis.Polynomial(3)
    params = sp.randn(bf.nparams)
    for req in [request.PositionRequest(pos), request.VoxelRequest(pos, sz, nsplit=1),
                request.VertexRequest(tria, nsplit=1), request.DerivativeRequest(pos, (1, 0, 0), epsilon=1)]:
        for select in [sp.rand(100) > 0.5, slice(10, 50, 3), [1, 7, 3, 8]]:
            arr1 = req.get_evaluator(bf)(params)[select]
            arr2 = req[select].get_evaluator(bf)(params)
            print(req, select, arr1[0], arr2[0])
            assert abs(arr1 - arr2).max() < 1e-12