import gyral_structure.algorithm
import scipy as sp
from gyral_structure import cost, request, basis
import tempfile
import pytest


def check_costfunc(costfunction, hessp_zero=False):
    """

    :param costfunction:
    :type costfunction: cost.CostFunc
    :return:
    """
    for suff in ['.pck', '.bz', '.bz2', '.gz']:
        file = tempfile.NamedTemporaryFile('w', suffix=suff)
        costfunction.save(file.name)
        cf = cost.read(file.name)
        p0 = sp.randn(cf.npos, cf.ndim)
        assert costfunction.cost(p0) == cf.cost(p0)
        file.close()

    for weight in (0.5, sp.pi):
        cf = weight * costfunction
        assert cf.request == costfunction.request
        p0 = sp.randn(cf.npos, cf.ndim)
        print('weight', weight, cf.cost(p0) / costfunction.cost(p0))
        assert abs((costfunction.cost(p0) * weight - cf.cost(p0)) /cf.cost(p0)) < 1e-7
        dp = sp.randn(cf.npos, cf.ndim) * 1e-5

        if cf.quadratic:
            P, q = cf.qp(p0 + dp / 2)

        # check numeric derivative
        df_numeric = cf.cost(p0 + dp) - cf.cost(p0)
        df_analytic = sp.sum(cf.dercost(p0 + dp / 2) * dp)
        print('dercost', df_numeric, df_analytic, df_numeric / df_analytic)
        assert abs((df_numeric - df_analytic) / (df_numeric + df_analytic)) < 1e-3
        if cf.quadratic:
            df_analytic = sp.sum((q + P * (p0 + dp / 2).flatten()) * dp.flatten())
            print('qp', df_numeric, df_analytic, df_numeric / df_analytic)
            assert abs((df_numeric - df_analytic) / (df_numeric + df_analytic)) < 1e-2

        # check Hessian multiplication
        hessp_numeric = cf.dercost(p0 + dp) - cf.dercost(p0)
        hessp_analytic = cf.hessp(p0 + dp / 2, dp)
        print('hessp', hessp_numeric[0], hessp_analytic[0], (hessp_numeric / hessp_analytic)[0])
        assert abs((hessp_numeric - hessp_analytic) / (abs(hessp_numeric) + abs(hessp_analytic) + 1e-5)).max() < 1e-3
        if cf.quadratic:
            hessp_analytic = (P * dp.flatten()).reshape(p0.shape)
            print('qp', hessp_numeric[0], hessp_analytic[0], (hessp_numeric / hessp_analytic)[0])
            assert abs((hessp_numeric - hessp_analytic) / (abs(hessp_numeric) + abs(hessp_analytic) + 1e-5)).max() < 1e-3

    bases = (basis.Fourier(60, costfunction.ndim, 8.125, 3.7881), )
    # same checks but now including the basis function mapping parameters to vector field
    for basisf in bases:
        cf = 0.5 * costfunction
        full_cost = cf.get_evaluator(basisf, method=gyral_structure.algorithm.Algorithm.matrix)
        p0 = sp.randn(basisf.nparams)
        dp = sp.randn(basisf.nparams) * 1e-5
        if full_cost.quadratic:
            P2, q2= full_cost.qp(p0 + dp / 2)

        df_numeric = full_cost.cost(p0 + dp) - full_cost.cost(p0)
        df_analytic = sp.sum(full_cost.dercost(p0 + dp / 2) * dp)
        print('df', df_numeric, df_analytic, df_numeric / df_analytic)
        assert abs((df_numeric - df_analytic) / (df_numeric + df_analytic + 1e-8)) < 1e-3
        if full_cost.quadratic:
            df_analytic = sp.sum((q2 + sp.array(P2.dot((p0 + dp / 2)[:, None])).flatten()) * dp)
            print('qp', df_numeric, df_analytic, df_numeric / df_analytic)
            assert abs((df_numeric - df_analytic) / (df_numeric + df_analytic + 1e-8)) < 1e-2

        hessp_numeric = full_cost.dercost(p0 + dp) - full_cost.dercost(p0)
        hessp_analytic = full_cost.hessp(p0 + dp / 2, dp)
        print('hessp', hessp_numeric[0], hessp_analytic[0], (hessp_numeric / hessp_analytic)[0])
        assert sp.median(abs((hessp_numeric - hessp_analytic) / (hessp_numeric + hessp_analytic + 1e-8))) < 1e-1
        if hessp_zero:
            assert (hessp_numeric == 0).all()
        else:
            assert (hessp_numeric != 0).any()
        if full_cost.quadratic:
            hessp_analytic = sp.array(P2.dot(dp[:, None])).flatten()
            print('qp', hessp_numeric[0], hessp_analytic[0], (hessp_numeric / hessp_analytic)[0])
            assert abs((hessp_numeric - hessp_analytic) / (hessp_numeric + hessp_analytic + 1e-8)).max() < 1e-1

        p0 = sp.randn(basisf.nparams)
        dp = sp.randn(basisf.nparams)
        cf_other = 0.32 * costfunction
        cmult = (costfunction + cf_other).get_evaluator(basisf)
        smult = costfunction.get_evaluator(basisf)
        assert abs((cmult.cost(p0) - 1.32 * smult.cost(p0)) / (cmult.cost(p0) + 1e-8)) < 1e-4
        assert abs((cmult.dercost(p0) - 1.32 * smult.dercost(p0)) / (cmult.dercost(p0) + 1e-8)).max() < 1e-4
        assert abs((cmult.hessp(p0, dp) - 1.32 * smult.hessp(p0, dp)) / (cmult.hessp(p0, dp) + 1e-8)).max() < 1e-4
        cmult = (costfunction - cf_other).get_evaluator(basisf)
        assert abs((cmult.cost(p0) - 0.68 * smult.cost(p0)) / (cmult.cost(p0) + 1e-8)) < 1e-4
        assert abs((cmult.dercost(p0) - 0.68 * smult.dercost(p0)) / (cmult.dercost(p0) + 1e-8)).max() < 1e-4
        assert abs((cmult.hessp(p0, dp) - 0.68 * smult.hessp(p0, dp)) / (cmult.hessp(p0, dp) + 1e-8)).max() < 1e-4

        nd = costfunction.ndim
        for req in [request.PositionRequest(sp.randn(9, nd)),
                    request.VertexRequest(sp.randn(9, nd, 3)),
                    request.DerivativeRequest(sp.randn(9, nd), (0, 1, 1) if nd == 3 else (0, 2)),
                    request.VoxelRequest(sp.randn(9, nd), sp.rand(9, nd))]:
            cf_other = cost.core.VectorCost(req, sp.randn(9, nd),
                                       norm_length=0, flip=True)
            cmult = (0.5 * (costfunction + cf_other)).get_evaluator(basisf)
            smult2 = cf_other.get_evaluator(basisf)
            if cmult.quadratic:
                P, q = cmult.qp(p0)
            assert abs(2 * cmult.cost(p0) - smult2.cost(p0) - smult.cost(p0)) < abs(1e-7 * cmult.cost(p0))
            assert abs(2 * cmult.dercost(p0) - smult2.dercost(p0) - smult.dercost(p0)).max() < abs(1e-7 * cmult.cost(p0))
            if cmult.quadratic:
                df_direct = cmult.dercost(p0)
                df_qp = q + sp.array(P.dot(p0[:, None])).flatten()
                assert abs((df_direct - df_qp) / (df_direct + df_qp + 1e-20)).max() < 1e-3
            assert abs(2 * cmult.hessp(p0, dp) - smult2.hessp(p0, dp) - smult.hessp(p0, dp)).max() < abs(1e-7 * cmult.cost(p0))
            if cmult.quadratic:
                hessp_direct = cmult.hessp(p0, dp)
                hessp_qp = sp.array(P.dot(dp[:, None])).flatten()
                assert abs((hessp_direct - hessp_qp) / (hessp_direct + hessp_qp + 1e-20)).max() < 1e-3


def test_derivative():
    sp.random.seed(12345)
    for ndim in (2, 3):
        pos = sp.randn(50, ndim)
        p0 = sp.randn(60)
        base = basis.Fourier(60, ndim, 8, 3.)
        for ixdim in range(ndim):
            pos2 = pos.copy()
            pos2[:, ixdim] += 5e-7
            pos3 = pos.copy()
            pos3[:, ixdim] -= 5e-7
            df_numeric = 1e6 * (base.get_evaluator(pos2)(p0) - base.get_evaluator(pos3)(p0))
            nder = sp.zeros(ndim)
            nder[ixdim] = 1
            req = request.DerivativeRequest(pos, tuple(nder))
            df_analytic = base.get_evaluator(req)(p0)
            print(df_numeric[0], df_analytic[0], (df_numeric / df_analytic)[0])
            assert abs((df_numeric - df_analytic) / (df_numeric + df_analytic + 1e-8)).max() < 1e-2


def test_costfunc():
    cost.core.SympyCost.simplify = False
    sp.random.seed(12345)
    for ndim in (3, 2):
        mask = sp.ones((3,) * ndim, dtype='bool')
        mask_req = request.read_volume((mask, sp.eye(ndim + 1)), nsplit=3)
        smooth1 = cost.meta.Smoothness.from_mask(cost.orient.Watson(sp.zeros((0, ndim)), sp.zeros((0, ndim)), mult_term='both'),
                                                 mask)
        smooth2 = cost.meta.Smoothness.from_request(cost.orient.Watson(sp.zeros((0, ndim)), sp.zeros((0, ndim)), mult_term='both'),
                                                    mask_req, max_dist=1.01)
        check_costfunc(smooth1)
        check_costfunc(smooth2)
        field = sp.randn(smooth1.npos, smooth1.ndim)
        print(smooth1.cost(field), smooth2.cost(field))
        assert abs(smooth1.cost(field) - smooth2.cost(field)) < 1e-8, "Both smoothness functions should be identical"
        assert abs(smooth1.dercost(field) - smooth2.dercost(field)).max() < 1e-8, "Both smoothness functions should be identical"
        check_costfunc(cost.meta.Smoothness.from_mask(cost.dens.SameDensity, mask))
        check_costfunc(cost.orient.VonMises(sp.randn(50, ndim), sp.randn(50, ndim), kappa=0.8))
        check_costfunc(cost.orient.Watson(sp.randn(50, ndim), sp.randn(50, ndim), kappa=0.8))
        req = request.PositionRequest(sp.randn(50, ndim))
        check_costfunc(cost.meta.ExponentAdd([cost.orient.NormedInner(req, sp.randn(50, ndim)) for _ in range(3)]))
        check_costfunc(cost.meta.MinCost([cost.orient.NormedInner(req, sp.randn(50, ndim)) for _ in range(4)]))
        if ndim == 3:
            vals, params = sp.randn(50, ndim), sp.randn(5, 50)
            fast_cost = cost.orient.BinghamDistribution(vals, *params, flipx=False)
            check_costfunc(fast_cost)
            sympy_cost = cost.orient.BinghamDistributionSympy(vals, *params)
            assert abs(((fast_cost.cost(vals) - sympy_cost.cost(vals))) / fast_cost.cost(vals)) < 1e-10
            assert abs(((fast_cost.dercost(vals) - sympy_cost.dercost(vals))) / fast_cost.dercost(vals)).max() < 1e-10
        check_costfunc(cost.dens.L1Density(sp.randn(50, ndim)))
        check_costfunc(cost.dens.L2Density(sp.randn(50, ndim)))
        params = sp.randn(50, ndim)
        assert abs((cost.dens.L1Density(params) ** 2).cost(params) - cost.dens.L2Density(params).cost(params)) < 1e-10

        check_costfunc(cost.dens.MinInner(sp.randn(50, ndim), sp.randn(50, ndim)), hessp_zero=True)
        check_costfunc(cost.dens.MinInner(sp.randn(50, ndim), sp.randn(50, ndim), min_inner=0.4), hessp_zero=True)
        check_costfunc(cost.dens.MinInner(sp.randn(50, ndim), sp.randn(50, ndim), min_inner=0.4, normed=True))
        check_costfunc(cost.orient.NormedInner(sp.randn(50, ndim), sp.randn(50, ndim)))
        check_costfunc(cost.orient.NormedInner(sp.randn(50, ndim), sp.randn(50, ndim), min_norm=1e4))
        cf_power = cost.orient.NormedInner(sp.randn(50, ndim), sp.randn(50, ndim)) ** 2.5
        check_costfunc(cf_power)
        cf_power.tosum = True
        check_costfunc(cf_power)
        for flip in (True, False):
            print(ndim, flip)
            check_costfunc(cost.dens.InnerCost(sp.randn(50, ndim), sp.randn(50, ndim), sp.randn(50), flip=flip))
            check_costfunc(cost.dens.InnerCost(sp.randn(50, ndim), sp.randn(50, ndim), sp.zeros(50), flip=flip))
            check_costfunc(cost.dens.TotalIntersect(sp.randn(50, ndim), sp.randn(50, ndim), 0, flip=flip, size=0.5))
            check_costfunc(cost.dens.TotalIntersect(request.VertexRequest(sp.rand(50, ndim, 3)),
                                                    sp.randn(50, ndim), 10., flip=flip))
            for norm in (0, 1, 2):
                print(ndim, flip, norm)
                check_costfunc(cost.core.VectorCost(sp.randn(50, ndim), sp.randn(50, ndim), norm_length=norm, flip=flip))
                if norm != 1:
                    check_costfunc(cost.core.VectorCost(sp.randn(50, ndim), sp.zeros((50, ndim)), norm_length=norm, flip=flip))

            check_costfunc(cost.dens.InnerCost(request.VoxelRequest(sp.randn(50, ndim)), sp.randn(50, ndim), sp.randn(50), flip=flip))
            check_costfunc(cost.dens.InnerCost(request.VertexRequest(sp.randn(50, ndim, 3)), sp.randn(50, ndim), sp.randn(50), flip=flip))


def check_param_cost(costfunction):
    p0 = sp.randn(60)
    dp = sp.randn(60) * 1e-4

    numeric = costfunction.cost(p0 + dp) - costfunction.cost(p0)
    deriv = costfunction.dercost(p0 + dp / 2)
    analytic = (deriv * dp).sum()
    print(numeric, analytic)
    assert abs((numeric - analytic) / (numeric + analytic + 1e-20)) < 1e-4

    basisf = basis.Fourier(60, 3, 8.125, 3.7881)
    ct = costfunction.get_evaluator(basisf)
    assert ct.cost(p0) == costfunction.cost(p0)
    assert (ct.dercost(p0) == costfunction.dercost(p0)).all()

    assert (2.1 * costfunction).cost(p0) == 2.1 * costfunction.cost(p0)
    assert ((2 * costfunction).dercost(p0) == 2 * costfunction.dercost(p0)).all()
    assert (-costfunction).cost(p0) == -(costfunction.cost(p0))
    assert ((-costfunction).dercost(p0) == -(costfunction.dercost(p0))).all()

    nd = 3
    for req in [request.PositionRequest(sp.randn(9, nd)),
                request.VertexRequest(sp.randn(9, nd, 3)),
                request.DerivativeRequest(sp.randn(9, nd), (0, 1, 1) if nd == 3 else (0, 2)),
                request.VoxelRequest(sp.randn(9, nd), sp.rand(9, nd))]:
        cf_other = cost.core.VectorCost(req, sp.randn(9, nd),
                                   norm_length=0, flip=True)
        ct_other = cf_other.get_evaluator(basisf)
        ct = (cf_other + costfunction).get_evaluator(basisf)

        print(ct.cost(p0), (ct_other.cost(p0) + ct.cost(p0)))
        assert abs((ct.cost(p0) - (ct_other.cost(p0) + costfunction.cost(p0))) / ct.cost(p0)) < 1e-6
        assert abs((ct.dercost(p0) - (ct_other.dercost(p0) + costfunction.dercost(p0))) / ct.dercost(p0)).max() < 1e-6


def test_param_cost():
    sp.random.seed(12345)
    params = sp.randn(50)
    assert cost.param.L2Params().cost(params) == (params ** 2).sum()
    assert (cost.param.L2Params() ** 0.5).cost(params) == abs(params).sum()
    assert abs((cost.param.L2Params() ** 0.5).dercost(params) - sp.sign(params)).max() < 1e-5
    assert cost.param.L2Params().cost(params) == cost.param.ParamVecMult(params).cost(params)
    print(cost.param.L2Params().cost(params), cost.param.ParamMatMult(sp.eye(50)).cost(params))
    assert abs(cost.param.L2Params().cost(params) - cost.param.ParamMatMult(sp.eye(50)).cost(params)).max() < 1e-6
    assert abs(cost.param.L2Params().cost(params) - cost.param.ParamTargetVec(sp.eye(50), sp.zeros(50)).cost(params)).max() < 1e-6
    assert abs(cost.param.L2Params().cost(params) - cost.param.ParamTargetVec(sp.eye(50), params).cost(sp.zeros(50))).max() < 1e-6
    check_param_cost(cost.param.L2Params())
    check_param_cost(cost.param.ParamVecMult(sp.randn(60)))
    symm_mat = sp.randn(60, 60)
    symm_mat += symm_mat.T
    check_param_cost(cost.param.ParamMatMult(symm_mat))
    check_param_cost(cost.param.ParamMatMult(symm_mat, sp.randn(60)))
    check_param_cost(cost.param.ParamTargetVec(sp.randn(60, 60), sp.randn(60)))

