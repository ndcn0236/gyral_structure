import scipy as sp
import sympy
from gyral_structure.basis import radial
from gyral_structure.basis.radial import sparse
from scipy import spatial
import itertools
from numpy import testing


def _test_divergence_free():
    """Test analytically whether the proposed matrix-valued vector fields are divergence-free
    """
    for smoothness in range(4):
        obj = radial.wendland(smoothness)
        proc = lambda idx: row[idx].subs('r', obj.radius).diff('xyz'[idx])
        for row in obj._matrix_valued(as_sympy=True):
            value = sympy.simplify((proc(0) + proc(1) + proc(2)))
            print(smoothness, value)
            #print('x-component', sympy.simplify(proc(0)))
            assert value == 0


def test_to_matrix():
    """Tests the computation of the matrix mapping parameters to field
    """
    sp.random.seed(12345)
    centroids = sp.zeros((5, 3))
    centroids[:, 0] = sp.linspace(-1, 1, 5)
    points = sp.randn(50, 3)
    out_of_range = (abs(points[:, 1:]) > 1).any(-1)
    assert out_of_range.sum() != 0
    for smoothness in range(1, 4):
        obj = radial.RadialBasis(radial.wendland(smoothness), centroids)
        mat = obj.get_full_mat(points)
        assert mat.shape == (points.size, centroids.size)
        assert (sp.array(abs(mat).sum(-1)).reshape(points.shape) == 0)[out_of_range, :].all()
        #assert (mat == obj.to_matrix(points[None, :, :] - centroids[:, None, :]).T).todense().all()


def test_precompute():
    """
    Tests the computation of which dipoles intersect a grid
    """
    sp.random.seed(123)
    centroids = sp.random.randn(25, 3)
    rb = radial.RadialBasis(radial.wendland(), centroids, size=0.4)
    params = sp.random.rand(rb.nparams)

    points = sp.randn(50, 3)
    res_init = rb.get_evaluator(points)(params)

    rb.precompute_evaluator(extent=0, resolution=0.2)
    testing.assert_allclose(rb.get_evaluator(points)(params), res_init)

    rb.precompute_evaluator(extent=0)
    testing.assert_allclose(rb.get_evaluator(points)(params), res_init)


def test_compare_RBF_to_old():
    for smoothness in range(1, 4):
        rbf_old = CompactRBFOld(smoothness)
        centroids = sp.randn(300, 3)
        targets = sp.randn(200, 3)
        rbf_new = radial.RadialBasis(radial.wendland(smoothness), centroids)
        new_mat = rbf_new.get_full_mat(targets)
        old_mat = rbf_old.to_matrix(targets, centroids)
        assert abs(new_mat - old_mat).max() <= 1e-8


class CompactRBFOld(object):
    """Sympy representations of the compactly supported radial basis functions from Wendland (1995)

    This is an old, slow version of the RBF, kept here for comparison with the new version in gyral_structure.basis.radial_basis
    """
    def __init__(self, smoothness=2):
        """Creates a new representation from the following list:

        smoothness == 0: (1 - r)+^2
        smoothness == 1: (1 - r)+^4 (4 r + 1)
        smoothness == 2: (1 - r)+^6 (35 r^2 + 18 r + 3)
        smoothness == 3: (1 - r)+^8 (32 r^3 + 25 r^3 + 8r + 1)

        :param smoothness: determines how differentiable the function is at the centroid
        """
        xyz = sympy.var('x, y, z')
        xyz_sq = [elem ** 2 for elem in xyz]
        self.radius = sympy.sqrt(xyz_sq[0] + xyz_sq[1] + xyz_sq[2])
        if smoothness not in (0, 1, 2, 3):
            raise ValueError("Compactly supported radial basis functions form Wendland (1995) only available for smoothness of 0, 1, 2, or 3")
        self.rad_symb = sympy.var('r')
        additional = (1 if smoothness == 0 else
                      (4 * self.rad_symb + 1 if smoothness == 1 else
                       (35 * self.rad_symb ** 2 + 18 * self.rad_symb + 3 if smoothness == 2 else
                        (32 * self.rad_symb ** 3 + 25 * self.rad_symb ** 2 + 8 * self.rad_symb + 1))))
        self.raw_func = (1 - self.rad_symb) ** ((smoothness + 1) * 2) * additional
        self.matrix_valued = self._matrix_valued(as_sympy=False)
        self.smoothness = smoothness

    def second_der(self, var1='x', var2='x'):
        """Computes the second derivative of the raw function with respect to `var1` and `var2`

        :param var1: (x, y, or z); deterimines the dimension of the first spatial derivative
        :param var2: (x, y, or z); deterimines the dimension of the second spatial derivative
        """
        rad_first_der = sympy.diff(self.raw_func, 'r')
        rad_second_der = sympy.diff(self.raw_func, 'r', 'r')

        comp_first_der = -(sympy.var(var1) * sympy.var(var2)) / self.rad_symb ** 3
        if var1 == var2:
            comp_first_der = comp_first_der + 1 / self.rad_symb
        comb_second_der = sympy.var(var1) * sympy.var(var2) / self.rad_symb ** 2
        return rad_first_der * comp_first_der + rad_second_der * comb_second_der

    def _matrix_valued(self, as_sympy=False):
        """Returns a (3, 3) array of the sympy equations giving the matrix-valued radial basis function.
        """
        res = sp.zeros((3, 3), dtype='object')
        for idx_var1 in range(3):
            for idx_var2 in range(3):
                if idx_var1 == idx_var2:
                    var1 = 'xyz'[(idx_var1 + 1) % 3]
                    var2 = 'xyz'[(idx_var1 + 2) % 3]
                    res[idx_var1, idx_var2] = -self.second_der(var1, var1) - self.second_der(var2, var2)
                else:
                    var1 = 'xyz'[idx_var1]
                    var2 = 'xyz'[idx_var2]
                    res[idx_var1, idx_var2] = self.second_der(var1, var2)
                if not as_sympy:
                    res[idx_var1, idx_var2] = sympy.lambdify(('r', 'x', 'y', 'z'),
                                                             res[idx_var1, idx_var2], modules="numpy")
        return res

    def to_matrix(self, targets, centroids, size=1., centroid_tree=None, target_tree=None, run_new=True):
        """Returns a matrix mapping a flattened (3, Np) parameters array to a (Nf, 3) field array.

        :param targets: (Nf, 3) array with the positions where the field should be computed
        :param centroids: (Np, 3) array with the positions of the radial basis function centroids
        :param size: (Np, ) array with the radii of the radial basis functions
        :param centroid_tree: pre-computed scipy kd-tree for nearest-neighbour lookup for the centroids
        :type centroid_tree: spatial.cKDTree
        :param target_tree: pre-computed scipy kd-tree for nearest-neighbour lookup for the targets
        :type target_tree: spatial.cKDTree
        :return: (Nf * 3, Np * 3) sparse matrix in COO format
        """
        offset = normalized_offset(centroids, targets, size1=size, tree1=centroid_tree, tree2=target_tree)
        row3d = sp.zeros((3, 3, offset.size), dtype='i4')
        col3d = sp.zeros((3, 3, offset.size), dtype='i4')
        dat3d = sp.zeros((3, 3, offset.size))
        for idx_var1 in range(3):
            for idx_var2 in range(3):
                func = self.matrix_valued[idx_var1, idx_var2]
                row3d[idx_var1, idx_var2] = offset['idx2'] * 3 + idx_var1
                col3d[idx_var1, idx_var2] = offset['idx1'] + idx_var2 * centroids.shape[0]
                dat3d[idx_var1, idx_var2] = func(offset['rad'], offset['offset'][:, 0],
                                                 offset['offset'][:, 1], offset['offset'][:, 2])
        return sparse.coo_matrix((dat3d.flatten(), (row3d.flatten(), col3d.flatten())),
                                 shape=(targets.shape[0] * 3, centroids.shape[0] * 3))


def normalized_offset(pos1, pos2, size1=0, size2=0, tree1=None, tree2=None):
    """Computes the offsets between `pos1` and `pos2` for all points within the point sizes

    :param pos1: (N, K) array of N points in K-dimensional space
    :param pos2: (M, K) array of M points in K-dimensional space
    :param size1: float or (N, ) array the point sizes
    :param size2: float or (M, ) array of the point sizes
    :param tree1: pre-computed scipy kd-tree
    :type tree1: spatial.cKDTree
    :param tree2: pre-computed scipy kd-tree
    :type tree2: spatial.cKDTree
    :return: (L, ) structured array for all pairs of points within the point sizes of each other with the following entries:
    - 'idx1': indices for `pos1`
    - 'idx2': indices for `pos2`
    - 'offset': offset from pos1 to pos2 normalized by the sizes of the points
    - 'rad': distance between pos1 and pos2 normalized by the sizes of the points
    """
    pos1 = sp.asarray(pos1)
    pos2 = sp.asarray(pos2)
    if tree1 is None:
        tree1 = spatial.cKDTree(pos1)
    if tree2 is None:
        tree2 = spatial.cKDTree(pos2)
    size1 = sp.asarray(size1)
    if size1.ndim == 0:
        size1 = sp.zeros(pos1.shape[0]) + size1
    size2 = sp.asarray(size2)
    if size2.ndim == 0:
        size2 = sp.zeros(pos2.shape[0]) + size2
    max_size = size1.max() + size2.max()
    conn = tree1.query_ball_tree(tree2, r=max_size)

    idx2_long = sp.fromiter(itertools.chain(*conn), dtype='i4')
    res = sp.zeros(idx2_long.size, dtype=[('idx1', 'i4'), ('idx2', 'i4'), ('offset', ('f8', pos1.shape[1])),
                                          ('rad', 'f8')])
    res['idx2'] = idx2_long
    idx_elem = 0
    for idx_arr, arr in enumerate(conn):
        res['idx1'][idx_elem: idx_elem + len(arr)] = idx_arr
        idx_elem += len(arr)
    res['offset'] = (pos2[res['idx2']] - pos1[res['idx1']]) / (size1[res['idx1']] + size2[res['idx2']])[:, None]
    res['rad'] = sp.sqrt((res['offset'] ** 2).sum(-1))
    use = res['rad'] < 1
    return res[use]
