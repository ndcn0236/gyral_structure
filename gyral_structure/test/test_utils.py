"""
Test :mod:`gyral_structure.utils`
"""
import numpy as np
from gyral_structure import utils
from numpy import testing


def test_diagonal_affine():
    """
    Tests for applying a diagonal affine to dyads
    """
    pos_affine = np.eye(4)
    pos_affine[range(3), range(3)] = 1.3

    neg_affine = pos_affine.copy()
    neg_affine[0, 0] *= -1

    dyads = np.random.randn(20, 10, 3)
    res_dyads = utils.dyad_to_mm(dyads, pos_affine)
    normed_dyads = dyads / np.sqrt((dyads ** 2).sum(-1)[..., None])
    testing.assert_allclose(res_dyads[..., 0], -normed_dyads[..., 0])
    testing.assert_allclose(res_dyads[..., 1:], normed_dyads[..., 1:])

    neg_res_dyads = utils.dyad_to_mm(dyads, neg_affine)
    testing.assert_allclose(res_dyads, neg_res_dyads)

    neg_affine[:3, -1] = np.random.randn(3)
    neg_res_dyads = utils.dyad_to_mm(dyads, neg_affine)
    testing.assert_allclose(res_dyads, neg_res_dyads)


def test_random_affine():
    """
    Tests for applying a random affine to dyads
    """
    affine = np.random.randn(4, 4)
    affine[-1, :] = [0, 0, 0, 1]
    if np.linalg.det(affine) > 0:
        affine *= -1
        affine[-1, :] = [0, 0, 0, 1]

    testing.assert_allclose(
        utils.dyad_to_mm(np.array([1., 0, 0]), affine),
        affine[:-1, 0] / np.sqrt((affine[:-1, 0] ** 2).sum(-1))
    )
    testing.assert_allclose(
        utils.dyad_to_mm(np.array([0, 2., 0]), affine),
        affine[:-1, 1] / np.sqrt((affine[:-1, 1] ** 2).sum(-1))
    )

    flipped_affine = affine.copy()
    flipped_affine[:-1, 0] *= -1

    random_dyads = np.random.randn(20, 10, 3)
    testing.assert_allclose(
        utils.dyad_to_mm(random_dyads, affine),
        utils.dyad_to_mm(random_dyads, flipped_affine),
    )
    testing.assert_allclose(
        utils.dyad_from_mm(utils.dyad_to_mm(random_dyads, affine), affine),
        random_dyads / np.sqrt((random_dyads ** 2).sum(-1))[..., None]
    )
    testing.assert_allclose(
        utils.dyad_from_mm(utils.dyad_to_mm(random_dyads, flipped_affine), flipped_affine),
        random_dyads / np.sqrt((random_dyads ** 2).sum(-1))[..., None]
    )

