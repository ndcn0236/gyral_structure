import scipy as sp
from gyral_structure import basis, request, algorithm
import tempfile
import pytest


def requests(ndim, npos=20, noder=False):
    for req in (request.PositionRequest(sp.randn(npos, ndim)),
                request.VoxelRequest(sp.randn(npos, ndim), sp.rand(ndim), nsplit=2),
                request.VertexRequest(sp.randn(npos, ndim, 3))):
        yield req
    if not noder:
        yield request.DerivativeRequest(sp.randn(npos, ndim), (1, ) * ndim)


def check_divergence_free(basis_func, npos=13):
    pos = sp.randn(npos, basis_func.ndim)
    params = sp.randn(basis_func.nparams)
    der = sp.zeros((npos, basis_func.ndim))
    for dim in range(basis_func.ndim):
        deriv = sp.zeros(basis_func.ndim, dtype='i4')
        deriv[dim] = 1
        der[:, dim] = request.DerivativeRequest(pos, tuple(deriv)).get_evaluator(basis_func)(params)[:, dim]
    assert abs(der.sum(-1) / (abs(der).sum(-1) + 1e-4)).max() < 1e-5


def check_computation(basis_func, npos=13, noder=False, nodiv=True):
    """Checks that the various ways to calculate the values are consistent

    :param basis_func: basis function to be tested
    :type basis: basis.BasisFunc
    :param nparams: number of parameters to test
    :param noder: don't test the derivative request
    """
    ndim = basis_func.ndim
    options = algorithm.full_chain(basis_func.compute)
    print(basis_func, options)
    if len(options) < 2:
        raise ValueError("Insufficient methods to compare")
    method = tuple(options)[0]

    for suff in ['.h5', '.hdf5']:
        file = tempfile.NamedTemporaryFile('w', suffix=suff)
        params = sp.randn(basis_func.nparams)
        basis_func.save(file.name, params)
        nbasis, nparams = basis.read(file.name)
        assert (nparams == params).all()
        req = next(requests(ndim, npos, noder=noder))
        assert abs(basis_func.get_evaluator(req, method=method)(params) - nbasis.get_evaluator(req, method=method)(nparams)).max() < 1e-8
        file.close()

    for req in requests(ndim, npos, noder=noder):
        limit = 1e-5
        parameters = sp.randn(basis_func.nparams)
        with algorithm.set(method=method):
            map1 = basis_func.get_evaluator(req)
        if hasattr(map1, 'method'):
            #print(map1.method, method)
            assert map1.method == method
        res1 = map1(parameters)
        print('running param_evaluator')
        resalt = basis_func.param_evaluator(parameters, method=method, nsim=npos//2)(req)
        if hasattr(basis_func, '_precomputed_grid'):
            basis_func._precomputed_grid = None
        assert res1.shape == (req.npos, ndim)
        #print(method, req, res1[0], resalt[0])
        assert sp.median(abs((res1 - resalt) / (res1 + resalt + 1e-4))) < limit

        for method2 in basis_func.compute:
            if method != method2:
                print(f'testing {method2} for {req}')
                res2 = basis_func.get_evaluator(req, method=method2)(parameters)
                assert res2.shape == (req.npos, ndim)
                #print(method, res1[0], method2, res2[0])
                assert sp.median(abs((res1 - res2) / (res1 + res2 + 1e-4))) < limit, "%s and %s do not give consistent results (for %s)" % (method, method2, req)

                res_param = basis_func.param_evaluator(parameters, method=method2, nsim=npos//2)(req)
                print(res2, 'param', npos // 2, res_param)
                assert res_param.shape == (req.npos, ndim)
                assert sp.median(abs((res1 - res_param) / (res1 + res_param + 1e-4))) < limit

        # checking propogation of derivatives
        derf = sp.randn(npos, ndim)
        derp1 = map1(derf, inverse=True)
        #print(method, derp1)
        assert derp1.size == basis_func.nparams
        for method2 in options:
            if method != method2:
                derp2 = basis_func.get_evaluator(req, method=method2)(derf, inverse=True)
                assert derp1.shape == derp2.shape
                #print(method, derp1[0], method2, derp2[0])
                assert sp.median(abs((derp1 - derp2) / (derp1 + derp2 + 1e-3))) < 1e-3, "%s and %s do not give consistent derivatives" % (method, method2)

    if nodiv:
        check_divergence_free(basis_func, npos)


@pytest.mark.slowtest
def test_computation():
    sp.random.seed(12345)
    check_computation(basis.RadialBasis(basis.buhmann(3),
                                        sp.randn(15, 3), size=sp.rand(15) * 3 + 0.1))
    check_computation(basis.RadialBasis(basis.wendland(2), sp.randn(14, 2), size=1.82))
    check_computation(basis.ChargeDistribution(sp.randn(11, 3)), nodiv=False)
    check_computation(basis.Fourier(150, 3, 10))
    mb = basis.SumBase([basis.Fourier(150, 3, 10), basis.ChargeDistribution(sp.randn(91, 3))])
    check_computation(mb, nodiv=False)
    for ndim in (2, 3):
        for order in (0, 1):
            print('gradient of ndim %i and order %i' % (ndim, order))
            check_computation(basis.Gradient(order, ndim=ndim))
    mb = basis.SumBase([basis.Fourier(150, 3, 10), basis.RadialBasis(basis.wendland(3), sp.randn(17, 3),
                                                                     size=sp.rand(17))])
    check_computation(mb)
    check_computation(basis.Polynomial(3, ndim=2))
    check_computation(basis.Polynomial(2, ndim=3))

    check_computation(basis.RadialBasis(basis.wendland(2), sp.randn(14, 3), size=1.82))
    #affine = sp.eye(4) * 8
    #affine[-1, -1] = 1
    #affine[:-1, -1] = -8
    #mask = sp.ones((3, 3, 3), dtype='bool')
    #check_computation(trilinear.TriLinear(mask, affine=affine), noder=True)
    #affine[:3, :3] += sp.randn(3, 3)
    #check_computation(trilinear.TriLinear(mask, affine=affine), noder=True)

    check_computation(basis.Fourier(160, 2, 10, sp.array([3, 8.2])))
    check_computation(basis.Fourier(180, 3, 6, sp.array([3, 8.2, 1.37])))

    check_computation(basis.ChargeDistribution(sp.randn(97, 3), charge_size=0.01), nodiv=False)
    check_computation(basis.ChargeDistribution(sp.randn(97, 2), charge_size=0.01), nodiv=False)

