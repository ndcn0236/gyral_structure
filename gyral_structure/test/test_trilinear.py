import itertools
import scipy as sp
from gyral_structure.basis import trilinear
from .. import request


def test_poly_to_grid():
    """Compare with alternative implementation of poly_to_grid.
    """
    res = sp.zeros((2, ) * 6)
    for xyz in itertools.product(range(2), repeat=3):
        for ijk in itertools.product(range(2), repeat=3):
            res[xyz + ijk] = sp.prod((sp.array(xyz) - 0.5) ** sp.array(ijk))
    assert (res == trilinear.poly_to_grid).all()


def test_grid_to_pos():
    """Test the interpolation to the corners, voxel center, midlines, and mid-planes.
    """
    sp.random.seed(12345)
    for _ in range(3):
        scalar_edge = sp.randn(2, 2, 2)
        field_edge = sp.randn(2, 2, 2, 3)

        # check the corners
        for ijk in itertools.product(range(2), repeat=3):
            xyz = sp.array(ijk) - 0.5
            print(scalar_edge[ijk], (trilinear.grid_to_pos_scalar(xyz) * scalar_edge).sum())
            assert scalar_edge[ijk] == (trilinear.grid_to_pos_scalar(xyz) * scalar_edge).sum()

            print(field_edge[ijk], (trilinear.grid_to_pos_field(xyz, curl=False) * field_edge).sum((1, 2, 3, 4)))
            assert (field_edge[ijk] == (trilinear.grid_to_pos_field(xyz, curl=False) * field_edge).sum((1, 2, 3, 4))).all()

        # check the voxel center
        print('center', (trilinear.grid_to_pos_scalar(sp.zeros(3)) * scalar_edge).sum(), scalar_edge.mean())
        assert (trilinear.grid_to_pos_scalar(sp.zeros(3)) * scalar_edge).sum() == scalar_edge.mean()
        print(field_edge.mean((0, 1, 2)), (trilinear.grid_to_pos_field(sp.zeros(3), curl=False) * field_edge).sum((1, 2, 3, 4)))
        assert abs(field_edge.mean((0, 1, 2)) - (trilinear.grid_to_pos_field(sp.zeros(3), curl=False) * field_edge).sum((1, 2, 3, 4))).max() < 1e-8

        # check the derivatives at the voxel center
        for dim in range(3):
            derivative = sp.zeros(3, dtype='i4')
            derivative[dim] = 1
            der_model = (trilinear.grid_to_pos_scalar(sp.zeros(3), derivative) * scalar_edge).sum()
            other_dims = [0, 1, 2]
            other_dims.remove(dim)
            params = scalar_edge.mean(tuple(other_dims))
            der = params[1] - params[0]
            print('derivative center', dim, der, der_model)
            assert abs(der - der_model) < 1e-8

        field_center = (trilinear.grid_to_pos_field(sp.zeros(3), curl=True) * field_edge).sum((1, 2, 3, 4))
        derpot = sp.zeros((3, 3))
        for der in range(3):
            other_dims = [0, 1, 2]
            other_dims.remove(der)
            for pot in range(3):
                fe_mean = field_edge[..., pot].mean(tuple(other_dims))
                derpot[der, pot] = fe_mean[1] - fe_mean[0]
        for dim in range(3):
            dim1 = (dim + 1) % 3
            dim2 = (dim + 2) % 3
            print('curl', dim, field_center[dim], derpot[dim1, dim2] - derpot[dim2, dim1])
            assert abs(field_center[dim] - (derpot[dim1, dim2] - derpot[dim2, dim1])) < 1e-8

        for dim in range(3):
            for edge in [0, 1]:
                # check the center of each plane
                pos = sp.zeros(3)
                pos[dim] = edge - 0.5
                select = [slice(None)] * 3
                select[dim] = edge
                print(dim, edge, pos, select, (trilinear.grid_to_pos_scalar(pos) * scalar_edge).sum(), scalar_edge[tuple(select)].mean())
                assert abs((trilinear.grid_to_pos_scalar(pos) * scalar_edge).sum() - scalar_edge[tuple(select)].mean()).max() < 1e-8

                # check the midline of each edge
                pos = sp.zeros(3) + edge - 0.5
                pos[dim] = 0
                select = [edge] * 3
                select[dim] = slice(None)
                print(dim, edge, pos, select, (trilinear.grid_to_pos_scalar(pos) * scalar_edge).sum(), scalar_edge[tuple(select)].mean())
                assert abs((trilinear.grid_to_pos_scalar(pos) * scalar_edge).sum() - scalar_edge[tuple(select)].mean()).max() < 1e-8

                # check the derivative at the midline
                params = scalar_edge[tuple(select)]
                der = params[1] - params[0]
                derivative = sp.zeros(3, dtype='i4')
                derivative[dim] = 1
                der2 = (trilinear.grid_to_pos_scalar(pos, derivative) * scalar_edge).sum()
                print(trilinear.grid_to_pos_scalar(pos, derivative))
                print('derivative', derivative, der, der2)
                assert abs(der - der2) < 1e-8

        for _ in range(5):
            pos = sp.rand(3) - 0.5
            assert (trilinear.grid_to_pos_scalar(pos, (2, 0, 0)) == 0).all()
            assert (trilinear.grid_to_pos_scalar(pos, (0, 2, 0)) == 0).all()
            assert (trilinear.grid_to_pos_scalar(pos, (0, 0, 2)) == 0).all()
            assert (trilinear.grid_to_pos_scalar(pos, (2, 1, 1)) == 0).all()
            assert (trilinear.grid_to_pos_scalar(pos, (1, 1, 1)) != 0).all()
            for curl in (False, True):
                assert (
                trilinear.grid_to_pos_field([pos], curl=curl)[0] == trilinear.grid_to_pos_field(pos, curl=curl)).all()
                assert (trilinear.grid_to_pos_field([pos] * 2, curl=curl)[0] == trilinear.grid_to_pos_field(pos, curl=curl)).all()
                assert (trilinear.grid_to_pos_field([pos] * 2, curl=curl)[1] == trilinear.grid_to_pos_field(pos, curl=curl)).all()


def test_trivial_trilinear():
    """Tests the trilinear interpolation at the corners and the voxel centers
    """
    sp.random.seed(12345)
    mask = sp.ones((3, 3, 3), dtype='bool')
    affine = sp.eye(4)
    basisfunc = trilinear.TriLinear(mask, affine)
    param_grid = sp.randn(4, 4, 4, 3)
    params = param_grid.flatten()
    req = request.PositionRequest(sp.zeros((1, 3)) - 0.5)
    for method in basisfunc.compute:
        map = basisfunc.get_evaluator(req, method)
        field = map(params)
        print(method)
        guess = ((param_grid[0, 1, 0, 2] - param_grid[0, 0, 0, 2]) -
                 (param_grid[0, 0, 1, 1] - param_grid[0, 0, 0, 1]))
        print(field[0, 0], guess)
        assert abs(field[0, 0] - guess) < 1e-8
    req = request.PositionRequest(sp.zeros((1, 3)) + 0.5 + 1e-10)
    for method in basisfunc.compute:
        map = basisfunc.get_evaluator(req, method)
        field = map(params)
        print(method)
        guess = ((param_grid[1, 2, 1, 2] - param_grid[1, 1, 1, 2]) -
                 (param_grid[1, 1, 2, 1] - param_grid[1, 1, 1, 1]))
        print(field[0, 0], guess)
        assert abs(field[0, 0] - guess) < 1e-3
    req = request.PositionRequest(sp.zeros((1, 3)) + 0.5 - 1e-10)
    for method in basisfunc.compute:
        map = basisfunc.get_evaluator(req, method)
        field = map(params)
        print(method)
        guess = ((param_grid[1, 1, 1, 2] - param_grid[1, 0, 1, 2]) -
                 (param_grid[1, 1, 1, 1] - param_grid[1, 1, 0, 1]))
        print(field[0, 0], guess)
        assert abs(field[0, 0] - guess) < 1e-2
    req = request.PositionRequest(sp.zeros((1, 3)))
    for method in basisfunc.compute:
        map = basisfunc.get_evaluator(req, method)
        field = map(params)
        print(method)
        guess = ((param_grid[:2, 1, :2, 2].mean() - param_grid[:2, 0, :2, 2].mean()) -
                 (param_grid[:2, :2, 1, 1].mean() - param_grid[:2, :2, 0, 1].mean()))
        print(field[0, 0], guess)
        assert abs(field[0, 0] - guess) < 1e-8

