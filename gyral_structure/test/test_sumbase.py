from .. import basis, request
import scipy as sp
from pytest import raises
from numpy import testing
import tempfile


def test_sumbase():
    b1 = basis.Polynomial(4)
    b2 = basis.Fourier(6, ndim=3)
    b3 = basis.RadialBasis(basis.wendland(2), sp.rand(7, 3))
    sb1 = basis.SumBase([b1, b2, b3])
    sb2 = basis.SumBase([b1])
    sb2.append(b2)
    sb2.append(b3)
    assert sb1.nparams == sb2.nparams
    assert sb1 == [b1, b2, b3]
    assert sb1.nparams == b1.nparams + b2.nparams + b3.nparams
    eb2 = basis.Fourier(40, ndim=2)
    with raises(ValueError):
        basis.SumBase([b1, eb2, b3])
    with raises(ValueError):
        basis.SumBase([eb2, b3, b1])

    pos = sp.rand(13, 3)
    basis_list = (b1, b2, b3)
    for req in [pos, request.PositionRequest(pos), request.VoxelRequest(pos, nsplit=1),
                request.VertexRequest(sp.rand(13, 3, 3), nsplit=1)]:
        params = [sp.randn(bf.nparams) for bf in basis_list]
        pfull = sp.concatenate(params)
        field = sp.randn(*pos.shape)
        values = [bf.get_evaluator(req)(pf) for bf, pf in zip(basis_list, params)]
        print('request', req)
        assert (sp.sum(values, 0) == sb1.get_evaluator(req)(pfull)).all()
        values_rev = sp.concatenate([bf.get_evaluator(req)(field, inverse=True) for bf in basis_list])
        assert values_rev.shape == pfull.shape
        assert (values_rev == sb1.get_evaluator(req)(field, inverse=True)).all()

        sb1.fix(1, params[1])
        ppart = sp.concatenate((params[0], params[2]))
        assert (sp.sum(values, 0) - sb1.get_evaluator(req)(ppart)).max() < 1e-5
        assert (sp.sum(values, 0) - sb1.param_evaluator(
            ppart, extent=req.radius() if isinstance(req, request.FieldRequest) else 0
        )(req)).max() < 1e-5
        for b in basis_list:
            if hasattr(b, '_precomputed_grid'):
                b._precomputed_grid = None

        values_rev = sp.concatenate([bf.get_evaluator(req)(field, inverse=True) for bf in (b1, b3)])
        assert values_rev.shape == ppart.shape
        print('latest', values_rev - sb1.get_evaluator(req)(field, inverse=True))
        assert abs(values_rev - sb1.get_evaluator(req)(field, inverse=True)).max() < 1e-5

        sb1.release(1)
        testing.assert_allclose(sp.sum(values, 0), sb1.get_evaluator(req)(pfull))

        print('testing sb2')
        print(sp.sum(values, 0), sb2.get_evaluator(req)(pfull))
        testing.assert_allclose(sb2.get_evaluator(req)(pfull), sp.sum(values, 0))
        sb2.fix_all(pfull)
        print(sp.sum(values, 0), sb2.get_evaluator(req)([]))
        testing.assert_allclose(sp.sum(values, 0), sb2.get_evaluator(req)([]), atol=1e-5)
        sb2.release_all()


def test_io():
    b1 = basis.Polynomial(4)
    b2 = basis.Fourier(6, ndim=3)
    b3 = basis.RadialBasis(basis.wendland(2), sp.rand(7, 3))
    sb1 = basis.SumBase([b1, b2, b3])

    params = sp.random.randn(sb1.nparams)
    pos = sp.rand(13, 3)

    ref_value = sb1.get_evaluator(pos)(params)

    file = tempfile.NamedTemporaryFile('w', suffix='.h5')
    sb1.save(file.name, params)
    new_sb1, new_params = basis.read(file.name)
    testing.assert_allclose(params, new_params)
    testing.assert_allclose(ref_value, new_sb1.get_evaluator(pos)(new_params))
    file.close()

    sb1.fix(0, params[:b1.nparams])
    testing.assert_allclose(ref_value, sb1.get_evaluator(pos)(params[b1.nparams:]))
    file = tempfile.NamedTemporaryFile('w', suffix='.h5')
    sb1.save(file.name, params[b1.nparams:])
    new_sb1, new_params = basis.read(file.name)
    testing.assert_allclose(params[b1.nparams:], new_params)
    testing.assert_allclose(ref_value, new_sb1.get_evaluator(pos)(new_params))
    file.close()
