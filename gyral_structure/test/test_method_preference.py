from gyral_structure.algorithm import Algorithm
from gyral_structure import algorithm, cuda


def test_method_preference():
    """Tests the preference order
    """
    available = (Algorithm.numpy, Algorithm.matrix_cuda, Algorithm.cuda,
                 Algorithm.matrix, Algorithm.numba)

    nopref = available if cuda.available else (Algorithm.numpy, Algorithm.matrix, Algorithm.numba)
    pref_matrix = (Algorithm.matrix_cuda, Algorithm.matrix, Algorithm.numpy, Algorithm.cuda,
                   Algorithm.numba) if cuda.available else (Algorithm.matrix, Algorithm.numpy, Algorithm.numba)
    no_matrix = (Algorithm.numpy, Algorithm.cuda, Algorithm.numba, Algorithm.matrix_cuda,
                 Algorithm.matrix) if cuda.available else (Algorithm.numpy, Algorithm.numba, Algorithm.matrix)

    def check_expected(expected):
        assert algorithm.full_chain(available) == expected
        with algorithm.set(store_matrix=True, override=True):
            assert algorithm.full_chain(available) == pref_matrix
        assert algorithm.full_chain(available) == expected
        with algorithm.set(store_matrix=False, override=True):
            assert algorithm.full_chain(available) == no_matrix
        assert algorithm.full_chain(available) == expected
        with algorithm.set(store_matrix=None, override=True):
            assert algorithm.full_chain(available) == expected

        for method in expected:
            new_expected = (method, ) + tuple(m for m in expected if m != method)
            assert algorithm.full_chain(available) == expected
            with algorithm.set(method=method):
                assert algorithm.full_chain(available) == new_expected
            assert algorithm.full_chain(available) == expected

    check_expected(nopref)
    with algorithm.set():
        check_expected(nopref)

    with algorithm.set(store_matrix=True):
        check_expected(pref_matrix)
        with algorithm.set(store_matrix=False):
            check_expected(pref_matrix)
        with algorithm.set(store_matrix=False, override=True):
            check_expected(no_matrix)
            with algorithm.set(store_matrix=True, override=True):
                check_expected(pref_matrix)
        with algorithm.set(store_matrix=None, override=True):
            check_expected(pref_matrix)

    with algorithm.set(store_matrix=False):
        check_expected(no_matrix)




