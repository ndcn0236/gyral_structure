from gyral_structure.basis import tricubic
import scipy as sp


def test_grid_to_pos_scalar():
    # test that the values at the corners are correct
    corners = sp.mgrid[:2, :2, :2].reshape(3, 8).T - 0.5
    i, j, k = sp.mgrid[:2, :2, :2].reshape(3, 8)
    for idx_diff, nder in enumerate([(0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1),
                                     (1, 1, 0), (1, 0, 1), (0, 1, 1), (1, 1, 1)]):
        kernel = tricubic.grid_to_pos_scalar(corners, nder=nder)
        assert (kernel[range(8), i, j, k, idx_diff] == 1).all()
        assert (kernel.sum((1, 2, 3, 4)) == 1).all()

    # test that the low-order derivatives are continious at the corners
    all_der = sp.mgrid[:2, :2, :2].reshape(3, 8).T
    for nder in all_der:
        kernel = tricubic.grid_to_pos_scalar(corners, nder=nder)
        print('testing dependency of corners for', nder)
        assert (kernel[range(8), i, j, k, :] != 0).any()
        kernel[range(8), i, j, k, :] = 0
        assert (kernel == 0).all()

    # test that the low-order derivatives are continiuous at the faces
    pos = (sp.mgrid[:10, :10, :1].reshape(3, 100).T / 9.) - 0.5
    pos_upper = pos.copy()
    pos_upper[:, 2] = 0.5
    for nder in all_der:
        kernel = tricubic.grid_to_pos_scalar(pos, nder=nder)
        print('testing dependency of z-face for', nder)
        assert abs(kernel[:, :, :, 0, :]).max() > 1e-3
        assert abs(kernel[:, :, :, 1, :]).max() < 1e-10
        kernel_upper = tricubic.grid_to_pos_scalar(pos_upper, nder=nder)
        assert abs(kernel_upper[:, :, :, 1, :]).max() > 1e-3
        assert abs(kernel_upper[:, :, :, 0, :]).max() < 1e-10
        assert abs(kernel[:, :, :, 0, :] - kernel_upper[:, :, :, 1, :]).max() < 1e-10
