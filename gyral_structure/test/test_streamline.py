"""
Tests the tractography
"""
from gyral_structure import basis, streamline, algorithm
import numpy as np
from numpy import testing


class SpiralBasis(basis.BasisFunc):
    """
    Basis function forming a spiral around the z-axis
    """
    potential = False
    nparams = 3
    scalar = False
    compute = (algorithm.Algorithm.numpy, )

    def __init__(self, ndim=3):
        self.ndim = ndim

    def get_func(self, positions, method):
        if method == algorithm.Algorithm.numpy:
            def func(input, inverse, derivative):
                if inverse:
                    return np.zeros((0, self.ndim))
                res = np.zeros(positions.shape)
                res[:, 0] = -positions[:, 1] * input[0]
                res[:, 1] = positions[:, 0] * input[1]
                if self.ndim == 3:
                    res[:, 2] = input[2]
                return res
            return func


def test_spiral_basis():
    for ndim in (2, 3):
        cb = SpiralBasis(ndim)
        pos = np.random.randn(5, ndim)
        res = cb.get_evaluator(pos, method='numpy')([1., 1., 0.])
        testing.assert_equal(res[:, 0], -pos[:, 1])
        testing.assert_equal(res[:, 1], pos[:, 0])
        if ndim == 3:
            testing.assert_equal(res[:, 2], 0)

        res = cb.get_evaluator(pos, method='numpy')([2., 3., 4.])
        testing.assert_equal(res[:, 0], -pos[:, 1] * 2)
        testing.assert_equal(res[:, 1], pos[:, 0] * 3)
        if ndim == 3:
            testing.assert_equal(res[:, 2], 4)


def test_spiral_streamline():
    cb = SpiralBasis()
    pos = np.zeros((1, 3))
    pos[:, 0] = 1.
    mask = np.ones((21, 21, 3), dtype='bool')
    affine = np.eye(4)
    affine[:-1, -1] = [-10, -10, -1]
    trk = streamline.Tracker(cb.param_evaluator([1., 1., 2e-5]), mask, affine, maxstep=1000, step_size=0.1)

    line = trk.track(pos, nstore=10)[0]
    print(line.shape)
    radius = np.sqrt(np.sum(line[:, :2] ** 2, -1))
    angle = np.arctan2(line[:, 1], line[:, 0])
    testing.assert_allclose(radius, 1., rtol=1e-3)
    testing.assert_allclose(angle[1:], (np.arange(1, 101) + np.pi) % (2 * np.pi) - np.pi, atol=1e-8, rtol=1e-2)
    testing.assert_allclose(line[1:, 2], np.arange(1, 101) * 2e-5, rtol=1e-5)
