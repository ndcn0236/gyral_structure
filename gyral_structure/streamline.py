"""Evaluate the streamlines running through a given field.
"""
from . import request, utils
import scipy as sp
from scipy import linalg
from mcot.surface import grid


def surface_startpoints(triangles, field_density):
    """Initializes streamlines at the surface based on the field density.

    :param triangles: (npos, ndim, 3) array representing the 3 corner points for `npos` triangles.
    :param field_density: (npos, ) array with the total number of streamlines crossing the surface
    :return: (N, ndim) array of starting points
    """
    field_density = sp.maximum(field_density, 0)
    total_density = sp.append([0], sp.around(sp.cumsum(field_density))).astype('i4')
    nstream = total_density[1:] - total_density[:-1]
    start_from = sp.zeros((total_density[-1], triangles.shape[1]))
    idx_pos = 0
    idx_nstream = 0
    for idx_max in range(1, 5):
        for rel_pos in request.VertexRequest._split(idx_max):
            use = nstream > idx_nstream
            if not use.any():
                break
            start_from[idx_pos: idx_pos + use.sum(), :] = sp.sum(triangles[use] * rel_pos, -1)
            idx_pos += use.sum()
            idx_nstream += 1
        if not use.any():
            break
    assert idx_pos == start_from.shape[0]
    return start_from


def volume_startpoints(charge_pos, charge_size, nstream):
    """Initializes streamlines at a charge distribution.

    Streamlines are generated randomly from a uniform distribution in the sphere

    :param charge_pos: centers of the charge spheres
    :param charge_size: size of the charge spheres
    :param nstream: float array; number of streamlines per charge sphere
    """
    npos, ndim = charge_pos.shape
    nstream = sp.asarray(nstream)
    if nstream.size != npos:
        nstream = sp.zeros(npos) + nstream
    total_density = sp.append(0, sp.cumsum(nstream))
    idx = sp.digitize(sp.arange(total_density[-1]), total_density)
    offset = sp.zeros((0, ndim))
    while offset.shape[0] < total_density[-1]:
        vals = sp.rand(total_density[-1], ndim) * 2 - 1
        use = sp.sum(vals ** 2, -1) < 1
        offset = sp.concatenate([offset, vals[use, :]], 0)
    return charge_pos[idx, :] + charge_size * offset[:npos, :]


class Tracker(object):
    def __init__(self, orient_func, stop_mask, affine_mask, step_size=0.1, maxstep=30000, stop_surfaces=()):
        """Creates a new tracking algorithm using Runga Kutta RK4

        :param orient_func: (npos, 3) array -> orientation; gives the fiber orientation at any point in space
        :param stop_mask: Nifti1Image; stop the streamline when it leaves the mask
        :param stop_affine: voxel -> mm transformation for the stop mask
        :param stop_conditions: List[(npos, 3) array, (npos, 3) array -> (npos, ) bool array]; tests whether a streamline should stop given its previous and current position
        """
        self.orient_func = orient_func
        self.stop_mask = stop_mask != 0
        self.affine_mask = affine_mask
        self.mm_to_stop = linalg.inv(affine_mask)
        self.step_size = step_size
        self.maxstep = maxstep
        self.stop_surface_intersects = tuple(grid.intersect(surf, self.stop_mask.shape, affine_mask)
                                             for surf in stop_surfaces)

    def track(self, start, nstore=10, inverse=False):
        ntrack = start.shape[0]
        streampos = sp.zeros((ntrack, self.maxstep // nstore + 1, 3))
        streampos[:, 0, :] = start
        current_pos = start.copy()

        running = sp.ones(ntrack, dtype='bool')
        for idx_step in range(self.maxstep):
            # take a step
            prev_pos = current_pos
            current_pos = self.update_pos(current_pos, inverse=inverse)
            streampos[running, idx_step // nstore + 1, :] = current_pos

            vox_pos = sp.around(self.mm_to_stop[:3, :3].dot(current_pos.T).T + self.mm_to_stop[:3, -1]).astype('i4')
            keep = sp.zeros(current_pos.shape[0], dtype='bool')
            in_range = ((vox_pos > 0) & (vox_pos < self.stop_mask.shape) &
                        (vox_pos >= 0)).all(-1)
            keep[in_range] = self.stop_mask[tuple(vox_pos[in_range].T)]
            if idx_step != 0:
                for surf_intersect in self.stop_surface_intersects:
                    idx_hit, pos_hit = surf_intersect.ray_intersect(prev_pos[keep], (current_pos - prev_pos)[keep],
                                                                    pos_inpr=0, max_dist=self.step_size)
                    tostop = idx_hit != -1
                    if tostop.any():
                        tostore = running.copy()
                        nolonger_keep = keep.copy()
                        nolonger_keep[keep] = tostop
                        tostore[running] = nolonger_keep
                        streampos[tostore, idx_step // nstore + 1, :] = pos_hit[tostop]
                        keep[nolonger_keep] = False
            running[running] = keep
            current_pos = current_pos[keep]
            if not running.any():
                break
        return [track[(track != 0).any(-1)] for track in streampos]

    def update_pos(self, current_pos, inverse=False):
        """Runge-Kutta RK4 update of position
        """
        def normed_vec(pos):
            vec = self.orient_func(pos)
            vec *= self.step_size / sp.sqrt(sp.sum(vec ** 2, -1)[..., None])
            if inverse:
                vec *= -1
            return vec
        k1 = normed_vec(current_pos)
        k2 = normed_vec(current_pos + k1 / 2)
        k3 = normed_vec(current_pos + k2 / 2)
        k4 = normed_vec(current_pos + k3)
        return current_pos + (k1 + 2 * k2 + 2 * k3 + k4) / 6.


def to_vtk(streamlines, color=(1., 0., 0.), linewidth=1, renderer=None, window=None, interact=False):
    """Convert streamlines to a vtkPolyData dataset for rendering in VTK

    The streamlines produced by Tracker.track can be used for plotting in VTK

    :param streamlines: list of individual streamlines (given as (N, 3) arrays)
    :param color: RGB color to show the streamlines; Could be one of:

        - (3, ) array to set the same color to all lines
        - (nlines, 3) array to set a different color to every line
        - (npoints, 3) to set a different color to each line segment

    :param renderer: vtkRenderer to which the lines will be added (default: new one is created
    :param window: vtkRenderWindow to which the renderer will be added (default: new one is created)
    :param interact: if True makes the VTK window interactive
    :return: VTK objects containing the streamlines
    :rtype: tuple[vtkPolyData, vtkRenderer, vtkRenderWindow]
    """
    import vtk
    import vtk.util.numpy_support as vtknp
    color = sp.array(color)
    assert color.shape[-1] == 3 or color.shape[-1] == 4

    all_points = sp.concatenate(streamlines, 0)
    points = vtk.vtkPoints()
    points.SetData(vtknp.numpy_to_vtk(all_points, deep=1))

    if color.ndim == 2:
        if color.shape[0] not in (all_points.shape[0], len(streamlines)):
            raise ValueError('Color array length should match number of points or number of streamlines')
        use_color = sp.ones(color.shape[0], dtype='bool')
    if color.ndim == 2 and color.shape[0] == streamlines[1].shape[0]:
        cellarr = vtk.vtkCellArray()
        idxpoint = 0
        for stream in streamlines:
            for _ in range(stream.shape[0] - 1):
                line = vtk.vtkPolyLine()
                line.GetPointIds().SetNumberOfIds(2)
                for i in range(2):
                    line.GetPointIds().SetId(i, i + idxpoint)
                idxpoint += 1
                cellarr.InsertNextCell(line)
            use_color[idxpoint - 1] = False
            idxpoint += 1
        assert idxpoint == points.shape[0]
    else:
        cellarr = vtk.vtkCellArray()
        idx_start = 0
        for stream in streamlines:
            line = vtk.vtkPolyLine()
            line.GetPointIds().SetNumberOfIds(stream.shape[0])
            for i in range(stream.shape[0]):
                line.GetPointIds().SetId(i, i + idx_start)
            cellarr.InsertNextCell(line)
            idx_start += stream.shape[0]

    pd = vtk.vtkPolyData()
    pd.SetPoints(points)
    pd.SetLines(cellarr)
    if color.ndim == 2:
        color = sp.around(color[use_color] * 255).astype('u1')
        pd.GetPointData().SetScalars(vtknp.numpy_to_vtk(color, deep=1))

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(pd)

    actor = vtk.vtkActor()
    if color.ndim == 1:
        actor.GetProperty().SetColor(*color)
    actor.GetProperty().SetLineWidth(linewidth)
    actor.SetMapper(mapper)

    if renderer is None:
        renderer = vtk.vtkRenderer()
    if window is None:
        window = vtk.vtkRenderWindow()
        window.SetSize(1000, 1000)
        window.AddRenderer(renderer)
    renderer.AddActor(actor)
    window.Render()

    if interact:
        interactor = vtk.vtkRenderWindowInteractor()
        interactor.SetRenderWindow(window)
        interactor.Start()
    return pd, renderer, window
