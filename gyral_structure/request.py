"""
Defines where the vector field (and hence the cost function) will be evaluated.

These are all derived from the :class:`FieldRequest`. The options are:

- :class:`PositionRequest`: provide (npos, ndim) array where the field will be evaluated. In most places, where a request is expected a (npos, ndim) array can be provided again, which will be converted into a :class:`PositionRequest` automatically
- :class:`VoxelRequest`: provide (npos, ndim) array with voxel centres and scalar or (ndim, ) array with voxel sizes. Will average the field over the voxel by evaluating the field in the center of `nsplit`^3 sub-voxels.
- :class:`VertexRequest`: provide (npos, ndim, 3) array with triangles. Will average the field over each triangle by evaluating the field in the center of `nsplit`^2 sub-triangles
- :class:`DerivativeRequest`: provide (npos, ndim) array and decide along which dimension the field derivative will be computed at those points.
- :data:`ParamRequest`: singular instance of request that can be used to simply return the parameters unaltered rather than computing a vector field

Under the hood to evaluate the field at multiple positions when cost functions are combined a :class:`MultRequest` is
used. The code there ensures that each request is only evaluated once for a given set of parameter even it is used
in multiple cost functions.
"""
import scipy as sp
import itertools
from itertools import zip_longest
from mcot.surface import CorticalMesh
import nibabel
import os
from numpy import linalg
from . import cuda
from scipy import spatial
from nibabel import gifti


class NoCombineError(Exception):
    """Raise when two requests can not be combined into a single request.
    """


class FieldRequest(object):
    """Generic object requesting a certain field to be computed.

    Can be passed to cost functions (see e.g. cost.VectorCost) to constrain the field solution.
    """
    positions = None

    def get_evaluator(self, basis, method=()):
        """Gets a function or matrix to compute the requested positions.

        :param basis: Basis function used to compute the field
        :param method: Computational method used to compute the field
        """
        return basis.get_evaluator(self, method=method)

    @property
    def npos(self, ):
        return self.positions.shape[0]

    @property
    def ndim(self, ):
        return self.positions.shape[1]

    def bounding_box(self, extent=0):
        """Computes a bounding box around all the positions required by this request.

        :param extent: float or (ndim, ) or (2, ndim) array with how much to extend the bounding box beyond the extreme points
        :return: ((min(x), max(x)), (min(y), max(y)), (min(z), max(z))) covering the full dataset
        """
        extent = abs(sp.broadcast_arrays(extent, sp.zeros((2, self.ndim)))[0])
        return sp.stack([self.positions.min(0) - extent[0], self.positions.max(0) + extent[1]], -1)

    def radius(self, ):
        """Maximal extent of the request from its center

        :return: scalar or (npos, ) array with the maximal distance from the center.
        """
        return sp.array(0.)

    def center(self, ):
        """Center of mass of the request

        :return: (npos, 3) array with the center of mass
        """
        return self.positions

    def shift(self, ):
        """Returns the shift needed so that the positions are centered on zero.
        """
        bb = self.bounding_box()
        return bb.sum(-1) / 2

    def scaling(self, as_scalar=False):
        """Returns the extent of the data in each dimension.

        :param as_scalar: returns the maximum across all dimensions if True
        """
        bb = self.bounding_box()
        res = bb[:, 1] - bb[:, 0]
        if as_scalar:
            return res.max()
        return res

    def as_volume(self, resolution, extent=0):
        """Defines a volume spanning the complete model area.

        :param resolution: float or (ndim, ) float array with the volume resolution
        :param extent: float or (ndim, ) or (2, ndim) float array with the distance in mm to go beyond the constraints in each dimension.
        :return: tuple with the array shape and affine transformation
        """
        minpos, maxpos = self.bounding_box(extent=extent).T
        shape = sp.ceil((maxpos - minpos) / resolution).astype('i4')
        affine = sp.eye(4)
        affine[range(self.ndim), range(self.ndim)] = resolution
        affine[:self.ndim, -1] = minpos
        return tuple(shape), affine

    def combine(self, other):
        raise NoCombineError()

    def flatten(self, ):
        return [self]

    def dict_to_field(self, field_dict):
        return field_dict[self]

    def field_to_dict(self, field):
        return {self: field}

    def wrap_qp(self, qp, Amat):
        P, q = qp
        A = Amat[self]
        qres = A.T.dot(q)
        Pres = A.T.dot(P.dot(A))
        return Pres, qres

    def split(self, ):
        """Splits the Request into positions where the basis function can be evaluated.

        :return: iterable yielding (positions, weights) pairs where the field requested is the weighted sum of the field at all the positions in the iterable
        """
        raise NotImplementedError("Splitting %s into individual positions is not defined" % self.__class__)

    def split_cuda(self, ):
        """Returns the code needed to evaluate the positions on the GPU

        :return: tuple with:

            - string containing the cuda code in a function named draw_pos
            - dictionary with a mapping from variable names to (signature, array) pairs
        """
        raise NotImplementedError("Splitting %s into individual positions is not defined" % self.__class__)

    def __repr__(self):
        return '%s(N=%i)' % (self.__class__.__name__, self.npos)

    def min_dist(self, points):
        """
        Estimates the minimal distance from a set of points to the requested volume.

        :param points: (N, ndim) array for N points in n-dimensional space
        :return: (N, ) array with the minimal distance for every point
        """
        dist = sp.zeros(points.shape[0]) + sp.inf
        for pos, w in self.split():
            tree = spatial.cKDTree(pos)
            d_min, _ = tree.query(points, distance_upper_bound=dist.max())
            replace = d_min < dist
            dist[replace] = d_min[replace]
        return dist


def _unique_indices(*values):
    """Gives every distinct position or triangle a unique index.
    """
    ident = sp.zeros(values[0].shape[0])
    for val in values:
        val_2d = val.reshape(val.shape[0], -1)
        multiplier = sp.randn(val_2d.shape[1])
        ident += sp.sum(val_2d * multiplier, -1)
    _, indices, reverse = sp.unique(ident, return_index=True, return_inverse=True)
    return indices, reverse


def _combined_vals(*requests, names=('positions', )):
    """Combines a number of different requests into a single request
    """
    bc_vals = [sp.broadcast_arrays(*tuple(getattr(req, name) for name in names)) for req in requests]
    full_vals = [sp.concatenate(vals, 0) for vals in zip(*bc_vals)]
    indices, reverse = _unique_indices(*tuple(full_vals))
    val_sorted = [val[indices] for val in full_vals]
    rev_index = []
    ixp = 0
    for req in requests:
        rev_index.append(reverse[ixp:ixp+req.npos])
        ixp += req.npos
    return val_sorted, rev_index


class PositionRequest(FieldRequest):
    """Computes the field at the provided positions.

    Can be passed to cost functions (see e.g. cost.VectorCost) to constrain the field solution.
    """
    def __init__(self, positions, method=()):
        self.positions = positions
        self.method = method

    def combine(self, other):
        """Combine with another PositionRequest to produce a single PositionRequest that evaluates both

        :param other: another PositionRequest
        :return: combined PositionRequest
        :raises: NoCombineError if failed to combine
        """
        if (not isinstance(other, PositionRequest) or
                self.method != other.method):
            raise NoCombineError()
        (all_loc, ), rev_index = _combined_vals(self, other)
        return PositionRequest(all_loc, method=self.method), rev_index[0], rev_index[1]

    def __getitem__(self, item):
        return PositionRequest(self.positions[item], self.method)

    def split(self, ):
        """Splits the Request into positions where the basis function can be evaluated.

        :return: iterable yielding (positions, weights) pairs where the field requested is the weighted sum of the field at all the positions in the iterable
        """
        yield self.positions, 1

    def split_cuda(self, ):
        """Returns the code needed to evaluate the positions on the GPU

        :return: tuple with:

            - string containing the cuda code in a function named draw_pos
            - dictionary with a mapping from variable names to (signature, array) pairs
        """
        code = """
        __device__ void set_state({dtype} *state, const int idx_pos, {draw_in}) {{
            state[0] = all_pos[idx_pos * {ndim} + 0];
            state[1] = all_pos[idx_pos * {ndim} + 1];
            if ( {ndim} == 3 ) state[2] = all_pos[idx_pos * {ndim} + 2];
        }}

        __device__ void draw_pos({dtype} *pos, {dtype} *weight, {dtype} *state, const int idx_split, {draw_in}) {{
            pos[0] = state[0];
            pos[1] = state[1];
            if ( {ndim} == 3 ) pos[2] = state[2];
        }}
        """
        return code, self.ndim, {'all_pos': (cuda.dtype + ' *all_pos', self.positions.flatten())}

    @property
    def total_split(self, ):
        return 1


class DerivativeRequest(FieldRequest):
    """Computes the Jacobian numerically at the provided positions.

    Can be passed to cost functions (see e.g. cost.VectorCost) to constrain the field solution.

    Provides an (npos, ndim, ndim) array where element [:, i, j] contains the derivative of the field component i with respect to the dimension j.
    """
    def __init__(self, positions, nder, method=(), epsilon=1e-6):
        self.positions = positions
        self.nder = tuple(nder)
        self.method = method
        self.epsilon = epsilon

    @staticmethod
    def all_derivatives(positions, total_der=1, method=()):
        """Computes all derivatives of a certain power.

        :param positions: (npos, ndim) array where the derivative will be calculated
        :param total_der: total derivative across all dimensions (defaults to a Jacobian).
        :param method: overrides the computational method used
        """
        return [DerivativeRequest(positions, nder, method) for nder in itertools.product(range(total_der + 1), repeat=positions.shape[1])
                if sp.sum(nder) == total_der]

    def combine(self, other):
        if not isinstance(other, DerivativeRequest) or self.nder != other.nder or self.method != other.method or self.epsilon != other.epsilon:
            raise NoCombineError()
        (all_loc, ), rev_index = _combined_vals(self, other)
        return DerivativeRequest(all_loc, self.nder, self.method, epsilon=self.epsilon), rev_index[0], rev_index[1]

    def radius(self, ):
        """Maximal extent of the request from its center

        :return: scalar or (npos, ) array with the maximal distance from the center.
        """
        return sp.array(self.epsilon)

    def __getitem__(self, item):
        return DerivativeRequest(self.positions[item], self.nder, self.method, self.epsilon)

    def _offset_weights(self, ):
        """Helper function to return the offsets and the weights with respect to the positions
        """
        use_epsilon = self.epsilon ** (1 / sp.sum(self.nder))

        def get_weights(nder):
            if nder == 0:
                return sp.ones(1, dtype='f4')
            res = sp.zeros(nder + 1, dtype='f4')
            subres = get_weights(nder - 1)
            res[:-1] -= subres
            res[1:] += subres
            return res / use_epsilon

        weights = sp.prod(sp.meshgrid(*tuple(get_weights(int(nd)) for nd in self.nder)), 0).flatten()
        all_offset = sp.stack(sp.meshgrid(*tuple(use_epsilon * (sp.arange(nd + 1) - nd / 2) for nd in self.nder)), -1).reshape(-1, len(self.nder))
        for offset, weight in zip(all_offset, weights):
            yield offset, weight

    def split_cuda(self, ):
        """Returns the code needed to evaluate the positions on the GPU

        :return: tuple with:

            - string containing the cuda code in a function named draw_pos
            - dictionary with a mapping from variable names to (signature, array) pairs
        """
        code = """
        __device__ void set_state({dtype} *state, const int idx_pos, {draw_in}) {{
            state[0] = all_pos[idx_pos * {ndim}];
            state[1] = all_pos[idx_pos * {ndim} + 1];
            if ( {ndim} == 3 ) state[2] = all_pos[idx_pos * {ndim} + 2];
        }}

        __device__ void draw_pos({dtype} *pos, {dtype} *weight, {dtype} * state, const int idx_split, {draw_in}) {{
            weight[0] = all_weights[idx_split];
            pos[0] = state[0] + offset[idx_split * {ndim}];
            pos[1] = state[1] + offset[idx_split * {ndim} + 1];
            if ( {ndim} == 3 ) pos[2] = state[2] + offset[idx_split * {ndim} + 2];
        }}
        """
        offsets = sp.array([offset for offset, _ in self._offset_weights()])
        weights = sp.array([weight for _, weight in self._offset_weights()])
        return code, self.ndim, {'all_pos': (cuda.dtype + ' *all_pos', self.positions.flatten()),
                                 'offset': (cuda.dtype + ' *offset', offsets.flatten()),
                                 'all_weights': (cuda.dtype + ' *all_weights', weights.flatten())}

    def split(self, ):
        """Splits the Request into positions where the basis function can be evaluated.

        :return: iterable yielding (positions, weights) pairs where the field requested is the weighted sum of the field at all the positions in the iterable
        """
        for offset, weight in self._offset_weights():
            yield self.positions + offset, weight

    @property
    def total_split(self, ):
        return sp.prod([nd + 1 for nd in self.nder])


class VoxelRequest(FieldRequest):
    """Computes the field averaged over one or more voxels.

    Can be passed to cost functions (see e.g. cost.VectorCost) to constrain the field solution.
    """
    def __init__(self, positions, size=1, method=(), nsplit=5):
        self.positions = positions
        self.size = sp.asarray(size)
        if self.size.shape == (self.npos, ) and self.npos != self.ndim:
            self.size = self.size[:, None]
        self.method = method
        self.nsplit = nsplit

    @property
    def nsplit(self, ):
        return self._nsplit

    @nsplit.setter
    def nsplit(self, value):
        if value <= 0:
            raise ValueError("Number of splits should be at least 1 for VoxelRequest")
        self._nsplit = value

    def combine(self, other):
        """Combine with another VoxelRequest to produce a single VoxelRequest that evaluates both

        :param other: another VoxelRequest
        :return: combined VoxelRequest
        :raises: NoCombineError if failed to combine
        """
        if not isinstance(other, VoxelRequest) or self.method != other.method or self.nsplit != other.nsplit:
            raise NoCombineError()
        (all_loc, all_size), rev_index = _combined_vals(self, other, names=('positions', 'size'))
        return VoxelRequest(all_loc, all_size, self.method, nsplit=self.nsplit), rev_index[0], rev_index[1]

    def __getitem__(self, item):
        pos, size = sp.broadcast_arrays(self.positions, self.size)
        return VoxelRequest(pos[item], size[item], self.method, nsplit=self.nsplit)

    def split(self, ):
        """Splits the Request into positions where the basis function can be evaluated.

        :return: iterable yielding (positions, weights) pairs where the field requested is the weighted sum of the field at all the positions in the iterable
        """
        weight = self.nsplit ** -self.ndim
        rel_pos1d = sp.linspace(-0.5, 0.5, 2 * self.nsplit + 1)[1:-1:2]
        for offset_raw in itertools.product(rel_pos1d, repeat=self.ndim):
            offset = offset_raw * self.size
            yield self.positions + offset, weight

    def split_cuda(self, ):
        """Returns the code needed to evaluate the positions on the GPU

        :return: tuple with:

            - string containing the cuda code in a function named draw_pos
            - dictionary with a mapping from variable names to (signature, array) pairs
        """
        code = ("""
        __device__ void set_state({dtype} *state, const int idx_pos, {draw_in}) {{
            state[0] = all_pos[idx_pos * {ndim}];
            state[1] = size[idx_pos * {ndim}];
            state[2] = all_pos[idx_pos * {ndim} + 1];
            state[3] = size[idx_pos * {ndim} + 1];
            if ( {ndim} == 3 ) {{
                state[4] = all_pos[idx_pos * {ndim} + 2];
                state[5] = size[idx_pos * {ndim} + 2];
            }}
        }}

        __device__ void draw_pos({dtype} *pos, {dtype} *weight, {dtype} *state, const int idx_split, {draw_in}) {{
            int nsplit = %i; """ % self.nsplit) + """
            int xoff = idx_split % nsplit;
            int yoff = (idx_split / nsplit) % nsplit;
            int zoff = (idx_split / nsplit) / nsplit;
            pos[0] = state[0] + state[1] * ((2.0 * xoff + 1.0) / (2.0 * nsplit) - 0.5);
            pos[1] = state[2] + state[3] * ((2.0 * yoff + 1.0) / (2.0 * nsplit) - 0.5);
            if ( {ndim} == 3 ) pos[2] = state[4] + state[5] * ((2.0 * zoff + 1.0) / (2.0 * nsplit) - 0.5);
        }}
        """
        full_pos, full_size = sp.broadcast_arrays(self.positions, self.size)
        return code, self.ndim * 2, {'all_pos': (cuda.dtype + ' *all_pos', full_pos.flatten()),
                                     'size': (cuda.dtype + ' *size', full_size.flatten())}

    @property
    def total_split(self, ):
        return self.nsplit ** 3

    def bounding_box(self, extent=0):
        """Computes a bounding box around all the positions required by this request.

        :param extent: float or (ndim, ) or (2, ndim) array with how much to extend the bounding box beyond the extreme points
        :return: ((min(x), max(x)), (min(y), max(y)), (min(z), max(z))) covering the full dataset
        """
        extent = abs(sp.broadcast_arrays(extent, sp.zeros((2, self.ndim)))[0])
        return sp.stack([(self.positions - self.size / 2).min(0) - extent[0],
                         (self.positions + self.size / 2).max(0) + extent[1]], -1)

    def radius(self, ):
        """Maximal extent of the request from its center

        :return: scalar or (npos, ) array with the maximal distance from the center.
        """
        if self.size.ndim == 0:
            return self.size * sp.sqrt(self.ndim) / 2.
        if self.size.ndim == 2 and self.size.shape[-1] == self.ndim:
            return sp.sqrt((self.size ** 2).sum(-1)) / 2.
        else:
            return self.size.ravel() * sp.sqrt(self.ndim) / 2.


class VertexRequest(FieldRequest):
    """Computes the field averaged over one or more vertices.

    Can be passed to cost functions (see e.g. cost.VectorCost) to constrain the field solution.
    """
    def __init__(self, triangles, method=(), nsplit=2):
        """Computes the field averaged over several triangles.

        :param triangles: (npos, ndim, 3) array representing the 3 corner points for `npos` triangles.
        :param method: which algorithm to use when evaluating the vertex fields
        :param nsplit: how often to subdivide the triangles into 6 equal-sized sub-triangles during averaging (so that each field is the average of 6 ** nsplit sub-fields).
        """
        self.triangles = triangles
        if self.triangles.shape[2] != 3:
            raise ValueError("Last dimension of triangles array should represent the 3 corner points")
        self.method = method
        self.nsplit = nsplit

    @property
    def npos(self, ):
        return self.triangles.shape[0]

    @property
    def ndim(self, ):
        return self.triangles.shape[1]

    @property
    def nsplit(self, ):
        return self._nsplit

    @nsplit.setter
    def nsplit(self, value):
        if value <= 0:
            raise ValueError("Number of splits should be at least 1 for VertexRequest")
        self._nsplit = value

    @property
    def total_split(self, ):
        return self.nsplit ** 2

    def bounding_box(self, extent=0):
        """Computes a bounding box around all the positions required by this request.

        :param extent: float or (ndim, ) or (2, ndim) array with how much to extend the bounding box beyond the extreme points
        :return: ((min(x), max(x)), (min(y), max(y)), (min(z), max(z))) covering the full dataset
        """
        extent = abs(sp.broadcast_arrays(extent, sp.zeros((2, self.ndim)))[0])
        return sp.stack([self.triangles.min((0, 2)) - extent[0],
                         self.triangles.max((0, 2)) + extent[1]], -1)

    def combine(self, other):
        """Combine with another VertexRequest to produce a single VertexRequest that evaluates both

        :param other: another VertexRequest
        :return: combined VertexRequest
        :raises: NoCombineError if failed to combine
        """
        if not isinstance(other, VertexRequest) or self.method != other.method or self.nsplit != other.nsplit:
            raise NoCombineError()
        (all_triangles, ), rev_index = _combined_vals(self, other, names=('triangles', ))
        return VertexRequest(all_triangles, self.method, nsplit=self.nsplit), rev_index[0], rev_index[1]

    def __getitem__(self, item):
        return VertexRequest(self.triangles[item], self.method, nsplit=self.nsplit)

    def size(self, ):
        """Computes the size of the surface elements
        """
        offset = self.triangles[:, :, 1:] - self.triangles[:, :, 0, None]
        if self.ndim == 2:
            cr = offset[..., 0, 0] * offset[..., 1, 1] - offset[..., 0, 1] * offset[..., 1, 0]
            return abs(cr) / 2
        else:
            cr = sp.cross(offset[..., 0], offset[..., 1])
            return sp.sqrt((cr ** 2).sum(-1)) / 2

    def split(self, ):
        """Splits the Request into positions where the basis function can be evaluated.

        :return: iterable yielding (positions, weights) pairs where the field requested is the weighted sum of the field at all the positions in the iterable
        """
        size = self.nsplit ** -2
        for offset in self._split(self.nsplit):
            pos = (self.triangles * offset).sum(-1)
            yield pos, size

    def split_cuda(self, ):
        """Returns the code needed to evaluate the positions on the GPU

        :return: tuple with:

            - string containing the cuda code in a function named draw_pos
            - dictionary with a mapping from variable names to (signature, array) pairs
        """
        code = """
        __device__ void set_state({dtype} *state, const int idx_pos, {draw_in}) {{
            for (int dim = 0; dim < {ndim}; dim ++) {{
                for (int idx = 0; idx < 3; idx ++) {{
                    state[idx * {ndim} + dim] = triangles[idx_pos * {ndim} * 3 + dim * 3 + idx];
                }}
            }}
        }}

        __device__ void draw_pos({dtype} *pos, {dtype} *weight, {dtype} *state, int idx_split, {draw_in}) {{
            {dtype} split_val;
            for (int dim = 0; dim < {ndim}; dim ++) pos[dim] = 0;
            for (int idx = 0; idx < 3; idx ++) {{
                split_val = split[idx_split * 3 + idx];
                for (int dim = 0; dim < {ndim}; dim ++) {{
                    pos[dim] += state[idx * {ndim} + dim] * split_val;
                }}
            }}
        }}
        """
        return code, self.ndim * 3, {'triangles': (cuda.dtype + ' *triangles', self.triangles.flatten()),
                                     'split': (cuda.dtype + ' *split', sp.array(list(self._split(self.nsplit))).flatten())}

    @staticmethod
    def _split(nsplit=1, vertices=None):
        if vertices is None:
            vertices = sp.eye(3)
        base = vertices[:, 0]
        off_v1 = (vertices[:, 1] - vertices[:, 0]) / nsplit
        off_v2 = (vertices[:, 2] - vertices[:, 1]) / nsplit

        for idx_down in range(nsplit):
            upper_start = base + off_v1 * idx_down
            lower_start = upper_start + off_v1

            walk_upper = 0
            walk_lower = 0
            while walk_lower != idx_down + 1:
                if walk_lower == walk_upper:
                    yield (upper_start + 2 * lower_start + (walk_upper + 2 * walk_lower + 1) * off_v2) / 3
                    walk_lower += 1
                elif walk_lower == walk_upper + 1:
                    yield (2 * upper_start + lower_start + (2 * walk_upper + walk_lower + 1) * off_v2) / 3
                    walk_upper += 1
                else:
                    raise RuntimeError('walk_lower should be equal or 1 bigger than walk_upper')

    def radius(self, ):
        """Maximal extent of the request from its center

        :return: scalar or (npos, ) array with the maximal distance from the center.
        """
        offset = self.triangles - self.center()[..., None]
        radius = (offset ** 2).sum(-2)
        return sp.sqrt(radius.max())

    def center(self, ):
        """Center of mass of the request

        :return: (npos, ndim) array with the center of mass
        """
        return self.triangles.mean(-1)


class _ParamRequestClass(FieldRequest):
    """Returns the model parameters rather than a specific field.
    """
    def get_evaluator(self, basis, method=()):
        from .basis.evaluator import IdentityEvaluator
        return IdentityEvaluator(basis, self, method=method)

    @property
    def npos(self, ):
        raise ValueError("Number of positions not defined for parameter request")

    @property
    def ndim(self, ):
        raise ValueError("Number of dimensions not defined for parameter request")

    def __repr__(self):
        return 'ParamRequest'

ParamRequest = _ParamRequestClass()


class MultRequest(FieldRequest):
    """Container containing multiple requests

    The actual requests are stored in the attribute `requests`
    """
    def __init__(self, requests):
        """Represents one or more requests.
        """
        self.requests = [req if isinstance(req, FieldRequest) else
                         PositionRequest(req) for req in requests]

    @property
    def npos(self, ):
        return sp.sum([req.npos for req in self.requests])

    @property
    def ndim(self, ):
        return self.requests[0].ndim

    def bounding_box(self, extent=0):
        """Computes a bounding box around all the positions required by this request.

        :param extent: float or (ndim, ) or (2, ndim) array with how much to extend the bounding box beyond the extreme points
        :return: ((min(x), max(x)), (min(y), max(y)), (min(z), max(z))) covering the full dataset
        """
        all_bb = sp.array([req.bounding_box(extent=extent) for req in self.requests])
        return sp.stack([all_bb[:, :, 0].min(0),
                         all_bb[:, :, 1].max(0)], -1)

    def flatten(self, ):
        res = []
        for req in self.requests:
            res.extend(req.flatten())
        return res

    def dict_to_field(self, field_dict):
        return (req.dict_to_field(field_dict) for req in self.requests)

    def field_to_dict(self, field):
        res = {}
        for req, f in zip_longest(self.requests, field):
            for sub_req, value in req.field_to_dict(f).items():
                if sub_req in res:
                    res[sub_req] = res[sub_req] + value
                else:
                    res[sub_req] = value
        return res

    def wrap_qp(self, qp, Amat):
        P = None
        for req, sub_qp in zip_longest(self.requests, qp):
            addition = req.wrap_qp(sub_qp, Amat)
            if P is None:
                P, q = addition
            else:
                P += addition[0]
                q += addition[1]
        return P, q

    def __repr__(self):
        return '%s([%s])' % (self.__class__.__name__, ', '.join(self.requests))

    def center(self, ):
        """Center of mass of the request

        :return: (npos, 3) array with the center of mass
        """
        return sp.concatenate([req.center() for req in self.requests], 0)

    def radius(self, ):
        """Maximal extent of the request from its center

        :return: scalar or (npos, ) array with the maximal distance from the center.
        """
        radii = []
        for req in self.requests:
            new_rad = sp.asarray(req.radius())
            if new_rad.ndim == 0:
                radii.append(sp.zeros(req.npos) + new_rad)
            else:
                radii.append(new_rad)
        return sp.concatenate(radii, 0)

    def min_dist(self, points):
        """
        Estimates the minimal distance from a set of points to the requested volume.

        :param points: (N, ndim) array for N points in n-dimensional space
        :return: (N, ) array with the minimal distance for every point
        """
        points = sp.asarray(points)
        dist = sp.zeros(points.shape[0]) + sp.inf
        for req in self.requests:
            md = req.min_dist(points)
            replace = md < dist
            dist[replace] = md[replace]
        return dist


def read_surface(gifti_surf, gifti_mask=None, nsplit=3, method=()):
    """Requests the field to be computed across the surface.

    :param gifti_surf: .surf.gii file describing the cortical surface (typically the white/gray matter boundary)
    :type gifti_surf: Union[str, gifti.GiftiImage, CorticalMesh]
    :param gifti_mask: .shape.gii file used to mask the surface (only non-zero vertices will be included; optional)
    :type gifti_surf: str
    :param nsplit: number of sub-triangles to split each triangle into along each dimension
    :param method: Algorithm to use when evaluating the basis function at the surface
    :return: Request object asking for the field to be computed across the surface
    :rtype: VertexRequest
    """
    if not isinstance(gifti_surf, CorticalMesh):
        surf = CorticalMesh.read(gifti_surf)
    else:
        surf = gifti_surf
    if gifti_mask is not None:
        surf = surf[gifti.read(gifti_mask).darrays[0].data != 0]
    triangles = sp.transpose(surf.vertices[:, surf.faces], (2, 0, 1)).astype(float)
    return VertexRequest(triangles, nsplit=nsplit, method=method)


def read_volume(nifti_vol, average=True, nsplit=5, in_mm=True, method=()):
    """Requests the field to be computed in all voxels in a volume.

    :param nifti_vol: NIFTI image; any non-zero voxels will be included in the request
    :type nifti_vol: Union[str, tuple, nibabel.analyze.SpatialImage]
    :param average: set to true to average over the voxel (otherwise the central value is taken).
    :param nsplit: number of sub-voxels to split each voxel into along each dimension (has no effect if average is False)
    :param in_mm: returns the positions/sizes in mm rather than voxel coordinates
    :param method: Algorithm to use when evaluating the basis function at the surface
    :return: Request object asking the field to be computed throughout the ROI (which can be passed on to cost functions)
    """
    if isinstance(nifti_vol, str):
        vol = nibabel.load(nifti_vol)
        arr, affine = vol.get_data(), vol.affine
    elif isinstance(nifti_vol, tuple):
        arr, affine = nifti_vol
    elif isinstance(nifti_vol, nibabel.analyze.SpatialImage):
        arr, affine = nifti_vol.get_data(), nifti_vol.affine
    else:
        raise ValueError("Input volume should be a filename or a nibabel SpatialImage (e.g. Nifti1Image)")
    if arr.ndim not in (2, 3):
        raise ValueError("Input mask should be 2D or 3D")
    pos_vox = sp.stack(sp.where(arr != 0), -1)
    if in_mm:
        pos = sp.dot(affine[:-1, :-1], pos_vox.reshape(-1, arr.ndim).T).T + affine[:-1, -1]
        size = abs(affine).sum(0)[:-1]
    else:
        pos = pos_vox
        size = 1

    if average:
        return VoxelRequest(pos, size, method=method, nsplit=nsplit)
    else:
        return PositionRequest(pos, method=method)


def read_bedpostx(bedpostx_dir, min_volume=0.05, mask=None, average=True, in_mm=True, method=()):
    """Reads in the crossing fibers from `bedpostx_dir` with a minimum volume fraction `min_volume`

    :param bedpostx_dir: name of bedpostx directory
    :param min_volume: minimum volume fraction to include the crossing fiber
    :param mask: only include voxels with a non-zero value in the mask (has to be in the same space as the diffusion data)
    :param average: set to true to average over the voxel (otherwise the central value is taken).
    :param in_mm: if True request positions in mm rather than voxel space
    :param method: Algorithm to use when evaluating the basis function at the surface
    :return: request to compute the field and (N vectors, N positions, N dimensions) array of target dyads
    """
    if mask is None:
        mask = nibabel.load(os.path.join(bedpostx_dir, 'nodif_brain_mask.nii.gz')).get_data()
    mask = mask != 0
    res = []
    for idx_dyad in range(1, 4):
        if os.path.isfile(os.path.join(bedpostx_dir, 'dyads%i.nii.gz' % idx_dyad)):
            img = nibabel.load(os.path.join(bedpostx_dir, 'dyads%i.nii.gz' % idx_dyad))
            field = img.get_data()[mask, :]
            volume_fraction = nibabel.load(os.path.join(bedpostx_dir, 'mean_f%isamples.nii.gz' % idx_dyad)).get_data()[mask]
            field[volume_fraction < min_volume, :] = 0
            if in_mm and linalg.det(img.get_affine()) < 0:
                field[..., 0] *= -1
            res.append(field)
        elif idx_dyad == 1:
            raise IOError("Not even the first dyad file was found in %s" % bedpostx_dir)
    affine = img.get_affine()
    req = read_volume(nibabel.Nifti1Image(mask.astype('i4'), affine), average=average, in_mm=in_mm, method=method)
    return req, sp.stack(res, 0)
