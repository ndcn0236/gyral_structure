"""
Contains a large number of functions to evaluate the basis functions on the GPU

For pre-computed matrix multiplication a kernel to evaluate the vector field can be gained by providing the matrix to

- :func:`sparse_mult_prepare` for sparse matrices
- :func:`dense_mult_prepare` for dense matrices

On the fly computation of dense matrices can be implemented through :class:`CudaMatrixMult`
"""
import warnings
available = True
try:
    import pycuda
    from pycuda._driver import RuntimeError
except ImportError:
    available = False
if available:
    try:
        from pycuda.compiler import SourceModule
        from pycuda import gpuarray
        import pycuda.driver as drv
    except RuntimeError as e:
        warnings.warn('Failed to initialize pycuda: {}'.format(e))
        available = False
from functools import lru_cache

import scipy as sp

# very ugly hack to be able to run pycuda on python 3
import sys
sys.maxint = sys.maxsize

nthreads = 128
dtype = 'double'


def correct_type(arr):
    """If needed return converted array for transfer to GPU.

    Arrays of the correct type will be returned unaltered.
    All float arrays will be made single/double precision (depending on cuda.dtype)
    All integer arrays will be made short ('i4')

    :param arr: input array
    :return: either `arr` itself if it is of the correct type or a converted `arr` to the correct type
    """
    if arr.dtype.name[:5] == 'float':
        new_dt = 'f4' if dtype == 'float' else 'f8'
    elif arr.dtype.name[:3] == 'int':
        new_dt = 'i4'
    else:
        return arr
    if new_dt != arr.dtype:
        return arr.astype(new_dt)
    return arr


def to_gpu_correct(arr):
    """Returns the GPUArray pointing to the input `arr` on the GPU

    This algorithm ensures that the `array` has the correct type (converting if if necessary) and then moves the data to the GPu.

    :param arr: numpy data array
    :return: pointer to the data on the GPU
    """
    if isinstance(arr, gpuarray.GPUArray):
        return arr
    arr = sp.atleast_1d(arr)
    return gpuarray.to_gpu(correct_type(arr))


# Sparse matrix multiplication

if available:
    _sparse_mult_mod_scalar = lambda nrow: """
    __global__ void sparse_matrix_mult({0}* result,
                                   const {0}* input,
                                   const int* indptr,
                                   const int* indices,
                                   const {0}* data)
    {{
        int idx_row = threadIdx.x + blockDim.x * blockIdx.x;
        if (idx_row < {1}){{
            int start = indptr[idx_row];
            int end = indptr[idx_row + 1];
            {0} value = 0.0f;
            for (int col=start; col < end; col ++){{
                value += input[indices[col]] * data[col];
            }}
            result[idx_row] = value;
        }}
    }}
    """.format(dtype, nrow)

    _sparse_mult_mod_vector = lambda nrow: """
    __global__ void sparse_matrix_mult({0}* result,
                                   const {0}* input,
                                   const int* indptr,
                                   const int* indices,
                                   const {0}* data)
    {{
        int thread_id = threadIdx.x + blockDim.x * blockIdx.x;
        int idx_row = thread_id / 32;
        int lane = thread_id - (idx_row * 32);

        if (idx_row < {1}){{
            int start = indptr[idx_row];
            int end = indptr[idx_row + 1];

            {0} value = 0.0f;
            for (int col=start + lane; col < end; col += 32){{
                value += input[indices[col]] * data[col];
            }}

            value += __shfl_down(value, 16);
            value += __shfl_down(value, 8);
            value += __shfl_down(value, 4);
            value += __shfl_down(value, 2);
            value += __shfl_down(value, 1);
            if (lane == 0)
                result[idx_row] = value;
        }}
    }}
    """.format(dtype, nrow)



def _sparse_mult_to_gpu(sparse):
    """Moves a sparse matrix to the GPU so that it can be used in sparse_mult_cpu.
    """
    csr = sparse.tocsr()
    return [to_gpu_correct(arr) for arr in (csr.indptr, csr.indices, csr.data)]


def sparse_mult_prepare(sparse):
    """Returns a function that does the GPU multiplication for the provided sparse matrix.
    """
    gpu_sparse = _sparse_mult_to_gpu(sparse)
    M = sparse.shape[0]
    output = gpuarray.zeros((M, ), dtype='f4' if dtype == 'float' else 'f8')
    if len(gpu_sparse[-1]) == 0:
        def empty_sparse_mult(vec_in):
            return output
        return empty_sparse_mult

    gpu_func = cached_func(_sparse_mult_mod_vector(gpu_sparse[0].size - 1), 'sparse_matrix_mult')
    input = gpuarray.zeros((sparse.shape[1], ), dtype='f4' if dtype == 'float' else 'f8')
    stream = drv.Stream()
    def sparse_mult(vec_in):
        """Does a sparse matrix multiplication.

        :param vec_in: (N, ) input vector
        :return: (M, ) output vector
        """
        input.set_async(vec_in.astype(input.dtype))
        gpu_func(output, input, *gpu_sparse,
                      block=(nthreads, 1, 1), grid=(int(sp.ceil(M / nthreads * 32)), 1, 1))
        return output
    sparse_mult.stream = stream
    return sparse_mult


# Dense matrix multiplication

_dense_mult_mod = lambda shape: """
__global__ void dense_matrix_mult({0}* __restrict__ result,
                               const {0}* __restrict__ input,
                               const {0}* __restrict__ array)
{{
    int idx_out = threadIdx.x + blockDim.x * blockIdx.x;
    {0} value = 0.0f;
    if (idx_out < {1}) {{
        for (int idx_in=0; idx_in < {2}; ++idx_in){{
            value += input[idx_in] * array[idx_out + idx_in * {1}];
        }}
        result[idx_out] = value;
    }}
}}
""".format(dtype, *shape)


def dense_mult_prepare(dense):
    """Prepares a dense matrix multiplication.

    :param dense: (M, N) array
    :return: function that does the dense matrix multiplication
    """
    func = cached_func(_dense_mult_mod(dense.shape), 'dense_matrix_mult')
    gpu_dense = to_gpu_correct(dense.T.flatten())
    input = gpuarray.zeros((dense.shape[1], ), dtype='f4' if dtype == 'float' else 'f8')
    output = gpuarray.zeros((dense.shape[0], ), dtype='f4' if dtype == 'float' else 'f8')
    stream = drv.Stream()
    def dense_mult(vec_in):
        """Returns the result from dense matrix multiplication.

        Dense matrix multiplication will be done on the GPU
        :param vec_in: (M, ) input vector
        :return: (N, ) output vector
        """
        input.set_async(vec_in.astype(input.dtype))
        func(output, input, gpu_dense,
             block=(nthreads, 1, 1), grid=(int(sp.ceil(dense.shape[0] / nthreads)), 1, 1))
        return output
    dense_mult.stream = stream
    return dense_mult

# Wrapping basis functions

_wrap_get_element_code = r"""
__global__ void matrix_mult_1d({dtype} *field, {dtype} *parameters{params_in})
{{
    int idx_pos = threadIdx.x + blockDim.x * blockIdx.x;
    {dtype} single_value;
    {dtype} value = 0.0f;
    if (idx_pos < {npos}){{
        for (int idx_param=0; idx_param < {nparams}; idx_param++) {{
            get_element(&single_value, idx_pos, idx_param{params_out});
            value += single_value * parameters[idx_param];
        }}
        field[idx_pos] = value;
    }}
}}

__global__ void matrix_mult_1d_invert({dtype} *parameters, {dtype} *field{params_in})
{{
    int idx_param = threadIdx.x + blockDim.x * blockIdx.x;
    {dtype} single_value;
    {dtype} value = 0.0f;
    if (idx_param < {nparams}){{
        for (int idx_pos=0; idx_pos < {npos}; idx_pos++) {{
            get_element(&single_value, idx_pos, idx_param{params_out});
            value += single_value * field[idx_pos];
        }}
        parameters[idx_param] = value;
    }}
}}

__global__ void matrix_mult_nd({dtype} *field, {dtype} *parameters{params_in})
{{
    int idx_pos = threadIdx.x + blockDim.x * blockIdx.x;
    {dtype} single_value[{ndim}];
    {dtype} value[{ndim}];
    for (int idx_dim=0; idx_dim < {ndim}; idx_dim++) {{
        value[idx_dim] = 0.0f;
    }}
    if (idx_pos < {npos}){{
        for (int idx_param=0; idx_param < {nparams}; idx_param++) {{
            get_element(single_value, idx_pos, idx_param{params_out});
            for (int idx_dim=0; idx_dim < {ndim}; idx_dim++) {{
                value[idx_dim] += single_value[idx_dim] * parameters[idx_param];
            }}
        }}
        for (int idx_dim=0; idx_dim < {ndim}; idx_dim++) {{
            field[idx_pos * {ndim} + idx_dim] = value[idx_dim];
        }}
    }}
}}

__global__ void matrix_mult_nd_invert({dtype} *parameters, {dtype} *field{params_in})
{{
    int idx_param = threadIdx.x + blockDim.x * blockIdx.x;
    {dtype} single_value[{ndim}];
    if (idx_param < {nparams}){{
        parameters[idx_param] = 0.0;
        for (int idx_pos=0; idx_pos < {npos}; idx_pos++) {{
            get_element(single_value, idx_pos, idx_param{params_out});
            for (int idx_dim=0; idx_dim < {ndim}; idx_dim++) {{
                parameters[idx_param] += single_value[idx_dim] * field[idx_pos * {ndim} + idx_dim];
            }}
        }}
    }}
}}
"""

def wrap_get_element(code):
    """Defines kernel functions if a `get_element` CUDA function has been defined.

    """
    include = """
#include <math.h>
#include <stdio.h>
"""
    return include + code + _wrap_get_element_code


class CudaMatrixMult(object):
    def __init__(self, code, npos, ndim, nparams, values=None, constants=None, params=None, scalar=True,
                 update_pos=None):
        """
        Compiles the matrix multiplication GPU kernel

        :param code: string with CUDA code to evaluate single element of the dense matrix
        :param npos: number of positions where the field should be evaluated
        :param ndim: number of dimensions (2 or 3)
        :param nparams: number of parameters in the cost function
        :param values: dict of keywords that will be formatted into the code string
        :param constants: dict of read-only arrays that should be uploaded to the GPU
        :param params: dict of read/write arrays that should be uploaded to the GPU
        :param scalar: if True each dimension of the vector field is computed independently
        :param update_pos: function to update the positions. Gets passed in all the parameter GPU Arrays and the new positions
        """
        import pycuda.autoinit
        self.part_code = code
        self.nvar = (npos, ndim, nparams)
        if constants is None:
            constants = {}
        self.constants = constants
        if values is None:
            values = {}
        self.values = values
        if params is None:
            params = {}
        self.params = params
        self.scalar = scalar
        self.param_arrs = []
        for name, (identifier, arr) in self.params.items():
            self.param_arrs.append(to_gpu_correct(arr))
        if update_pos is not None:
            self.update_pos = lambda new_positions: update_pos(
                {name: arr for name, arr in zip(self.params, self.param_arrs)}, new_positions)

        self.func = {}

    def code(self, derivative, inverse):
        """
        Formats the code with the `values` embedded
        """
        kwargs = dict(self.values, npos=self.nvar[0], ndim=self.nvar[1], nparams=self.nvar[2], derivative=derivative,
                      inverse=inverse, nposd=self.nvar[0] * self.nvar[1], nparamsd=self.nvar[0] * self.nvar[2], dtype=dtype)
        kwargs['params_in'] = ''
        kwargs['params_out'] = ''
        for name, (identifier, arr) in self.params.items():
            kwargs['params_in'] += ', ' + identifier
            kwargs['params_out'] += ', ' + name
        return self.part_code.format(**kwargs)

    def compile_func(self, derivative, inverse):
        """
        Compiles the matrix multiplication kernel on the GPU

        :param derivative: whether to compute the derivative of the field (-1 for no derivative)
        :param inverse: if True go from vector field derivatives to parameter derivatives
        :return: function that calls the GPU kernel
        """
        code = self.code(derivative, inverse)
        sm = cached_source_module(code)
        for name, value in self.constants.items():
            gpu_val = sm.get_global(name)[0]
            drv.memcpy_htod(gpu_val, correct_type(value))
        key = (derivative, inverse)
        if self.scalar:
            self.func[key] = cached_func(code, 'matrix_mult_1d_invert' if inverse else 'matrix_mult_1d')
        else:
            self.func[key] = cached_func(code, 'matrix_mult_nd_invert' if inverse else 'matrix_mult_nd')
        return self.func[key]

    def __call__(self, parameters, inverse=False, derivative=-1):
        """
        Runs matrix multiplication on the GPU

        :param parameters: (nparams, ) array if not inverse else (npos, ndim) array
        :param inverse: if True go from vector field derivatives to parameter derivatives rather than other way around
        :param derivative: whether to evaluate derivative of the field (default: no derivative)
        :return: GPUArray with the result
        """
        if not (derivative, inverse) in self.func:
            self.compile_func(derivative, inverse)
        f = self.func[(derivative, inverse)]
        npos, ndim, nparams = self.nvar
        if inverse:
            result = gpuarray.zeros(nparams, dtype=dtype)
        else:
            result = gpuarray.zeros(npos if self.scalar else (npos, ndim), dtype=dtype)

        f(result, to_gpu_correct(parameters), *self.param_arrs,
          block=(nthreads, 1, 1), grid=(int(sp.ceil(result.shape[0] / nthreads)), 1, 1))
        return result


# Grid convolution

class GridConvolve(object):
    """Convolves values in a grid with 4x4x4 convolutions.
    """
    def __init__(self, param_to_field, field_to_param, kernels):
        """Sets up a new grid convolution.

        :param param_to_field: (Np, 4, 4, 4) int array; relates each parameter index to the field in a 4x4x4 neighbourhood; -1 for unused field indices
        :param field_to_param: (Nf, 4, 4, 4) int array; relates each field index to the parameters in a 4x4x4 neighbourhood
        :param kernels: (N, 4, 4, 4, 3) array mapping the model parameters to N field parameters
        """
        import pycuda.autoinit
        self.param_to_field = param_to_field
        self.field_to_param = field_to_param
        self.gpu_param_to_field = to_gpu_correct(param_to_field.transpose((1, 2, 3, 0)).flatten())
        self.gpu_field_to_param = to_gpu_correct(field_to_param.transpose((1, 2, 3, 0)).flatten())

        if kernels.ndim == 4:
            kernels = kernels[None, ...]
        assert kernels.shape[1:] == (4, 4, 4, 3)
        self.kernels = kernels

        self.gpu_func_inverse = SourceModule(self.code(inverse=True)).get_function('convolve')
        self.gpu_func = SourceModule(self.code(inverse=False)).get_function('convolve')
        self._gpu_params = to_gpu_correct(sp.zeros(self.n_params * 3))
        self._gpu_field = to_gpu_correct(sp.zeros(self.n_kernels * self.n_field))

    def code(self, inverse=True):
        all_res = ['res%s' % letter for letter in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[:(3 if inverse else self.n_kernels)]]
        code_start = """#include <stdio.h>
__global__ void convolve({dtype}* output_vals, {dtype}* input_vals, int* mapping){{
    int idxo = threadIdx.x + blockDim.x * blockIdx.x;
    int idxi;
    {dtype} input_value;
"""
        lines = []
        for res in all_res:
            lines.append('  {dtype} %s = 0.;' % res)
        lines.append('if (idxo < {n_output}) {{')
        for x in range(64):
            kernel_line = self.kernels.reshape(self.n_kernels, 64, 3)[:, x, :]
            if (kernel_line != 0).any():
                lines.append('  idxi = mapping[idxo + %i * {n_output}];' % x)
                lines.append('  if (idxi != -1) {{')
                for t in range(self.n_kernels if inverse else 3):
                    kernel_input = kernel_line[t, :] if inverse else kernel_line[:, t]
                    if (kernel_input != 0).any():
                        lines.append('      input_value = input_vals[idxi + %i * {n_input}];' % t)
                        for k in range(3 if inverse else self.n_kernels):
                            if kernel_input[k] != 0:
                                lines.append('      %s += %f * input_value;' % (all_res[k], kernel_input[k]))
                lines.append('  }}')
        for k in range(3 if inverse else self.n_kernels):
            lines.append('  output_vals[idxo + %i * {n_output}] = %s;' % (k, all_res[k]))
        lines.extend(['}}'] * 2)
        return (code_start + '\n'.join(lines)).format(dtype=dtype, all_res=', '.join(all_res),
                                                      n_input=self.n_field if inverse else self.n_params,
                                                      n_output=self.n_params if inverse else self.n_field)

    @property
    def n_kernels(self):
        return self.kernels.shape[0]

    @property
    def n_params(self):
        return self.param_to_field.shape[0]

    @property
    def n_field(self):
        return self.field_to_param.shape[0]

    def __call__(self, params, inverse=False):
        if inverse:
            assert params.shape == (self.n_field, self.n_kernels)
            self._gpu_field.set(params.T.flatten())
            self.gpu_func_inverse(self._gpu_params, self._gpu_field, self.gpu_param_to_field,
                          block=(nthreads, 1, 1), grid=(int(sp.ceil(self.n_params / nthreads)), 1, 1))
            return self._gpu_params.get_async().reshape(3, self.n_params).T.flatten()
        else:
            assert params.shape == (self.n_params * 3, )
            self._gpu_params.set(params.reshape(self.n_params, 3).T.flatten())
            self.gpu_func(self._gpu_field, self._gpu_params, self.gpu_field_to_param,
                          block=(nthreads, 1, 1), grid=(int(sp.ceil(self.n_field / nthreads)), 1, 1))
            return self._gpu_field.get_async().reshape(self.n_kernels, self.n_field).T


class QuadraticConvolution(object):
    """Maps the parameters to a single value.

    For parameters p_{ijkd} at position (i, j, k) in the 4x4x4 neighbourhood and dimension d, compute
    \sum_{ijkd i'j'k'd'} K_{ijkd i'j'k'd'} a_{ijkd} a_{i'j'k'd'}
    """
    def __init__(self, param_to_field, field_to_param, kernel):
        import pycuda.autoinit
        self.param_to_field = param_to_field
        self.field_to_param = field_to_param
        self.gpu_field_to_param = to_gpu_correct(field_to_param.flatten())

        assert kernel.shape == (4, 4, 4, 3, 4, 4, 4, 3)
        self.kernel = kernel.transpose((3, 0, 1, 2, 7, 4, 5, 6))
        self.gpu_kernel = to_gpu_correct(self.kernel.flatten())

        self.gpu_func = cached_func(self.code(), 'convolve')
        self.gpu_derconv = cached_func(self.code_derivative_convolve(), 'convolve')
        self.gpu_dersum = cached_func(self.code_derivative_sum(), 'sum')
        self._gpu_params = to_gpu_correct(sp.zeros(self.param_to_field.shape[0] * 3))
        self._gpu_params_allder = to_gpu_correct(sp.zeros(self.param_to_field.shape[0] * 3 * 64))
        self._gpu_field = to_gpu_correct(sp.zeros(self.field_to_param.shape[0] * 2))

    def code(self, ):
        return """#include <stdio.h>
__global__ void convolve({dtype}* result, {dtype}* parameters, {dtype}* kernel, int* mapping){{
    {dtype} local_res = 0.;
    int dim, idx1, idx2, idx_map;
    int idx_field = blockIdx.x;
    int idx_param = threadIdx.x;
    __shared__ {dtype} local_params[192];
    if (idx_field < {nfield}) {{
        idx_map = mapping[idx_param + 64 * idx_field];
        for (dim = 0; dim < 3; dim++) {{
            local_params[idx_param + dim * 64] = parameters[idx_map * 3 + dim];
        }}
        __syncthreads();
        for (idx2 = 0; idx2 < 192; idx2++) {{
            for (dim = 0; dim < 3; dim++) {{
                idx1 = dim * 64 + idx_param;
                local_res += kernel[idx1 + idx2 * 192] * local_params[idx2] * local_params[idx1];
            }}
        }}

        for (int i=1; i<warpSize; i*=2) {{
            local_res += __shfl_xor(local_res, i);
        }}
        if (idx_param % 32 == 0) {{
            result[idx_field * 2 + idx_param / 32] = local_res;
        }}
    }}
}}
""".format(dtype=dtype, nfield=self.field_to_param.shape[0])

    def code_derivative_convolve(self, ):
        return """#include <stdio.h>
__global__ void convolve({dtype}* result, {dtype}* parameters, {dtype}* kernel, int* mapping){{
    {dtype} local_res;
    int dim, idx1, idx2, idx_map;
    int idx_field = blockIdx.x;
    int idx_param = threadIdx.x;
    __shared__ {dtype} local_params[192];
    if (idx_field < {nfield}) {{
        idx_map = mapping[idx_param + 64 * idx_field];
        for (dim = 0; dim < 3; dim++) {{
            local_params[idx_param + dim * 64] = parameters[idx_map * 3 + dim];
        }}
        __syncthreads();
        for (dim = 0; dim < 3; dim++) {{
            local_res = 0.;
            idx1 = dim * 64 + idx_param;
            for (idx2 = 0; idx2 < 192; idx2++) {{
                local_res += kernel[idx1 + idx2 * 192] * local_params[idx2];
            }}
            result[(idx_map * 3 + dim) * 64 + idx_param] = 2 * local_res;
        }}
    }}
}}
""".format(dtype=dtype, nfield=self.field_to_param.shape[0])

    def code_derivative_sum(self, ):
        return """
__global__ void sum({dtype}* result, {dtype}* tosum){{
    int idx_field = blockIdx.x;
    int idx_param = threadIdx.x;
    {dtype} local_res;
    if (idx_field < {nparams}) {{
        local_res = tosum[idx_param + idx_field * 64] + tosum[idx_param + idx_field * 64 + 32];
        for (int i=1; i<warpSize; i*=2) {{
            local_res += __shfl_xor(local_res, i);
        }}
        if (idx_param == 0){{
            result[idx_field] = local_res;
        }}
    }}
}}
""".format(dtype=dtype, nparams=self.param_to_field.shape[0] * 3)

    def run_gpu(self, params):
        """Evaluates the convolution for the given parameters.

        The result is stored in _gpu_field.

        :param params: numpy array giving the parameters setting the vector potential.
        """
        self._gpu_params.set(params)
        self.gpu_func(self._gpu_field, self._gpu_params, self.gpu_kernel, self.gpu_field_to_param,
                      block=(64, 1, 1), grid=(self.field_to_param.shape[0], 1, 1))

    def run_gpu_der(self, params):
        """Evaluates the derivative of the convolution for the given parameters.

        The intermediate result is stored in _gpu_params_allder.
        The final result is stored in _gpu_params

        :param params: numpy array giving the parameters setting the vector potential
        """
        self._gpu_params.set(params)
        self.gpu_derconv(self._gpu_params_allder, self._gpu_params, self.gpu_kernel, self.gpu_field_to_param,
                      block=(64, 1, 1), grid=(self.field_to_param.shape[0], 1, 1))
        self.gpu_dersum(self._gpu_params, self._gpu_params_allder,
                          block=(32, 1, 1), grid=(self.param_to_field.shape[0] * 3, 1, 1))

    def per_voxel(self, params):
        """Evaluates the convolution for the given parameters.

        :param params: numpy array giving the parameters setting the vector potential.
        :return: (nfield, ) array with the result of the convolution for every voxel
        """
        self.run_gpu(params)
        return self._gpu_field.get_async().reshape(self.field_to_param.shape[0], 2).sum(-1)

    def evaluate(self, params):
        """Evaluates the convolution for the given parameters.

        :param params: numpy array giving the parameters setting the vector potential.
        :return: float with the total result of the convolution
        """
        self.run_gpu(params)
        return gpuarray.sum(self._gpu_field).get_async()

    def derivative(self, params):
        """Evaluates the derivative of the quadratic convolution for the given parameters.

        :param params: numpy array giving the parameters setting the vector potential.
        :return: numpy array with the same shape of `params` giving the derivative of the parameters.
        """
        self.run_gpu_der(params)
        return self._gpu_params.get_async()



@lru_cache(40)
def cached_func(code, func_name):
    return cached_source_module(code).get_function(func_name)


@lru_cache(10)
def cached_source_module(code):
    return SourceModule(code)
