"""
Use :py:func:`set` to alter how the cost functions is evaluated
"""
from enum import Enum
from six import string_types


def full_chain(available=None):
    """
    Returns the ordered set of algorithms that should be tested for the basis function

    :param available: sequence of available basis functions in order of preference
    :return: re-ordered list of available basis functions based on preferences set in this module
    """
    from . import cuda
    if use_cuda is True and not cuda.available:
        raise ValueError("Requested to use cuda, but pycuda was not succesfully imported")

    preference = {(Algorithm[avail_method] if isinstance(avail_method, string_types) else avail_method): 10 - idx
                  for idx, avail_method in enumerate(available)}

    if method in preference:
        preference[method] += 100

    for cuda_method in (Algorithm.cuda, Algorithm.matrix_cuda):
        if cuda_method in preference:
            if use_cuda is True:
                preference[cuda_method] += 20
            elif use_cuda is False:
                preference[cuda_method] -= 40
            if not cuda.available:
                del preference[cuda_method]

    for matrix_method in (Algorithm.matrix, Algorithm.matrix_cuda):
        if matrix_method in preference:
            if store_matrix is True:
                preference[matrix_method] += 20
            elif store_matrix is False:
                preference[matrix_method] -= 40

    return tuple(val[0] for val in sorted(preference.items(), key=lambda val: val[1], reverse=True))


class Algorithm(Enum):
    """Methods to compute the vector field based on the parameters.
    """
    matrix = 1
    numpy = 2
    numba = 3
    matrix_cuda = 4
    cuda = 5
    convolve = 6


use_cuda = None
store_matrix = None
method = None


def set(use_cuda: bool=None, store_matrix: bool=None, method: Algorithm=None, override=False):
    """
    Sets the default method to calculate the cost function.

    Use in a with statement, for example
    ::
        with set(use_cuda=False):
            pass

    This causes any evaluators generated in the with-statement not to use the GPU.

    :param use_cuda: if True prefers cuda-enabled methods, if False don't use cuda (default: nothing is changed)
    :param store_matrix: if True, prefers to store the matrix prior to calculation, if False don't precompute the matrix (default: nothing is changed)
    :param method: explicitly set the preferred algorithm (default: nothing is changed)
    :param override: if True overrides any previous settings (default: False)
    """
    return SettingsContext(use_cuda, store_matrix, method, override)


class SettingsContext(object):
    def __init__(self, use_cuda: bool=None, store_matrix: bool=None,
                 method: Algorithm=None, override: bool=False):
        self.use_cuda = use_cuda
        self.store_matrix = store_matrix
        if isinstance(method, string_types):
            method = Algorithm[method]
        self.method = method
        self.override = override

    def __enter__(self):
        global use_cuda, store_matrix, method
        self.previous = use_cuda, store_matrix, method
        if self.use_cuda is not None and (self.override or use_cuda is None):
            use_cuda = self.use_cuda
        if self.store_matrix is not None and (self.override or store_matrix is None):
            store_matrix = self.store_matrix
        if self.method is not None and (self.override or method is None):
            method = self.method

    def __exit__(self, exc_type, exc_val, exc_tb):
        global use_cuda, store_matrix, method
        use_cuda, store_matrix, method = self.previous
