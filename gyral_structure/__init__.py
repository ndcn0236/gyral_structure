"""
Describes white matter structure as a vector field.
This vector field defines at every point the local fibre orientation and density.

- This vector field is defined as the sum of basis functions (defined in :py:mod:`gyral_structure.basis`).
- The coefficients of these basis functions are found by minimizing any cost function (see :py:mod:`gyral_structure.cost`).
- These cost functions generally require the vector field to be evaluated in certain voxels or vertices. These requests are handled by :py:mod:`gyral_structure.request`.
- Finally the algorithm used to compute this vector field is defined by :py:mod:`gyral_structure.algorithm` (e.g. cpu or gpu, pre-compute matrix or not).
- Once the best-fit coefficients have been found streamlines can be computed using :py:mod:`gyral_structure.streamline`.

You can view the code at https://git.fmrib.ox.ac.uk/ndcn0236/gyral_structure
"""
