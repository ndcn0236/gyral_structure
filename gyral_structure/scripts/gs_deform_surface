#!/usr/bin/env python
import argparse
parser = argparse.ArgumentParser('Moves a surface through a vector field')
parser.add_argument('field', help='Input HDF5 file with the basis function and best-fit array')
parser.add_argument('input', help='input surface (.surf.gii file)')
parser.add_argument('mask', help='streamlines will stop when leaving this mask')
parser.add_argument('output', help='Output .surf.gii file with the streamlines')
parser.add_argument('-n', '--negative', action='store_true',
                    help='Start streamline where they enter rather than leave the cortex')
parser.add_argument('--steplength', type=float, default=0.2,
                    help='tractography step length (default: 0.2 mm)')
parser.add_argument('-S', '--nsteps', type=int, default=2000,
                    help='maximum number of steps to take per streamline (default: 2000)')
parser.add_argument('-ol', '--output_length',
                    help='Outputs streamline lengths to given directory in GIFTI file')
parser.add_argument('-ot', '--output_tracts',
                    help='Outputs streamlines as TRK file')
args = parser.parse_args()

import nibabel
import numpy as np
from gyral_structure import basis, streamline
from mcot.surface import CorticalMesh, grid
from mcot.surface import write_gifti
from numpy import linalg
from scipy import ndimage

basis_func, best_fit_arr = basis.read(args.field)
surf = CorticalMesh.read(args.input)
start_points = surf.vertices.T

img = nibabel.load(args.mask)
trck = streamline.Tracker(basis_func.param_evaluator(best_fit_arr), img.get_data(), img.affine,
                          args.steplength, args.nsteps)
lines = trck.track(start_points, nstore=1, inverse=not args.negative)

line_lengths = np.array([(len(line) - 1) * args.steplength for line in lines])

new_vertices = np.array([line[-1] for line in lines])

deep_wm = (img.get_data() == 0) & (grid.signed_distance(surf, img.shape, img.affine) < 0)
dist = ndimage.distance_transform_edt(~deep_wm)

voxel_vertices = nibabel.affines.apply_affine(linalg.inv(img.affine), new_vertices)
distances = dist[tuple(np.around(voxel_vertices).astype('i4').T)]

bad_surface = distances > max(nibabel.affines.voxel_sizes(img.affine))

print("{} out of {} vertices did not reach the deep WM and have been replaced with their neighbours".format(
    bad_surface.sum(), bad_surface.size))

pp = surf.graph_point_point(include_diagonal=False)
map_to_bad = pp[bad_surface, :]

mean_vertices = new_vertices.copy()
nneigh = map_to_bad.dot(np.ones(surf.nvertices, dtype=int))
for _ in range(100):
    mean_vertices[bad_surface] = map_to_bad.dot(mean_vertices) / nneigh[:, None]

if args.output_length is not None:
    offset = np.sqrt(((mean_vertices - surf.vertices.T) ** 2).sum(-1))
    write_gifti(args.output_length, [offset], brain_structure=surf.anatomy)

surf.vertices = mean_vertices.T
surf.write(args.output)

if args.output_tracts:
    hdr = nibabel.streamlines.trk.TrkFile.create_empty_header()
    hdr['dimensions'] = img.shape
    hdr['voxel_sizes'] = abs(img.affine[:3, :3]).sum(0)
    trac = nibabel.streamlines.Tractogram(lines, affine_to_rasmm=linalg.inv(img.affine))
    tracfile = nibabel.streamlines.trk.TrkFile(trac, hdr)
    nibabel.streamlines.save(tracfile, args.output_tracts)
