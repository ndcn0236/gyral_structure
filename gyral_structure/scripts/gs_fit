#!/usr/bin/env python
import argparse
parser = argparse.ArgumentParser('Script to fit a vector field to the gyral white matter')
parser.add_argument('gyral_wm_mask', help='NIFTI file with gyral WM mask (e.g., from `gs_mask`)')
parser.add_argument('white', help='GIFTI (.surf.gii) file with white/gray matter boundary (used to put the surface constraints)')
parser.add_argument('pial', help='GIFTI (.surf.gii) file with the pial surface (used to place the initial charges)')
parser.add_argument('surf_mask',
                    help='GIFTI (.shape.gii) mask setting where surface constraints will be imposed')
parser.add_argument('dyads',
                    help='NIFTI file with dyads that need to be followed')
parser.add_argument('out',
                    help='HDF5 file with the best-fit basis functions')
parser.add_argument('-m', '--mid-thickness', default=None,
                    help='GIFTI (.surf.gii) file with mid-thickness (optional to also put surface constraints at mid-thickness)')
parser.add_argument('-ns', '--nsplit-surf', default=2, type=int,
                    help='Number of sub-triangles to split each surface element along each dimension')
parser.add_argument('-nv', '--nsplit-vol', default=2, type=int,
                    help='Number of sub-voxels to split each volume element along each dimension')
parser.add_argument('--wL2', default=1e-3, type=float,
                    help='Weight of the L2 constraint on the volumetric fibre density')
parser.add_argument('--wdyad', default=1., type=float,
                    help='Weight of the cost of alignment with dyads')
parser.add_argument('--wdensity', default=1., type=float,
                    help='Weight of the cost of target density constraint across the surface')
parser.add_argument('--wradial', default=1, type=float,
                    help='Weight of the radial constraint on the surface fibre density')
parser.add_argument('--wsmoothness', default=0., type=float,
                    help='Weight of the smoothness contraint on the orientation between neighbouring voxels')
parser.add_argument('-s1', '--first_rbf', default=20., type=float,
                    help='RBF Dipole size during the first optimisation step in mm (default: 20)')
parser.add_argument('-s2', '--second_rbf', default=7., type=float,
                    help='RBF Dipole size during the second optimisation step in mm (default: 7)')
parser.add_argument('-td', '--target_density',
                    help='GIFTI (.shape.gii) file with the target surface density in number of streamlines per vertex '
                         '(default: uniform per unit of cortical volume).'
                         'Can be set to "white", "mid", or "pial" to be uniform on that surface.')
parser.add_argument('--ignore_bad_dyads', action='store_true',
                    help='ignore alignment constraint for voxels where the dyad is undefined')
args = parser.parse_args()

import nibabel as nib
import numpy as np
from gyral_structure import basis, cost, request, utils
from mcot.surface import CorticalMesh, grid, Cortex
from warnings import warn
from nibabel import affines


# define where the vector field will be evaluated
white_request = request.read_surface(args.white, args.surf_mask, nsplit=args.nsplit_surf)
if args.mid_thickness is not None:
    mid_request = request.read_surface(args.mid_thickness, args.surf_mask, nsplit=args.nsplit_surf)
vol_request = request.read_volume(args.gyral_wm_mask, nsplit=args.nsplit_vol)


# define the cost functions
L2 = cost.dens.L2Density(vol_request) / vol_request.npos

dyads_img = nib.load(args.dyads)
dyads_arr = dyads_img.get_fdata()[nib.load(args.gyral_wm_mask).get_fdata() != 0]
mm_dyads_arr = utils.dyad_to_mm(dyads_arr, dyads_img.affine)
bad_dyads = (dyads_arr == 0).all(-1)
if bad_dyads.any():
    if not args.ignore_bad_dyads:
        raise ValueError(f"Dyads not defined for {bad_dyads.sum()} voxels in gyral white matter mask. Set --ignore_bad_dyads flag to ignore this issue")
    warn(f"Dyads not defined for {bad_dyads.sum()} voxels in gyral white matter mask. Ignoring these voxels when computing alignment")
    mm_dyads_arr[bad_dyads] = 0
assert np.isfinite(mm_dyads_arr).all()
aligned = cost.orient.Watson(vol_request, field=mm_dyads_arr) / vol_request.npos

voxel_size = affines.voxel_sizes(dyads_img.affine)
smoothness = cost.meta.Smoothness.from_request(cost.orient.VonMises, vol_request, max_dist=voxel_size.max() * 1.1)
smoothness = smoothness / smoothness.index1.size

surf_mask = nib.load(args.surf_mask).darrays[0].data != 0
white_surf = CorticalMesh.read(args.white)[surf_mask]

if args.target_density is None:
    target_density = Cortex([white_surf, CorticalMesh.read(args.pial)[surf_mask]]).wedge_volume()
elif args.target_density in ('white', 'mid', 'pial'):
    fn = getattr(args, 'mid_thickness' if args.target_density == 'mid' else args.target_density)
    surf = CorticalMesh.read(fn)[surf_mask]
    target_density = surf.size_faces()
else:
    target_density_vertex = nib.load(args.target_density).darrays[0].data[surf_mask]
    target_density = white_surf.graph_connection_point().T.dot(target_density_vertex) / 3.

white_normal = white_surf.normal().T
white_radial = cost.orient.VonMises(white_request, field=white_normal) / white_request.npos
white_density = cost.dens.TargetInner(white_request, field=white_normal,
                                      target_inner=target_density / white_surf.size_faces()) / white_request.npos
if args.mid_thickness is not None:
    mid_surf = CorticalMesh.read(args.mid_thickness)[surf_mask]
    mid_normal = mid_surf.normal().T
    mid_radial = cost.orient.VonMises(mid_request, field=mid_normal) / mid_request.npos
    mid_uniform = cost.dens.TargetInner(mid_request, field=mid_normal,
                                        target_inner=target_density / mid_surf.size_faces()) / mid_request.npos
    radial = 0.5 * (white_radial + mid_radial)
    density = 0.5 * (white_density + mid_uniform)
else:
    radial = white_radial
    density = white_density

no_dyads = args.wdensity * density + args.wradial * radial + args.wL2 * L2
if args.wsmoothness != 0:
    no_dyads = no_dyads + args.wsmoothness * smoothness
with_dyads = no_dyads + args.wdyad * aligned


# define the basis functions
## initial charge distribution
white = CorticalMesh.read(args.white)
ref_img = nib.load(args.gyral_wm_mask)
white_dist = grid.signed_distance(white, ref_img.shape, affine=ref_img.affine)
idx_max = np.argmin(white_dist)
voxel_max = np.array(np.unravel_index(idx_max, ref_img.shape)).flatten()
mm_max = ref_img.affine[:3, :3].dot(voxel_max) + ref_img.affine[:3, -1]

masked_pial = CorticalMesh.read(args.pial)[surf_mask]
points_pial = masked_pial.vertices[:, masked_pial.faces].mean(1).T

charge_pial = -target_density

charge_max = -np.atleast_1d(charge_pial.sum())

charge_basis = basis.SumBase([
    basis.ChargeDistribution(points_pial, 0.1),
    basis.ChargeDistribution(mm_max[None, :], 0.1)
])
charge_basis.fix(0, charge_pial)
charge_basis.fix(1, charge_max)

## radial basis functions
def get_rbf(resolution=3., spacing=1./3.):
    """
    Defines the radial basis functions

    :param resolution: radius of the basis functions
    :param spacing: spacing between basis functions relative to the resolution (best between 1/2 and 1/3)
    :return: radial basis functions
    """
    req = request.MultRequest([white_request, vol_request])
    bb = req.bounding_box()
    centroids = basis.hcp_packing(bb, distance=resolution * spacing)
    use_centroids = req.min_dist(centroids) < resolution / 2.
    return basis.RadialBasis(basis.wendland(smoothness=2), centroids[use_centroids], resolution)


# initial fitting
init_basis = basis.SumBase([charge_basis, get_rbf(args.first_rbf)])
fitter = no_dyads.get_evaluator(init_basis)
res = fitter.fit(np.zeros(init_basis.nparams), verbose_every=100, tol=1e-5)
print('Initial fitting:')
print(res)
init_basis.fix(1, res.x)

# final fitting
final_basis = basis.SumBase([init_basis, get_rbf(args.second_rbf)])
fitter = with_dyads.get_evaluator(final_basis)
res = fitter.fit(np.zeros(final_basis.nparams), verbose_every=100, tol=1e-10)
print('Final fitting:')
print(res)
final_basis.fix(1, res.x)
if res.nit < 10 or (res.status != 0 and res.nit < 1000):
    final_basis.save(args.out + "_no_convergence")
    raise ValueError("Failed to converge")
final_basis.save(args.out)




