"""Defines subclasses for all cost function terms

- :class:`GenericCost` serves as the superclass of all cost functions
- :class:`CostFunc` serves as any cost function on the vector field rather than the parameters directly (see cost.param for the latter)
- :class:`SympyCost` puts a constraint on the vector field defined by any sympy equation (which is generally easier than defining the cost function and its derivative in python)

In addition this module defines :class:`CostEvaluator`, which combines any cost function with a basis function.
This class contains the actual optimisation code.
"""
import inspect
from numbers import Number
import scipy as sp
from ..request import PositionRequest
from scipy import sparse, optimize
from ..utils import get_filetype
import pickle
import time


class GenericCost(object):
    """Superclass of all cost objects.

    Used to define how cost functions are added together and how they can be combined with a basis function
    """
    symmetric = False
    """Is the cost function symmetric between the reference field and the computed request (should be True to be used in smoothing)?"""
    quadratic = False
    """Is the cost function quadratic?"""
    independent_pos = True
    """Is the cost function evaluated for every requested position independently?"""
    epsilon = 1e-6
    """step size to numerically evaluate the derivative/hessian"""

    def __neg__(self):
        """Multiplies the cost function with -1

        called by -self
        """
        from .meta import WeightedCost
        return WeightedCost(self, -1)

    def __pos__(self):
        """Returns the cost function itself

        called by +self
        """
        return self

    def __mul__(self, other):
        """Multiplication of the cost function of a weight.

        e.g. `other * self`

        :param other: number giving the weight of the cost function
        :return: New weighted cost function
        """
        if isinstance(other, Number):
            from .meta import WeightedCost
            return WeightedCost(self, other)
        else:
            return NotImplemented

    def __rmul__(self, other):
        """Multiplication of the cost function of a weight.

        e.g. `self * other`

        :param other: number giving the weight of the cost function
        :return: New weighted cost function
        """
        return self.__mul__(other)

    def __truediv__(self, other):
        """
        Divide the cost function by a weight

        e.g. `self / other`

        :param other: number that the cost function should be divided by
        :return: New weighted cost function
        """
        if isinstance(other, Number):
            return self.__mul__(1./other)
        else:
            return NotImplemented

    def __add__(self, other):
        """Adds two cost functions together.

        e.g. `self + other`

        :param other: other term of the total cost function
        :return: cost function that evaluates the sum of both terms
        """
        if isinstance(other, GenericCost):
            from .meta import SumCost
            components = []
            if isinstance(self, SumCost):
                components.extend(self.constraints)
            else:
                components.append(self)
            if isinstance(other, SumCost):
                components.extend(other.constraints)
            else:
                components.append(other)
            return SumCost(tuple(components))
        return NotImplemented

    def __sub__(self, other):
        """Subtracts one cost function from the other

        e.g. `self - other`
        :param other: other term of the total cost function
        :return: `cost function that evaluates to the difference between both terms"""
        return self + (-other)

    def __pow__(self, power):
        """Raises the cost function to a certain power

        Note that the cost function is raised to the power before summing over all individual voxels or vertices (if possible)

        :param power: number to which the output of the cost function is raised
        :return: cost function that evaluates the old values to the given power
        """
        from .meta import PowerCost
        return PowerCost(self, power)

    def get_evaluator(self, basis_func, method=()):
        """Combines the cost function with a basis function

        :param basis_func: basis function defining the mapping of parameters to a vector field
        :param method: algorithm used to __call__ the basis function (overrides the algorithm chosen when defining the basis function if provided)
        :return: object that can __call__ the cost function given a set of parameters (and compute derivatives)
        """
        return CostEvaluator(self, basis_func, method=method)

    def describe_fit(self, predicted_field):
        """
        Describes the fit quality.

        :param predicted_field: (npos, ndim) array
        :return: statement of the fit quality
        """
        return '%s(%r=%f)' % (self.__class__.__name__, self.request, self.cost(predicted_field))


class CostFunc(GenericCost):
    def __init__(self, request):
        """Base functionality cost-function.

        :param request: Defines where the field will be evaluated
        :type request: FieldRequest
        """
        if isinstance(request, sp.ndarray):
            request = PositionRequest(request)
        self.request = request
        if self.ndim not in (2, 3):
            raise ValueError('second dimension of positions array should be 2 or 3')

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        raise NotImplementedError("Cost function not implemented for %s" % type(self))

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        if self.independent_pos:
            field = predicted_field.copy()
            derfield = sp.zeros(predicted_field.shape)
            for ixdim in range(self.ndim):
                field[:, ixdim] -= self.epsilon / 2
                reference = self.cost(field, tosum=False)
                field[:, ixdim] += self.epsilon
                derfield[:, ixdim] = (self.cost(field, tosum=False) - reference) / self.epsilon
            return derfield
        else:
            raise NotImplementedError("Derivative of cost function not implemented for %s" % type(self))

    def hessp(self, predicted_field, pfield):
        """Compute the multiplication of the hessian (at `predicted_field`) with `pfield`.

        :param predicted_field: (npos, ndim) array indicating where the hessian will be calculated
        :param pfield: (npos, ndim) array indicating in which direction the hessian will be calculated
        :return: (npos, ndim) of the local hessian times pfield
        """
        return self.dercost(predicted_field + pfield / 2) - self.dercost(predicted_field - pfield / 2)

    def qp(self, predicted_field):
        """Returns the (sparse) matrix :math:`d` and vector :math:`q` needed to rephrase the problem as a quadratic programming problem.

        For a field F, minimize
        :math:`(1/2) * F' P F + F' q`
        where element :math:`i`, dimension :math:`d` is encoded in the vector :math:`F` as :math:`F[i + d * n]`

        :param predicted_field: Returns the quadratic field at the requested position
        :return: tuple with (P, q):

            - P: (npos * ndim, npos * ndim) matrix
            - q: (npos * ndim, ) vector
        """
        if not self.quadratic:
            raise ValueError("Cost function (%s) is not quadratic, so can not use quadratic programming" % self)
        raise NotImplementedError("Quadratic programming not implemented for %s" % self)

    @property
    def ndim(self, ):
        return self.request.ndim

    @property
    def npos(self, ):
        return self.request.npos

    def bounding_box(self, extent=0):
        """Computes a bounding box around all the positions required by this request.

        :param extent: float or (ndim, ) or (2, ndim) array with how much to extend the bounding box beyond the extreme points
        :return: ((xmin, xmax), (ymin, ymax), (zmin, zmax)) covering the full dataset
        """
        return self.request.bounding_box(extent=extent)

    def __getitem__(self, item):
        arg_names = inspect.getfullargspec(self.__init__)[0]
        assert arg_names[0] == 'self'
        if arg_names[1] != 'request':
            raise ValueError("Expects request as the first argument")
        kwargs = {}
        for arg_name in arg_names[2:]:
            if hasattr(self, arg_name):
                val = getattr(self, arg_name)
                if isinstance(val, sp.ndarray) and val.shape[0] == self.npos:
                    val = val[item]
                kwargs[arg_name] = val
        return type(self)(self.request[item], **kwargs)

    def save(self, filename):
        """Saves the cost function to the filename

        :param filename: file to store the basis function & solution (if the extension is .gz, .bz, or .bz2 the result will be zipped)
        """
        with get_filetype(filename)(filename, 'wb') as file:
            pickle.dump(self, file)

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self.request)


class SympyCost(CostFunc):
    """Generates CostFunc classes based on a Sympy equation.
    """
    simplify = True
    field_symbols = set()
    _funcstr = None

    def __init__(self, request):
        super().__init__(request)
        if self.funcstr is not None:
            self.evaluate_sympy()

    @property
    def funcstr(self, ):
        return self._funcstr

    @funcstr.setter
    def funcstr(self, val):
        self._funcstr = val
        self.evaluate_sympy()

    def vectors(self, ):
        import sympy
        vectors = {}
        for field_name in self.field_symbols.union({'F'}):
            symbols = sympy.symbols(' '.join(['%s%i' % (field_name, idx) for idx in range(self.ndim)]))
            vectors[field_name] = sympy.Matrix(symbols)
        return vectors

    def evaluate_sympy(self, ):
        import sympy
        s = lambda func: func.simplify() if self.simplify else func
        vectors = self.vectors()
        self._sympy_func = s(sympy.sympify(self.funcstr, locals=vectors))
        self._symbols = tuple(self._sympy_func.atoms(sympy.Symbol))
        self._derfunc = tuple(s(sympy.diff(self._sympy_func, "F%i" % ixsymb)) for ixsymb in range(self.ndim))
        self._eval_func = sympy.lambdify(self._symbols, self._sympy_func, "numexpr")
        self._eval_derfunc = tuple(sympy.lambdify(self._symbols, f, "numexpr") for f in self._derfunc)

    def get_variables(self, predicted_field):
        res = []
        for symbol in self._symbols:
            if symbol.name in {'F0', 'F1', 'F2'}:
                value = predicted_field[:, int(symbol.name[1])]
            elif symbol.name[:-1] in self.field_symbols and symbol.name[-1] in {'0', '1', '2'}:
                ref_field = getattr(self, symbol.name[:-1])
                if ref_field.shape != (self.npos, self.ndim):
                    raise ValueError("Reference field '%s' should have the same shape as requested field" % symbol.name[:-1])
                value = ref_field[:, int(symbol.name[-1])]
            else:
                value = getattr(self, symbol.name)
            res.append(value)
        return res

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        values = self.get_variables(predicted_field)
        cost = self._eval_func(*values)
        if tosum:
            return cost.sum()
        return cost

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        values = self.get_variables(predicted_field)
        return sp.stack([df(*values) for df in self._eval_derfunc], -1)

    def __getstate__(self):
        """Returns the state that will be pickled

        All sympy objects have been removed, because they can not be pickled and hence will have to be regenerated
        """
        to_pickle = dict(self.__dict__)
        for name in ('_sympy_func', '_symbols', '_derfunc', '_eval_func', '_eval_derfunc'):
            del to_pickle[name]
        return to_pickle

    def __setstate__(self, state):
        """Recreates the sympy cost function from the pickle
        """
        self.__dict__.update(state)
        self.evaluate_sympy()


class CostEvaluator(object):
    """Evaluates the cost function for a given basis function

    :ivar evaluator: actually computes the field at the requested positions
    """
    def __init__(self, cost_func, basis_func, method=None):
        """Evaluates a cost function using the basis function.

        :param cost_func: Cost function describing all constraints
        :type cost_func: CostFunc
        :param basis_func: Describes the mapping from parameters to the field
        :type basis_func: BasisFunc
        """
        self.cost_func = cost_func
        self.basis_func = basis_func

        self.evaluator = self.cost_func.request.get_evaluator(self.basis_func, method=method)

    @property
    def nparams(self, ):
        return self.basis_func.nparams

    @property
    def ndim(self, ):
        return self.cost_func.ndim

    @property
    def npos(self, ):
        return self.cost_func.npos

    @property
    def quadratic(self, ):
        return self.cost_func.quadratic

    def cost(self, params):
        """Computes the cost for a set of parameters

        :param params: (nparams, ) array with the parameters for the basis function
        :return: total cost for the given model parameters
        """
        res = self.cost_func.cost(self.evaluator(params, inverse=False))
        if getattr(self, '_verbose_every', None) is not None:
            if self._verbose_every_iter == 0:
                self._verbose_every_time = time.time()
                time_str = 'NaN'
            else:
                time_str = str((time.time() - self._verbose_every_time) * 1000 / self._verbose_every_iter)

            if self._verbose_every_iter % self._verbose_every == 0:
                print('{:5d}: {:.3e} (t={} ms)'.format(self._verbose_every_iter, res, time_str))
            self._verbose_every_iter += 1
        return res

    def dercost(self, params):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param params: (nparams, ) array with the parameters for the basis function
        :return: (nparams, ) array defining which direction the parameters should move to INCREASE the cost function
        """
        return self.evaluator(self.cost_func.dercost(self.evaluator(params, inverse=False)), inverse=True)

    def hessp(self, params, dparams):
        """Compute the multiplication of the hessian (at `params`) with `dparams`.

        :param params: (nparams, ) array indicating where the hessian will be calculated
        :param dparams: (nparams, ) array indicating in which direction the hessian will be calculated
        :return: (nparams, ) of the local hessian times dparams
        """
        return self.evaluator(self.cost_func.hessp(self.evaluator(params, inverse=False),
                                                   self.evaluator(dparams, inverse=False)), inverse=True)

    def describe_fit(self, params):
        """
        Prints a description of the fit quality

        :param params: (nparams, ) array with the parameters for the basis function
        """
        value = self.cost_func.cost(self.evaluator(params, inverse=False))
        desc = self.cost_func.describe_fit(self.evaluator(params, inverse=False))
        print('%s = %f' % (desc, value))

    def qp(self, params):
        """Returns the (sparse) matrix :math:`d` and vector :math:`q` needed to rephrase the problem as a quadratic programming problem.

        For a field F, minimize
        :math:`(1/2) * F' P F + F' q`
        where element :math:`i`, dimension :math:`d` is encoded in the vector :math:`F` as :math:`F[i + d * n]`

        :param predicted_field: Returns the quadratic field at the requested position
        :return: tuple with (P, q):

            - P: (npos * ndim, npos * ndim) matrix
            - q: (npos * ndim, ) vector
        """
        if not self.quadratic:
            raise ValueError("Cost-functions are non-quadratic so can not compute P and q")
        predicted_field = self.evaluator(params, inverse=False)
        return self.evaluator.wrap_qp(self.cost_func.qp(predicted_field))

    def cvxopt_solve(self, p0, ftol=1e-3, maxiter=1, L2norm=1e-6):
        """Use cvxopt to exactly solve the quadratic equation.
        """
        import cvxopt
        params = p0
        for ixiter in range(maxiter):
            P, q = self.qp(params)
            if not self.quadratic or ixiter == 0:
                if sparse.issparse(P):
                    Pcoo = P.tocoo()
                    Pcvx = cvxopt.spmatrix(Pcoo.data, Pcoo.row, Pcoo.col, size=Pcoo.shape)
                else:
                    Pcvx = cvxopt.matrix(P)
                Puse = Pcvx + cvxopt.spdiag(cvxopt.matrix(sp.ones(p0.size) * L2norm))
            qcvx = cvxopt.matrix(q)
            res = cvxopt.solvers.coneqp(Puse, qcvx)
            pnew = sp.array(res['x']).flatten()
            if abs((params - pnew) / (params + pnew + ftol)).max() < ftol:
                return pnew
            params = pnew
        print('maximum number of iterations reached')
        return params

    def bounding_box(self, extent=0):
        """Computes a bounding box around all the positions required by this request.

        :param extent: float or (ndim, ) or (2, ndim) array with how much to extend the bounding box beyond the extreme points
        :return: ((xmin, xmax), (ymin, ymax), (zmin, zmax)) covering the full dataset
        """
        return self.cost_func.bounding_box(extent=extent)

    def fit(self, params=None, method='L-BFGS-B',
            options={'maxfun': 50000, 'maxiter': 60000},
            tol=1e-8, verbose_every=None):
        """Fits the basis function to the given cost-function with an added smoothness constraint.

        :param params: initial parameters (default: nearly zero)
        :param method: name of the optimization algorithm
        :param options: parameters to pass to the optimization algorithm
        :param tol: sets the tolerance
        :param verbose_every: print an update every `verbose_every` iterations
        """
        if params is None:
            params = sp.randn(self.nparams) * 1e-8
        self._verbose_every = verbose_every
        self._verbose_every_iter = 0
        res = optimize.minimize(self.cost, params, method=method, jac=self.dercost, options=options, tol=tol)
        del self._verbose_every
        return res


class VectorCost(CostFunc):
    """
    Computes the offset between the field at the requested point and a reference field
    """
    def __init__(self, request, field=None, norm_length=0, flip=False, mindens=0):
        """Defines a cost function to match the `field` at the given `positions`.

        The field can represent observed fiber orientations or other constraints (e.g. matching the surface normals).
        :param request: Defines where the field will be evaluated
        :param field: (npos, ndim) array with the expected field at the given locations (defaults to 0, i.e. an L2 norm)
        :param norm_length: Whether to normalize the length of the vectors (0: no normalization; 1: normalize the model field; 2: normalize the target field)
        :param flip: if True, allow the field to flip sign
        :param mindens: minimum density to consider
        """
        super(VectorCost, self).__init__(request)
        if field is None:
            field = sp.zeros((self.npos, self.ndim))
        self.field = field
        if (self.npos, self.ndim) != self.field.shape:
            raise ValueError("Position array and vector field array shapes should match")
        if norm_length not in (0, 1, 2):
            raise ValueError("norm_length parameter should be one of (0, 1, 2)")
        self.norm_length = norm_length
        self.flip = flip
        self.mindens = mindens

    @property
    def quadratic(self, ):
        return self.norm_length == 0 or (self.flip and self.norm_length == 2)

    @property
    def symmetric(self, ):
        return self.norm_length == 0

    def cost(self, predicted_field, length=None, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        if length is None:
            length = self.length(predicted_field)
        use = sp.sum(predicted_field ** 2, -1) >= self.mindens ** 2
        if self.norm_length == 1:
            result = (self.field - length * predicted_field) ** 2
            result[length[:, 0] == 0, :] = predicted_field[length[:, 0] == 0, :] ** 2
        else:
            result = (length * self.field - predicted_field) ** 2
        if tosum:
            return result[use, :].sum()
        return result.sum(-1)

    def dercost(self, predicted_field, length=None):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param length: (npos, ) array with the pre-computed expected length of the predicted field (see self.length)
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        if length is None:
            length = self.length(predicted_field)
        use = sp.sum(predicted_field ** 2, -1) >= self.mindens ** 2
        if self.norm_length != 1:
            base = 2 * (predicted_field - length * self.field)
            base[~use, :] = 0
        else:
            return super().dercost(predicted_field)
            dl_dF = -self.field * ((sp.sum(self.field ** 2, -1) /
                                    sp.sum(predicted_field * self.field, -1) ** 2))[..., None]
            part1 = 2 * length * (length * predicted_field - self.field)
            if self.norm_length == 1:
                part2 = 2 * sp.sum(predicted_field * (length * predicted_field - self.field), -1)[..., None] * dl_dF
            elif self.norm_length == 3:
                part2 = (sp.sum(2 * length * (length * predicted_field - self.field) * predicted_field, -1)[..., None] *
                         length * (self.field / sp.sum(self.field * predicted_field, -1)[..., None] -
                                   2 * predicted_field / sp.sum(predicted_field ** 2, -1)[..., None]))
            base = part1 + part2
            base[length[..., 0] == 0, :] = 2 * predicted_field[length[..., 0] == 0, :]
            base[~use, :] = 0
        return base

    def length(self, predicted_field):
        """Returns the norm that the target field should have to match the observed field

        :param predicted_field: (npos, ndim) array with the predicted field at the requested postions
        :return: (npos, ) array
        """
        if self.norm_length == 0:
            if self.flip:
               length = sp.sign((self.field * predicted_field).sum(-1))[..., None]
            else:
                length = sp.ones((predicted_field.shape[0], 1))
        else:
            if self.norm_length == 1:
                norm = sp.sum(predicted_field ** 2, -1)[..., None]
            else:
                norm = sp.sum(self.field ** 2, -1)[..., None]
            length = sp.sum(self.field * predicted_field, -1)[..., None] / norm
            length[norm == 0] = 0
            if not self.flip:
                length[length < 0] = 0
        return length

    def hessp(self, predicted_field, pfield):
        """Compute the multiplication of the hessian (at `predicted_field`) with `pfield`.

        :param predicted_field: (npos, ndim) array indicating where the hessian will be calculated
        :param pfield: (npos, ndim) array indicating in which direction the hessian will be calculated
        :return: (npos, ndim) of the local hessian times pfield
        """
        if self.norm_length == 0:
            return 2 * pfield
        elif self.norm_length == 1:
            return super(VectorCost, self).hessp(predicted_field, pfield)
        elif self.norm_length == 2:
            norm = sp.sum(self.field ** 2, -1)
            dl_dF = sp.zeros(self.field.shape)
            replace = norm != 0
            dl_dF[replace] = self.field[replace] / norm[replace][..., None]
            if not self.flip:
                dl_dF[self.length(predicted_field)[..., 0] == 0, :] = 0.
            return 2 * (pfield - sp.sum(dl_dF * pfield, -1)[..., None] * self.field)

    def qp(self, predicted_field):
        """Returns the (sparse) matrix :math:`d` and vector :math:`q` needed to rephrase the problem as a quadratic programming problem.

        For a field F, minimize
        :math:`(1/2) * F' P F + F' q`
        where element :math:`i`, dimension :math:`d` is encoded in the vector :math:`F` as :math:`F[i + d * n]`

        :param predicted_field: Returns the quadratic field at the requested position
        :return: tuple with (P, q):

            - P: (npos * ndim, npos * ndim) matrix
            - q: (npos * ndim, ) vector
        """
        if not self.quadratic:
            raise ValueError("Cost function (%s) is not quadratic, so can not use quadratic programming" % self)
        P = 2 * sparse.identity(self.npos * self.ndim)
        q = self.dercost(predicted_field).flatten()
        if self.norm_length == 0:
            return P, q - P * predicted_field.flatten()
        elif self.norm_length == 1:
            f_dot_f = sp.sum(self.field ** 2, -1)[:, None]
            F_dot_f = sp.sum(self.field * predicted_field, -1)[:, None]
            length = F_dot_f / f_dot_f
            length[f_dot_f == 0] = 0
            diagonal = (4 * self.field * predicted_field / f_dot_f + 2 / length +
                        2 * self.field ** 2 * predicted_field / F_dot_f)
            weights = self.field[:, :, None] * (-2 * predicted_field / (length ** 2 * f_dot_f) +
                                                predicted_field * self.field ** 2 / F_dot_f ** 2 -
                                                4 * predicted_field ** 2 * self.field * f_dot_f / F_dot_f ** 3)[:, None, :]
            sz = (self.npos, self.npos)
            indices = sp.arange(self.ndim * self.npos).reshape((self.npos, self.ndim))
            _, rows, cols = sp.broadcast_arrays(weights, indices[:, None, :], indices[:, :, None])
            P = (sparse.coo_matrix((weights.flatten(), (rows.flatten(), cols.flatten())), shape=sz) +
                 sparse.dia_matrix((diagonal, 0), shape=sz))
            return P, q - P * predicted_field.flatten()
        elif self.norm_length == 2:
            norm = sp.sum(self.field ** 2, -1)
            dl_dF = self.field / norm[..., None]
            dl_dF[norm == 0, :] = 0.
            weights = dl_dF[:, None, :] * self.field[:, :, None]
            indices = sp.arange(self.ndim * self.npos).reshape((self.npos, self.ndim))
            _, rows, cols = sp.broadcast_arrays(weights, indices[:, None, :], indices[:, :, None])
            P = P - sparse.coo_matrix((weights.flatten(), (rows.flatten(), cols.flatten())),
                                      shape=P.shape) * 2
            return P, q - P * predicted_field.flatten()


def read(filename):
    """Loads a cost function from the given filename

    :param filename: file to load the basis function & solution from
    """
    with get_filetype(filename)(filename, 'rb') as file:
        return pickle.load(file)

