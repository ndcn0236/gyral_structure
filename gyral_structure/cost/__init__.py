"""Defines individual terms of the cost function.

Normal arithmetic can be used to add the cost function terms together, e.g. Watson + 0.01 * L2

Terms are separated based on what they affect:

- Density constraints are available in :mod:`gyral_structure.cost.dens`
- Orientation constraints are available in :mod:`gyral_structure.cost.orient`
- Constraints that combine other constraints (e.g., smoothness constraints or adding them) are defined in :mod:`gyral_structure.cost.meta`
- Constraints on the parameters rather than the vector field are defined in :mod:`gyral_structure.cost.param`
- :mod:`gyral_structure.cost.core` contains the base class and the cost optimisation code
"""
from . import core, dens, meta, orient, param
from .core import read
