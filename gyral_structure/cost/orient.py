"""Defines terms for the cost function that constrain the fibre orientation independent of fiber density

- :class:`VonMises`: encourages alignment with the direction of a reference field (disallows flipping)
- :class:`Watson`: encourages alignment with the orientation of a reference field (allows for flipping)
- :class:`BinghamDistribution`: encourages alignment with a reference field with an ellipsoidal uncertainty (allows for flipping)
"""
import nibabel
import scipy as sp
from .meta import MinCost
from .core import SympyCost, CostFunc
from .. import request


class Watson(SympyCost):
    """Computes the (unnormalized) -log(p) for the Watson distribution.
    """
    field_symbols = {'field'}

    def __init__(self, request, field, kappa=1., min_norm=1e-20, mult_term='None'):
        """Returns the Watson distribution.

        :param request: where the field will be calculated
        :param field: fiber orientations to compare the observed field with
        :param kappa: width of the Watson distribution
        :param min_norm: correction term to stop the cost function to go to NaN if the requested field is zero
        :param mult_term: how each field-request comparison is weighted

            - 'predicted': weight with the density of the model field
            - 'field': weight with the density of the comparison field
            - 'None': all comparisons have equal weight
            - 'both': weight with the sum of the model and comparison field densities
        """
        super(Watson, self).__init__(request)
        self.field = field
        self.kappa = kappa
        self.min_norm = min_norm
        self.mult_term = mult_term
        to_mult = {'predicted': 'sqrt(F.dot(F))',
                   'field': 'sqrt(field.dot(field))',
                   'None': '1',
                   'both': '(sqrt(F.dot(F)) + sqrt(field.dot(field)))'}
        self.funcstr = '-%s * kappa * (F.dot(field)) ** 2 / (field.dot(field) * F.dot(F) + min_norm)' % to_mult[mult_term]

    @property
    def symmetric(self, ):
        return self.mult_term in {'both', 'None'}


class VonMises(SympyCost):
    """Computes the (unnormalized) -log(p) for the von Mises distribution.
    """
    field_symbols = {'field'}

    def __init__(self, request, field, kappa=1., min_norm=1e-20, mult_term='None'):
        """Returns the Von Mises distribution.

        :param request: where the field will be calculated
        :param field: fiber orientations to compare the observed field with
        :param kappa: width of the von Mises distribution
        :param min_norm: correction term to stop the cost function to go to NaN if the requested field is zero
        :param mult_term: how each field-request comparison is weighted

            - 'predicted': weight with the density of the model field
            - 'field': weight with the density of the comparison field
            - 'None': all comparisons have equal weight
            - 'both': weight with the sum of the model and comparison field densities
        """
        super(VonMises, self).__init__(request)
        self.field = field
        self.kappa = kappa
        self.min_norm = min_norm
        self.mult_term = mult_term
        to_mult = {'predicted': 'sqrt(F.dot(F))',
                   'field': 'sqrt(field.dot(field))',
                   'None': '1',
                   'both': '(sqrt(F.dot(F)) + sqrt(field.dot(field)))'}
        self.funcstr = '-%s * kappa * F.dot(field) / sqrt(field.dot(field) * F.dot(F) + min_norm)' % to_mult[mult_term]

    @property
    def symmetric(self, ):
        return self.mult_term in {'both', 'None'}


class BinghamDistribution(CostFunc):
    """Computes the (unnormalized) -log(p) for the Bingham distribution
    """
    def __init__(self, request, theta, phi, psi, ka, kb, maxk=1e5, flipx=True):
        """Returns the Bingham distribution.

        :param request: where the field will be calculated
        :param theta: elevation of the primary eigenvector
        :param phi: orientation of the primary eigenvector in the x-y plane
        :param psi: determines the orientation of the secondary eigenvector
        :param ka: inverse disperison along the primary eigenvector
        :param kb: inverse disperison along the secondary eigenvector
        :param maxk: maximum value for the inverse dispersion
        :param flipx: if True flips the x-axis
        """
        super(BinghamDistribution, self).__init__(request)
        if self.ndim == 2:
            raise ValueError("Bingham distribution is not defined in 2 dimensions")
        self.theta = theta
        self.phi = phi
        self.psi = psi
        self.ka = ka
        self.kb = kb
        self.R = sp.matmul(self.rotation_matrix(psi, 2),
                           sp.matmul(self.rotation_matrix(theta, 1),
                                     self.rotation_matrix(phi, 2)))
        self.flipx = flipx
        if self.flipx:
            self.R[:, :, 0] *= -1
        self.maxk = maxk

    def evaluate_B(self, ):
        """Evaluates the matrix with which the observed field orientation should be multiplied

        This method is automatically called whenever `maxk` is set.
        """
        Bdiag = sp.zeros((self.npos, 3, 3))
        Bdiag[:, 0, 0] = sp.minimum(self.ka, self.maxk)
        Bdiag[:, 1, 1] = sp.minimum(self.kb, self.maxk)
        self.B = sp.matmul(sp.transpose(self.R, (0, 2, 1)),
                           sp.matmul(Bdiag, self.R))
        assert sp.nanmax(abs((sp.transpose(self.B, (0, 2, 1)) - self.B) / (self.B + 1e-20))) < 1e-5

    @property
    def maxk(self, ):
        """
        Maximum value for the inverse dispersion
        """
        return self._maxk

    @maxk.setter
    def maxk(self, val):
        self._maxk = val
        self.evaluate_B()

    @staticmethod
    def rotation_matrix(angle, around_axis):
        """Creates a rotation matrix around the given axis

        :param angle: (N, ) array with the angular rotation in radians
        :param around_axis: around which axis the matrix should rotate
        :return (N, 3, 3) array for the rotation around the N angles
        """
        c = sp.cos(angle)
        s = sp.sin(angle)
        base = sp.zeros((angle.size, 3, 3))
        dim1 = (around_axis + 1) % 3
        dim2 = (around_axis + 2) % 3
        base[:, around_axis, around_axis] = 1.
        base[:, dim1, dim1] = c
        base[:, dim2, dim2] = c
        base[:, dim1, dim2] = s
        base[:, dim2, dim1] = -s
        return base

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        norm = sp.sqrt(sp.sum(predicted_field ** 2, -1))
        elements = ((predicted_field[:, None, :] * self.B).sum(-1) * predicted_field).sum(-1) / norm
        if tosum:
            return sp.nansum(elements)
        return elements

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        norm = 1 / sp.sqrt(sp.sum(predicted_field ** 2, -1))[:, None]
        dnorm = -predicted_field * norm ** 3
        delements = (predicted_field[:, None, :] * self.B).sum(-1) * 2
        elements = (delements * predicted_field).sum(-1)[:, None] * 0.5
        res = delements * norm + dnorm * elements
        res[~sp.isfinite(res)] = 0.
        return res

    @classmethod
    def read_wb_bingham(cls, filename, voxel_size=0, fmin=0.05, maxk=1e5, flipx=True):
        """Reads the Bingham distribution from the CIFTI file produced by the `wb_command -estimate-fiber-binghams` command.

        NOTE: the model orientations are in mm-space not voxel-space like in bedpostx

        :param filename: CIFTI file with the Bingham distributions
        :param voxel_size: size over which each voxel is averaged
        :param fmin: minimum mean f to be included in the analysis
        :param maxk: maximum ka/kb values (can be very high in the results from -estimate-fiber-binghams)
        :param flipx: if True flip the x-coordinates of the dyad (useful for a negative affine)
        """
        img = nibabel.load(filename)
        columns = [nm.map_name for nm in img.header.matrix.mims[0].named_maps]
        data = img.get_data()
        assert len(columns) == data.shape[0]
        mm_pos = sp.zeros((data.shape[1], 3))

        for idxdim, dim in enumerate(['x coord', 'y coord', 'z coord']):
            idx_col = columns.index(dim)
            mm_pos[:, idxdim] = data[idx_col, :]

        nelems = sp.sum(['theta%i' % (idx + 1) in columns for idx in range(10)])
        get_data = lambda name, idx: data[columns.index('%s%i' % (name, idx + 1)), :]
        use = sp.zeros((nelems, data.shape[1]), dtype='bool')
        for idx_elem in range(nelems):
            use[idx_elem, :] = get_data('mean 	', idx_elem) > fmin

        if voxel_size == 0:
            req_pos = request.PositionRequest(mm_pos)
        else:
            req_pos = request.VoxelRequest(mm_pos, voxel_size)

        params = {}
        for name in ['theta', 'phi', 'psi', 'ka', 'kb']:
            arr = sp.zeros((nelems, data.shape[1]), dtype=data.dtype)
            for idx_elem in range(nelems):
                arr[idx_elem, :] = get_data(name, idx_elem)
            arr[~use] = sp.nan
            params[name] = (arr[0] if nelems == 1 else arr)
        if nelems == 1:
            return cls(req_pos, maxk=maxk, flipx=flipx, **params)
        else:
            sub_cost = [cls(req_pos, maxk=maxk, flipx=flipx,
                            **{name: value[idx] for name, value in params.items()}) for idx in range(nelems)]
            return MinCost(sub_cost)


class BinghamDistributionSympy(SympyCost):
    """Alternative implementation of the Bingham distribution using sympy

    Used for comparison with :class:`BinghamDistribution` (which is faster)
    """
    def __init__(self, request, theta, phi, psi, ka, kb):
        """Returns the Bingham distribution.

        :param request: where the field will be calculated
        :param theta: elevation of the primary eigenvector
        :param phi: orientation of the primary eigenvector in the x-y plane
        :param psi: determines the orientation of the secondary eigenvector
        :param ka: accuracy along the primary eigenvector
        :param kb: accuracy along the secondary eigenvector
        """
        import sympy
        super(BinghamDistributionSympy, self).__init__(request)
        if self.ndim == 2:
            raise ValueError("Bingham distribution is not defined in 2 dimensions")
        self.theta = theta
        self.phi = phi
        self.psi = psi
        self.ka = ka
        self.kb = kb
        self.R_psi = sympy.rot_axis3('psi')
        self.R_theta = sympy.rot_axis2('theta')
        self.R_phi = sympy.rot_axis3('phi')
        self.R = self.R_psi * self.R_theta * self.R_phi
        self.Bdiag = sympy.Matrix([['ka', 0, 0], [0, 'kb', 0], [0, 0, 0]])
        self.B = self.R.T * self.Bdiag * self.R
        F = self.vectors()['F']
        Fnorm = sympy.sqrt(F.dot(F))
        Fvec = F / Fnorm
        self.funcstr = Fnorm * Fvec.dot(self.B.dot(Fvec))


class NormedInner(CostFunc):
    """Encourages alignment with an observed field.

    For a model field :math:`F` and observed field :math:`f` minimizes:
    :math:`c = - (F \cdot f)^2 / ((F \cdot F) (f \cdot f))`
    """
    def __init__(self, request, field, min_norm=1e-10):
        """Creates a new constrained on the normalized inner product

        :param request: Defines where the model field will be evaluated
        :param field: (N, ndim) array that the model field will be compared against
        """
        super(NormedInner, self).__init__(request)
        self.field = field
        self.min_norm = min_norm

    symmetric = True

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        norm = sp.sum(predicted_field ** 2, -1) * sp.sum(self.field ** 2, -1) + self.min_norm
        result = sp.sum(predicted_field * self.field, -1) ** 2 / norm
        result[norm == 0] = 0.
        if not tosum:
            return result
        return result.sum()

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        f_norm = sp.sum(self.field ** 2, -1)[:, None]
        F_norm = sp.sum(predicted_field ** 2, -1)[:, None]
        F_dot_f = sp.sum(self.field * predicted_field, -1)[:, None]
        norm = F_norm * f_norm + self.min_norm
        result = 2 * F_dot_f / norm * (self.field - (F_dot_f / norm) * f_norm * predicted_field)
        return result

    def hessp(self, predicted_field, pfield):
        """Compute the multiplication of the hessian (at `predicted_field`) with `pfield`.

        :param predicted_field: (npos, ndim) array indicating where the hessian will be calculated
        :param pfield: (npos, ndim) array indicating in which direction the hessian will be calculated
        :return: (npos, ndim) of the local hessian times pfield
        """
        f_norm = sp.sum(self.field ** 2, -1)[:, None]
        F_norm = sp.sum(predicted_field ** 2, -1)[:, None]
        F_dot_f = sp.sum(self.field * predicted_field, -1)[:, None]
        norm = F_norm * f_norm + self.min_norm
        mixed_term = - 4 * F_dot_f * f_norm / norm ** 2
        result = (
            - 2 * F_dot_f ** 2 * f_norm / norm ** 2 * pfield
            + 2 * sp.sum(self.field * pfield, -1)[:, None] * self.field / norm
            + mixed_term * sp.sum(self.field * pfield, -1)[:, None] * predicted_field
            + mixed_term * sp.sum(predicted_field * pfield, -1)[:, None] * self.field
            + 8 * F_dot_f ** 2 * f_norm ** 2 * sp.sum(predicted_field * pfield, -1)[:, None] * predicted_field / norm ** 3
          )
        return result
