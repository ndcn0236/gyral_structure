"""Defines cost functions that take other cost functions as input

Most of these can be created by adding/subtracting/multiplying cost functions, except for:

- :class:`ExponentAdd`: add cost function terms exponentially
- :class:`Smoothness`: compares the vector field in neighbouring voxels
- :class:`MinCost`: use the minimum of several cost functions for fitting (useful when fitting to crossing fibers)
"""
from itertools import zip_longest
import numba
import scipy as sp
from scipy import spatial
from .core import CostFunc
from ..request import FieldRequest, PositionRequest, VoxelRequest, DerivativeRequest, MultRequest


class ExponentAdd(CostFunc):
    """:math:`C = log(\sum_i(exp(c_i))` for all :math:`c_i` in sub_cost

    Each element of the requested cost function is added together separately
    """
    def __init__(self, sub_cost):
        """New cost function combining all functions in sub_cost

        :param sub_cost: cost functions to be combined. All cost functions should have the same request
        :type sub_cost: List[CostFunc]
        """
        self.sub_cost = list(sub_cost)
        for sc in self.sub_cost:
            if sc.request != self.request:
                raise ValueError("All sub-costs should request the field at the same positions")

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        total_cost = None
        for sc in self.sub_cost:
            if total_cost is None:
                total_cost = sp.exp(sc.cost(predicted_field, tosum=False))
            else:
                total_cost += sp.exp(sc.cost(predicted_field, tosum=False))
        if tosum:
            return sp.log(total_cost).sum()
        return sp.log(total_cost)

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        df = None
        raw_cost = None
        for sc in self.sub_cost:
            c = sp.exp(sc.cost(predicted_field, tosum=False)[:, None])
            dc = sc.dercost(predicted_field)
            if raw_cost is None:
                raw_cost = c
                df = dc * c
            else:
                raw_cost += c
                df += dc * c
        return df / raw_cost

    @property
    def request(self, ):
        return self.sub_cost[0].request

    @request.setter
    def request(self, new_req):
        for sc in self.sub_cost:
            sc.request = new_req

    def describe_fit(self, predicted_field):
        """
        Describes the fit quality.

        :param predicted_field: (npos, ndim) array
        :return: statement of the fit quality
        """
        desc = ', '.join([sc.describe_fit(predicted_field) for sc in zip(self.constraints)])
        return 'ExponentAdd([%s] = %f)' % (desc, self.cost(predicted_field))


class MinCost(CostFunc):
    """Returns the minimum for several cost functions

    Useful to fit to crossing fiber data
    """
    def __init__(self, sub_cost):
        """Computes the minimum for every field requested over the provided cost functions

        :param sub_cost: list of cost functions with the same request
        :raises ValueError: if the cost functions have different requests
        """
        self.sub_cost = sub_cost
        for sc in self.sub_cost:
            if sc.request != self.request:
                raise ValueError("All sub-costs should request the field at the same positions")

    @property
    def request(self, ):
        return self.sub_cost[0].request

    @request.setter
    def request(self, new_req):
        for sc in self.sub_cost:
            sc.request = new_req

    def arg_min(self, predicted_field):
        """Finds the closest vector to the predicted field.

        :param predicted_field: (npos, ndim) array with the field predicted at the requested positions
        :return: tuple with 2 elements:

            - (npos, ) integer array with the indices of the minimum cost function (between 0 and number of cost functions -1)
            - (npos, ) array with the minimum cost at each requested position
        """
        min_cost = self.sub_cost[0].cost(predicted_field, tosum=False)
        idx = sp.zeros(self.npos, dtype='i4')
        for idx_sc, sc in enumerate(self.sub_cost[1:]):
            new_cost = sc.cost(predicted_field, tosum=False)
            replace = (new_cost < min_cost) | ~sp.isfinite(min_cost)
            idx[replace] = idx_sc + 1
            min_cost[replace] = new_cost[replace]
        return idx, min_cost

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        _, arr = self.arg_min(predicted_field)
        if tosum:
            return sp.nansum(arr)
        return arr

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        idx, _ = self.arg_min(predicted_field)
        df = sp.zeros((self.npos, self.ndim))
        for idx_sc, sc in enumerate(self.sub_cost):
            use = idx == idx_sc
            df[use, :] = sc.dercost(predicted_field)[use, :]
        return df

    def hessp(self, predicted_field, pfield):
        """Compute the multiplication of the hessian (at `predicted_field`) with `pfield`.

        :param predicted_field: (npos, ndim) array indicating where the hessian will be calculated
        :param pfield: (npos, ndim) array indicating in which direction the hessian will be calculated
        :return: (npos, ndim) of the local hessian times pfield
        """
        idx, _ = self.arg_min(predicted_field)
        df = sp.zeros((self.npos, self.ndim))
        for idx_sc, sc in enumerate(self.sub_cost):
            use = idx == idx_sc
            df[use, :] = sc.hessp(predicted_field, pfield)[use, :]
        return df

    def describe_fit(self, predicted_field):
        """
        Describes the fit quality.

        :param predicted_field: (npos, ndim) array
        :return: statement of the fit quality
        """
        desc = ', '.join([sc.describe_fit(predicted_field) for sc in zip(self.constraints)])
        return 'min([%s] = %f)' % (desc, self.cost(predicted_field))


class Smoothness(CostFunc):
    """Enforces a smoothness contraint.

    Applies a cost function to the fields found in neighbouring voxels
    """
    def __init__(self, request, sub_cost, index1, index2):
        """Creates a comparison between the field values at `index1` and `index2` according to `sub_cost`.

        :param request: Where the field values will be calculated
        :type request: FieldRequest
        :param sub_cost: Cost function to compare the field at `index1` and `index2`
        :type sub_cost: CostFunc
        :param index1: index array
        :param index2: index array
        """
        super(Smoothness, self).__init__(request)
        if not sub_cost.symmetric:
            raise ValueError("Cost function should be symmetric to be a smoothness constraint")
        self.sub_cost = sub_cost
        self.index1 = index1
        self.index2 = index2

    @classmethod
    def from_mask(cls, sub_cost, mask, affine=None, jacobian=False, nsplit=3):
        """Creates a cost function that evaluates the `sub_cost` in neighbouring voxels in the `mask`

        :param sub_cost: cost function that expects two fields which will be compared
        :param mask: (Nx, Ny, Nz) array; the smoothness constraint will be applied wherever the mask in non-zero
        :param affine: (4, 4) array with the mapping from voxel to mm space
        :param jacobian: if True evaluates the smoothness constraint on the first derivative of the field rather than the field itself
        :param nsplit: number of individual elements to split the voxels in
        :return: cost function that compares the field in neighbouring voxels
        """
        if isinstance(sub_cost, type) and issubclass(sub_cost, CostFunc):
            sub_cost = sub_cost(request=sp.zeros((0, mask.ndim)),
                                field=sp.zeros((0, mask.ndim)))
        if affine is None:
            affine = sp.eye(mask.ndim + 1)
        idx_mask = -sp.ones(mask.shape, dtype='i4')
        idx_mask[mask] = sp.arange(mask.sum())
        vox_pos = sp.array(sp.where(mask))
        mm_pos = sp.dot(affine[:-1, :-1], vox_pos).T + affine[:-1, -1]
        indices = [sp.zeros((0, ), dtype='i4'), sp.zeros((0, ), dtype='i4')]
        for dim in range(mask.ndim):
            other_indices = sp.roll(idx_mask, 1, dim)
            select = [slice(None)] * mask.ndim
            select[dim] = 0
            other_indices[tuple(select)] = -1
            use = (other_indices != -1) & mask
            indices[0] = sp.append(indices[0], idx_mask[use])
            indices[1] = sp.append(indices[1], other_indices[use])
        sub_cost.request = PositionRequest(sp.zeros((indices[0].size, mask.ndim)))
        if not jacobian:
            vox_size = sp.sum(affine[:-1, :-1], 1)
            req = VoxelRequest(mm_pos, vox_size, nsplit=nsplit)
            return cls(req, sub_cost, indices[0], indices[1])
        else:
            return SumCost([cls(req, sub_cost, indices[0], indices[1])
                            for req in DerivativeRequest.all_derivatives(mm_pos)])

    @classmethod
    def from_request(cls, sub_cost, request, max_dist):
        """Uses `sub_cost` to compare the fields in any requested positions within `min_dist` from each other

        :param sub_cost: cost function that expects two field which will be compared
        :type sub_cost: CostFunc
        :param request: defines where the field will be evaluated
        :type request: FieldRequest
        :param max_dist: all pairs of requested fields, whose centers are within `max_dist` from each other will be compared
        :return: cost function that compares the field in neighbouring requested positions
        """
        if isinstance(sub_cost, type) and issubclass(sub_cost, CostFunc):
            sub_cost = sub_cost(request=sp.zeros((0, request.ndim)),
                                field=sp.zeros((0, request.ndim)))
        tree = spatial.cKDTree(request.center())
        index1, index2 = sp.array(list(tree.query_pairs(max_dist))).T
        sub_cost.request = PositionRequest(sp.zeros((index1.size, request.ndim)))
        return Smoothness(request, sub_cost, index1, index2)

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized
        """
        if not tosum:
            raise ValueError("Can not compute element-wise cost function")
        self.sub_cost.field = predicted_field[self.index2, :]
        return self.sub_cost.cost(predicted_field[self.index1, :])

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        self.sub_cost.field = predicted_field[self.index2, :]
        dc_indexed = self.sub_cost.dercost(predicted_field[self.index1, :])
        res = sp.zeros(predicted_field.shape)
        for dim in range(self.ndim):
            res[:, dim] += sp.bincount(self.index1, weights=dc_indexed[:, dim], minlength=self.npos)

        self.sub_cost.field = predicted_field[self.index1, :]
        dc_indexed = self.sub_cost.dercost(predicted_field[self.index2, :])
        for dim in range(self.ndim):
            res[:, dim] += sp.bincount(self.index2, weights=dc_indexed[:, dim], minlength=self.npos)
        return res


class WeightedCost(CostFunc):
    """Returns the cost from the `sub_cost` multiplied by a float stored in `weight`
    """
    def __init__(self, sub_cost, weight):
        """Returns a cost-function that is a multiplication of the `sub-cost` by weight.

        :param sub_cost: basis cost function
        :type sub_cost: CostFunc:
        :param weight: number with which the cost function is multiplied.
        """
        self.sub_cost = sub_cost
        self.weight = weight

    @property
    def request(self, ):
        return self.sub_cost.request

    @request.setter
    def request(self, new_req):
        self.sub_cost.request = new_req

    @property
    def quadratic(self, ):
        return self.sub_cost.quadratic

    def __setattr__(self, key, value):
        if key in ('sub_cost', 'weight'):
            super(WeightedCost, self).__setattr__(key, value)
        else:
            setattr(self.sub_cost, key, value)

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        if tosum:
            return self.sub_cost.cost(predicted_field) * self.weight
        else:
            return self.sub_cost.cost(predicted_field, tosum=False) * self.weight

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        return self.mult_iter(self.sub_cost.dercost(predicted_field))

    def hessp(self, predicted_field, pfield):
        """Compute the multiplication of the hessian (at `predicted_field`) with `pfield`.

        :param predicted_field: (npos, ndim) array indicating where the hessian will be calculated
        :param pfield: (npos, ndim) array indicating in which direction the hessian will be calculated
        :return: (npos, ndim) of the local hessian times pfield
        """
        return self.mult_iter(self.sub_cost.hessp(predicted_field, pfield))

    def mult_iter(self, iter):
        """Helper function to iteratively multiply the result of sub_cost with the given weight.
        """
        if isinstance(iter, sp.ndarray):
            return self.weight * iter
        else:
            return [self.mult_iter(elem) for elem in iter]

    def qp(self, predicted_field):
        """Returns the (sparse) matrix :math:`d` and vector :math:`q` needed to rephrase the problem as a quadratic programming problem.

        For a field F, minimize
        :math:`(1/2) * F' P F + F' q`
        where element :math:`i`, dimension :math:`d` is encoded in the vector :math:`F` as :math:`F[i + d * n]`

        :param predicted_field: Returns the quadratic field at the requested position
        :return: tuple with (P, q):

            - P: (npos * ndim, npos * ndim) matrix
            - q: (npos * ndim, ) vector
        """
        def mult_iter(iter):
            """Helper function to iteratively multiply the q and P with the given weight
            """
            if isinstance(iter, tuple):
                return self.weight * iter[0], self.weight * iter[1]
            else:
                return [mult_iter(elem) for elem in iter]

        return mult_iter(self.sub_cost.qp(predicted_field))

    def __repr__(self):
        return 'WeightedCost(%r, %s)' % (self.sub_cost, self.weight)

    def __str__(self):
        return '%s * %s' % (self.weight, self.sub_cost)

    def describe_fit(self, predicted_field):
        """
        Describes the fit quality.

        :param predicted_field: (npos, ndim) array
        :return: statement of the fit quality
        """
        return '%f %s' % (self.weight, self.sub_cost.descibe_fit(predicted_field))


class PowerCost(CostFunc):
    """Raises the cost function result in `sub_cost` to a given `power`
    """
    def __init__(self, sub_cost, power, tosum=False):
        """Returns the cost-function to the nth power.

        :param sub_cost: basis cost function
        :type sub_cost: CostFunc
        :param power: number to which the `sub_cost` is raised
        :param tosum: if True sum the cost function before raising it to the nth power
        """
        self.sub_cost = sub_cost
        self.power = power
        self.tosum = tosum
        if not sub_cost.independent_pos and not tosum:
            raise ValueError("Cost functions for %s are not independent for every position so can not raise them to some power" % sub_cost)

    @property
    def request(self, ):
        return self.sub_cost.request

    @request.setter
    def request(self, new_req):
        self.sub_cost.request = new_req

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        if self.tosum:
            if not tosum:
                raise ValueError('Can not return cost function without summing, because base cost function already sums the values')
            base = self.sub_cost.cost(predicted_field)
            return base ** self.power
        else:
            base = self.sub_cost.cost(predicted_field, tosum=False)
            if tosum:
                return (base ** self.power).sum()
            else:
                return base ** self.power

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        dc = self.sub_cost.dercost(predicted_field)
        if self.tosum:
            return self.power * self.sub_cost.cost(predicted_field) ** (self.power - 1) * dc
        else:
            select = slice(None) if predicted_field.ndim == 1 else (slice(None), None)
            return self.power * self.sub_cost.cost(predicted_field, tosum=False)[select] ** (self.power - 1) * dc

    def hessp(self, predicted_field, pfield):
        """Compute the multiplication of the hessian (at `predicted_field`) with `pfield`.

        :param predicted_field: (npos, ndim) array indicating where the hessian will be calculated
        :param pfield: (npos, ndim) array indicating in which direction the hessian will be calculated
        :return: (npos, ndim) of the local hessian times pfield
        """
        dc = self.sub_cost.dercost(predicted_field)
        hp = self.sub_cost.hessp(predicted_field, pfield)
        if self.tosum:
            ct = self.sub_cost.cost(predicted_field)
            dcp = (dc * pfield).sum()
        else:
            ct = self.sub_cost.cost(predicted_field, tosum=False)[:, None]
            dcp = (dc * pfield).sum(-1)[:, None]
        part1 = (self.power * (self.power - 1) * ct ** (self.power - 2) *
                 dcp * dc)
        part2 = self.power * ct ** (self.power - 1) * hp
        return part1 + part2

    def __repr__(self):
        return 'PowerCost(%r, %s)' % (self.sub_cost, self.power)

    def __str__(self):
        return '%s^%s' % (self.sub_cost, self.power)


@numba.jit(nopython=True)
def _smooth_cost_numba(predicted_field, indices1, indices2):
    res = 0.
    for idx1, idx2 in zip(indices1, indices2):
        for dim in range(predicted_field.shape[1]):
            res += (predicted_field[idx1, dim] - predicted_field[idx2, dim]) ** 2
    return res


class SumCost(CostFunc):
    """One or more constraints.
    """
    def __init__(self, constraints):
        self.constraints = tuple(constraints)

    @property
    def request(self):
        return MultRequest([con.request for con in self.constraints])

    @property
    def quadratic(self):
        return all(con.quadratic for con in self.constraints)

    @property
    def ndim(self, ):
        return self.constraints[0].ndim

    def __len__(self):
        return len(self.constraints)

    def cost(self, predicted_field, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :param tosum: if False return the cost evaluated for each requested position rather than the full cost
        :return: float, which should be minimized (or (npos, ) array if tosum is False)
        """
        if tosum:
            return sp.sum([con.cost(field) for con, field in zip_longest(self.constraints, predicted_field)])
        else:
            return sp.sum([con.cost(field, tosum=False) for con, field in zip_longest(self.constraints, predicted_field)], 0)

    def dercost(self, predicted_field):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param predicted_field: (npos, ndim) array
        :return: (npos, ndim) defining which direction the predicted field should move to INCREASE the cost function
        """
        return [con.dercost(field) for con, field in zip_longest(self.constraints, predicted_field)]

    def hessp(self, predicted_field, pfield):
        """Compute the multiplication of the hessian (at `predicted_field`) with `pfield`.

        :param predicted_field: (npos, ndim) array indicating where the hessian will be calculated
        :param pfield: (npos, ndim) array indicating in which direction the hessian will be calculated
        :return: (npos, ndim) of the local hessian times pfield
        """
        return [con.hessp(field, pf) for con, field, pf in zip_longest(self.constraints, predicted_field, pfield)]

    def qp(self, predicted_field):
        """Returns the (sparse) matrix :math:`d` and vector :math:`q` needed to rephrase the problem as a quadratic programming problem.

        For a field F, minimize
        :math:`(1/2) * F' P F + F' q`
        where element :math:`i`, dimension :math:`d` is encoded in the vector :math:`F` as :math:`F[i + d * n]`

        :param predicted_field: Returns the quadratic field at the requested position
        :return: tuple with (P, q):

            - P: (npos * ndim, npos * ndim) matrix
            - q: (npos * ndim, ) vector
        """
        return (con.qp(field) for con, field in zip_longest(self.constraints, predicted_field))

    def add_constraint(self, new_constraint):
        """Extends the cost function by adding an additional constraint
        """
        self.constraints = self.constraints + (new_constraint, )

    def __repr__(self):
        return 'SumCost(%r)' % (self.constraints, )

    def __str__(self):
        return '(%s)' % (' + '.join([str(con) for con in self.constraints]))

    def describe_fit(self, predicted_field):
        """
        Describes the fit quality.

        :param predicted_field: (npos, ndim) array
        :return: statement of the fit quality
        """
        desc = [sc.describe_fit(sf) for sc, sf in zip(self.constraints, predicted_field)]
        return ' + '.join(desc)
