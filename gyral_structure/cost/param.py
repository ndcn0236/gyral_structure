"""Terms of the cost function that take the model parameters rather than the vector field as input

- :class:`ParamCost`: superclass of all such cost functions
- :class:`L2Params`: L2 constraint on the parameters
- :class:`ParamVecMult`: minimize the multiplication of the parameters with a vector
- :class:`ParamMatMult`: minimize :math:`p A p - q p` for some matrix :math:`A` and vector :math:`q` (for parameters :math:`p`)
- :class:`ParamTargetVec`: minimize :math:`|A p - q|` for some matrix :math:`A` and vector :math:`q` (for parameters :math:`p`)
"""
from .core import GenericCost
from ..request import ParamRequest


class ParamCost(GenericCost):
    """Cost function based on parameters rather than the vector field.
    """
    request = ParamRequest


class L2Params(ParamCost):
    """Computes the L2 norm on the raw parameters rather than the vector field.
    """
    def cost(self, params, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param params: (nparams, ) array
        :param tosum: if False return the cost evaluated for each parameter rather than the full cost
        :return: float, which should be minimized (or (nparams, ) array if tosum is False)
        """
        if tosum:
            return (params ** 2).sum()
        else:
            return params ** 2

    def dercost(self, params):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param params: (nparams, ) array
        :return: (nparams, ) defining which direction the predicted field should move to INCREASE the cost function
        """
        return 2 * params


class ParamVecMult(ParamCost):
    """Minimizes the multiplication of the parameters with some vector of equal length
    """
    def __init__(self, vector):
        self.vector = vector

    def cost(self, params, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param params: (nparams, ) array
        :param tosum: if False return the cost evaluated for each parameter rather than the full cost
        :return: float, which should be minimized (or (nparams, ) array if tosum is False)
        """
        if tosum:
            return (self.vector * params).sum()
        else:
            return self.vector * params

    def dercost(self, params):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param params: (nparams, ) array
        :return: (nparams, ) defining which direction the predicted field should move to INCREASE the cost function
        """
        return self.vector


class ParamMatMult(ParamCost):
    """Minimizes the result of matrix multiplication with the parameters

    For parameters :math:`p` the cost function will return:
    :math:`p' \cdot self.matrix \cdot p + self.vector \cdot p`
    """
    def __init__(self, matrix, vector=None):
        """Creates a new ParamMatMult

        :param matrix: matrix with which the parameter vector will be multiplied twice
        :param vector: vector with which the parameter vector will be multiplied (defaults to all zeros)
        """
        self.matrix = matrix
        self.vector = vector

    def cost(self, params, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param params: (nparams, ) array
        :param tosum: if False return the cost evaluated for each parameter rather than the full cost
        :return: float, which should be minimized (or (nparams, ) array if tosum is False)
        """
        if not tosum:
            raise ValueError('Can not return cost function without summing')
        res = self.matrix.dot(params).dot(params)
        if self.vector is not None:
            res += (self.vector * params).sum()
        return res

    def dercost(self, params):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param params: (nparams, ) array
        :return: (nparams, ) defining which direction the predicted field should move to INCREASE the cost function
        """
        res = 2 * self.matrix.dot(params)
        if self.vector is not None:
            res += self.vector
        return res


class ParamTargetVec(ParamCost):
    """Minimizes the Euclidean offset between the multiplication of a matrix with the parameters and a target vector

    minimizes :math:`|self.matrix \cdot parameters - self.vector|_2`
    """
    def __init__(self, matrix, vector):
        """Creates a new ParamTargetVec

        :param matrix: (nvec, nparams) matrix mapping the parameters to a (nvec, ) array
        :param vector: (nvec, ) target vector
        """
        self.matrix = matrix
        self.vector = vector

    def cost(self, params, tosum=True):
        """Computes the cost-function given the predicted field at the provided positions.

        :param params: (nparams, ) array
        :param tosum: if False return the cost evaluated for each vector element rather than the full cost
        :return: float, which should be minimized (or (nparams, ) array if tosum is False)
        """
        res = self.matrix.dot(params)
        res -= self.vector
        res **= 2
        if tosum:
            return res.sum()
        return res

    def dercost(self, params):
        """Computes the gradient of the cost-function with respect to the predicted field at the provided positions.

        :param params: (nparams, ) array
        :return: (nparams, ) defining which direction the predicted field should move to INCREASE the cost function
        """
        inp_vec = self.matrix.dot(params)
        inp_vec -= self.vector
        return 2 * self.matrix.T.dot(inp_vec)