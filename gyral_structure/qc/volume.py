"""
Produces several QC plots for surfaces
"""

from gyral_structure.request import read_volume
from gyral_structure.basis import BasisFunc
from gyral_structure.utils import dyad_to_mm
import numpy as np
import os.path as op
from . import nsplit
import nibabel as nib


def plot_all(mask_img: nib.Nifti1Image, dyads: nib.Nifti1Image, basis: BasisFunc,
             params=(), directory='.', skip_nsplit=False, skip_plots=False):
    """
    Plots all the QC plots on the surface

    :param mask_img: which voxels to include
    :param dyads: DTI V1 dyads to compare to
    :param basis: basis function
    :param params: parameters
    :param directory: directory to store the plots
    :param skip_nsplit: skip the (slow) nsplit plot if True
    :param skip_plots: skip all plots if True
    """
    mask = mask_img.get_data() > 0
    request = read_volume(mask_img, nsplit=1)
    field_voxels = basis.get_evaluator(request)(params)

    density = np.sqrt((field_voxels ** 2).sum(-1))
    alignment = abs(np.sum(dyad_to_mm(dyads.get_fdata()[mask], mask_img.affine) * field_voxels, -1)) / density

    if not (skip_nsplit or skip_plots):
        import matplotlib.pyplot as plt
        fig, axes = plt.subplots(1, 1)
        nsplit.plot_offset(request, basis, params, axes=axes, nsplit_ref=3)
        fig.savefig(op.join(directory, 'nsplit.png'))

    for toplot, name, label in [
        (density, 'density',  'N(streamlines)'),
        (alignment, 'alignment',  'V1 alignment'),
    ]:
        plot_array(mask_img, toplot, op.join(directory, name), label, skip_plots=skip_plots)


def plot_array(img, arr, name, label, skip_plots=False):
    """
    Produces QC plots for array on surface

    :param img: image with the mask
    :param arr: array evaluated on the faces of the surface
    :param name: basename of the output file
    :param label: text describing the array
    :param skip_plots: skip all plots if True
    """
    full_arr = np.zeros(img.shape, dtype=arr.dtype)
    full_arr[img.get_fdata() > 0] = arr
    nib.Nifti1Image(full_arr, None, header=img.header).to_filename(f'{name}.nii.gz')

    if not skip_plots:
        import matplotlib.pyplot as plt
        fig, axes = plt.subplots(1, 1)
        axes.hist(arr, bins=51)
        axes.set_xlabel(label)
        axes.set_ylabel('N(voxels)')
        fig.savefig(f'{name}_hist.png')
