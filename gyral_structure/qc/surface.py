"""
Produces several QC plots for surfaces
"""

from mcot.surface import CorticalMesh
from gyral_structure.request import read_surface
from gyral_structure.basis import BasisFunc
from nibabel import cifti2
import numpy as np
import os.path as op
from . import nsplit


def faces_to_vertices(arr, surface: CorticalMesh):
    """
    Maps array defined on the faces to the vertices

    :param arr: (Nfaces, ) array
    :param surface: cortical surface
    :return: (Nvertices, ) array
    """
    mapping = surface.graph_connection_point()
    nneigh = np.array(mapping.dot(np.ones(surface.nfaces, dtype=int))).flatten()
    return np.array(mapping.dot(arr)).flatten() / nneigh


def plot_all(surface: CorticalMesh, mask, nstream, basis: BasisFunc,
             params=(), directory='.', skip_nsplit=False, skip_plots=False):
    """
    Plots all the QC plots on the surface

    :param surface: cortical mesh to be shown
    :param mask: which vertices to include
    :param nstream: number of streamlines expected
    :param basis: basis function
    :param params: parameters
    :param directory: directory to store the plots
    :param skip_nsplit: skip the (slow) nsplit plot if True
    :param skip_plots: skip all plots if True
    """
    masked_surface = surface[mask]
    request = read_surface(masked_surface, nsplit=1)
    field_faces = basis.get_evaluator(request)(params)

    ncrossing = (field_faces * masked_surface.normal().T).sum(-1)
    density = np.sqrt((field_faces ** 2).sum(-1))
    radiality = ncrossing / density

    expected = nstream / masked_surface.size_faces()

    if not (skip_nsplit or skip_plots):
        import matplotlib.pyplot as plt
        fig, axes = plt.subplots(1, 1)
        nsplit.plot_offset(request, basis, params, axes=axes)
        fig.savefig(op.join(directory, 'nsplit.png'))

    scl = cifti2.ScalarAxis([
        'normed density (crossing)', 'normed density (total)',
        'density (crossing)', 'density (total)', 'radiality'
    ])
    bm = cifti2.BrainModelAxis.from_mask(mask, name=surface.anatomy.cifti)
    cifti2.Cifti2Image(
        np.stack([faces_to_vertices(arr, masked_surface) for arr in [ncrossing / expected, density / expected,
                                                                     ncrossing, density, radiality]], axis=0),
        header=cifti2.Cifti2Header.from_axes((scl, bm))
    ).to_filename(op.join(directory, 'field.dscalar.nii'))

    if not skip_plots:
        for toplot, name, label in [
            (ncrossing / expected, 'density_crossing', r'$\rho$(streamlines crossing)'),
            (density / expected, 'density_total',  r'$\rho$(streamlines)'),
            (radiality, 'radiality',  'radial alignment'),
        ]:
            plot_array(surface, mask, toplot, op.join(directory, name), label)


def plot_array(surface: CorticalMesh, mask, arr_faces, name, label):
    """
    Produces QC plots for array on surface

    :param surface: cortical surface
    :param mask: which vertices to include
    :param arr_faces: array evaluated on the faces of the surface
    :param name: basename of the output file
    :param label: text describing the array
    """
    import matplotlib.pyplot as plt
    fig, axes = plt.subplots(1, 1)
    axes.hist(arr_faces, bins=51)
    axes.set_xlabel(label)
    axes.set_ylabel('N(faces)')
    fig.savefig(f'{name}_hist.pdf')

    full_arr_vertices = np.full(surface.nvertices, np.nan)
    full_arr_vertices[mask] = faces_to_vertices(arr_faces, surface[mask])

    surface.render(full_arr_vertices, view='+x', filename=f'{name}_lateral.png', interact=False)
    surface.render(full_arr_vertices, view='-x', filename=f'{name}_medial.png', interact=False)
