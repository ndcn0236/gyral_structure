"""
Produces plot to validate the value of `nsplit` for a particular request
"""

from gyral_structure.request import FieldRequest
from gyral_structure.basis import BasisFunc
import numpy as np
import seaborn as sns


def plot_offset(request: FieldRequest, basis: BasisFunc, params=(), nsplit_ref=4, axes=None):
    """
    Checks whether there has been overfitting

    Does increasing the `nsplit` lead to a significant shift

    :param request: where the cost function should be evaluated (voxels or vertices)
    :param basis: basis function
    :param params: parameters
    :param nsplit_ref: reference nsplit value
    :param axes: axis to plot on
    """

    def get_field(nsplit):
        request.nsplit = nsplit
        return basis.get_evaluator(request)(params)

    ref_field = get_field(nsplit_ref)

    dens = {}
    for nsplit in range(1, nsplit_ref):
        field = get_field(nsplit) - ref_field
        dens[nsplit] = np.sqrt(np.sum(field ** 2, 1))

    if axes is None:
        import matplotlib.pyplot as plt
        axes = plt.gca()
    xval = np.concatenate([np.ones(arr.size) * nsplit for nsplit, arr in dens.items()]).astype(int)
    yval = np.concatenate([arr for nsplit, arr in dens.items()])
    violin_plot = sns.violinplot(xval, yval, scale='width')
    axes.set_xlabel('nsplit')
    axes.set_ylabel(f'error in density\nrelative to nsplit={nsplit_ref}')
    return violin_plot
