import gzip
import bz2
import numpy as np


def get_filetype(filename):
    """Determines whether the filename is zipped based solely on the filename extension

    :return: File object used to open the filename
    """
    extension = filename.split('.')[-1]
    if extension == 'bz2' or extension == 'bz':
        return bz2.BZ2File
    if extension == 'gz':
        return gzip.GzipFile
    return open


def dyad_to_mm(dyads, affine):
    """
    Converts the dyads from the FSL convention to mm space

    Can be inverted using :func:`dyad_from_mm`.

    :param dyads: (..., 3) array with the dyads as loaded from e.g. a dti_V1 file
    :param affine: (4, 4) or (3, 3) array with the affine transformation from voxel to mm space
        (in the latter case excluding translation)
    :return: (..., 3) array with the normalised dyads in mm space
    """
    ndim = dyads.shape[-1]
    if affine.shape == (ndim + 1, ndim + 1):
        affine = affine[:-1, :-1]

    if np.linalg.det(affine) > 0:
        # bvecs assume negative determinant, so need to flip around x-axis
        dyads_tmp = dyads.copy()
        dyads_tmp[..., 0] *= -1
    else:
        dyads_tmp = dyads
    mm_dyads_arr = dyads_tmp.dot(affine.T)
    mm_dyads_arr /= np.sqrt((mm_dyads_arr ** 2).sum(-1))[..., None]
    return mm_dyads_arr


def dyad_from_mm(dyads, affine):
    """
    Converts the dyads from mm space to the FSL convention

    Can be inverted using :func:`dyad_to_mm`.

    :param dyads: (..., 3) array with the dyads in mm space
    :param affine: (4, 4) or (3, 3) array with the affine transformation from voxel to mm space
        (in the latter case excluding translation)
    :return: (..., 3) array with the normalised dyads in the FSL convention
    """
    ndim = dyads.shape[-1]
    if affine.shape == (ndim + 1, ndim + 1):
        affine = affine[:-1, :-1]

    use_affine = np.linalg.inv(affine)
    fsl_dyads = dyads.dot(use_affine.T)
    fsl_dyads /= np.sqrt((fsl_dyads ** 2).sum(-1))[..., None]
    if np.linalg.det(affine) > 0:
        # bvecs assume negative determinant, so need to flip around x-axis
        fsl_dyads[..., 0] *= -1
    return fsl_dyads
