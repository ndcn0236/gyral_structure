"""Defines the two central classes defining the basis function, that maps parameters to vector fields

- :py:class:`BasisFunc` should be used as a superclass for any parameter to field mapper
- :py:class:`SumBase` can be used to create a field by summing multiple other basis functions

In addition a helper function :func:`wrap_numba` is defined, which is meant to assist in writing implementations of the basis functions using numba
"""
from collections import UserList
import numpy as np
import scipy as sp
from scipy import sparse
import pickle
from ..algorithm import Algorithm
from .. import request, algorithm
from ..utils import get_filetype
import h5py
from warnings import warn


def wrap_numba(npos, ndim, nparams, scalar=True, sparse=None):
    """Vectorizes a numba function that computes a single element of the matrix mapping parameters to fields

    The resulting function can be used to define a basis function

    :param npos: number of positions where the field will be calculated
    :param ndim: dimensionality of the space (i.e. 2- or 3-dimensional)
    :param nparams:  number of parameters
    :param scalar: If True the numba function only computes a scalar output rather than the full vector
    :param sparse: Optionally two equal-length lists of integers indexing which positions and parameters affect each other
    :return: wrapper function that converts an input function with signature
        (position, idx_param, derivative=-1) -> float
        to an output function with signature
        (positions, parameters, derivative=-1, inverse=False) -> field
    """
    from numba import jit
    if sparse is not None:
        arr_pos, arr_param = sparse

    def wrapper(func):
        """Wraps a numba function computing single element in matrix into a function doing the matrix multiplication.

        :param func: (positions, idx_param, derivative=-1) -> float; this function returns the parameter at idx_param should be multiplied with to compute the field.
        :return: (positions, parameters, derivative=-1, inverse=False) -> does a matrix or inverse matrix multiplication on the parameters
        """
        if sparse is None:
            if scalar:
                @jit(nopython=True)
                def matrix_mult_numba_invert(result, parameters, derivative=-1):
                    for idx_param in range(nparams):
                        result[idx_param] = 0.
                        for idx_pos in range(npos):
                            result[idx_param] += parameters[idx_pos] * func(idx_pos, idx_param, derivative)
            else:
                @jit(nopython=True)
                def matrix_mult_numba_invert(result, parameters, tmp_ndim, derivative=-1):
                    for idx_param in range(result.size):
                        result[idx_param] = 0.
                        for idx_pos in range(npos):
                            func(tmp_ndim, idx_pos, idx_param, derivative)
                            result[idx_param] += np.sum(parameters[idx_pos] * tmp_ndim)

            if scalar:
                @jit(nopython=True)
                def matrix_mult_numba(result, parameters, derivative=-1):
                    for idx_pos in range(npos):
                        result[idx_pos] = 0.
                        for idx_param in range(parameters.size):
                            result[idx_pos] += parameters[idx_param] * func(idx_pos, idx_param, derivative)
            else:
                @jit()
                def matrix_mult_numba(result, parameters, tmp_ndim, derivative=-1):
                    for idx_pos in range(npos):
                        result[idx_pos] = 0.
                        for idx_param in range(parameters.size):
                            func(tmp_ndim, idx_pos, idx_param, derivative)
                            result[idx_pos] += parameters[idx_param] * tmp_ndim
        else:
            if scalar:
                @jit(nopython=True)
                def matrix_mult_numba_invert(result, parameters, derivative=-1):
                    for idx_param in range(result.size):
                        result[idx_param] = 0.
                    for idx_param, idx_pos in zip(arr_param, arr_pos):
                        result[idx_param] += parameters[idx_pos] * func(idx_pos, idx_param, derivative)
            else:
                @jit(nopython=True)
                def matrix_mult_numba_invert(result, parameters, tmp_ndim, derivative=-1):
                    for idx_param in range(result.size):
                        result[idx_param] = 0.
                    for idx_param, idx_pos in zip(arr_param, arr_pos):
                        func(tmp_ndim, idx_pos, idx_param, derivative)
                        result[idx_param] += np.sum(parameters[idx_pos] * tmp_ndim)

            if scalar:
                @jit(nopython=True)
                def matrix_mult_numba(result, parameters, derivative=-1):
                    for idx_pos in range(npos):
                        result[idx_pos] = 0.
                    for idx_param, idx_pos in zip(arr_param, arr_pos):
                        result[idx_pos] += parameters[idx_param] * func(idx_pos, idx_param, derivative)
            else:
                @jit(nopython=True)
                def matrix_mult_numba(result, parameters, tmp_ndim, derivative=-1):
                    for idx_pos in range(npos):
                        result[idx_pos] = 0.
                    for idx_param, idx_pos in zip(arr_param, arr_pos):
                        func(tmp_ndim, idx_pos, idx_param, derivative)
                        result[idx_pos] += parameters[idx_param] * tmp_ndim

        def matrix_mult(parameters, derivative=-1, inverse=False):
            """Matrix multiplication using numba.

            Computes a scalar field at the given positions for a set of parameters.
            :param parameters: (nparams, ) array of the parameters setting the field
            :param derivative: set to 0, 1, or 2 to compute the derivative along that dimension (default: no derivative)
            :param inverse: use the matrix transpose for multiplication
            :return: (npos, ) array of the scalar field at the given `positions`
            """
            parameters = sp.atleast_1d(parameters)
            res = sp.atleast_1d(sp.zeros(nparams if inverse else (npos if scalar else (npos, ndim))))
            args = ((res, parameters, derivative) if scalar else
                    (res, parameters, sp.zeros(ndim), derivative))
            if inverse:
                matrix_mult_numba_invert(*args)
            else:
                matrix_mult_numba(*args)
            return res
        return matrix_mult
    return wrapper


class BasisFunc(object):
    """Defines a basis function mapping 2/3D positions to a 2/3D vector field.

    Call this object with an (npos, ndim) array of positions to get a function which computes the field from parameters or the other way around

    The user of any subclass will typically call one of two methods:

    - :meth:`get_evaluator` which returns a :class:`RequestEvaluator <.evaluator.RequestEvaluator>` that maps parameters to the field at a fixed position (used during field optimisation)
    - :meth:`param_evaluator` which returns a function that maps positions to the field for a fixed set of parameters (used during streamline tractography).

    Subclasses should override three methods:

    - :meth:`get_partial_mat`: returns the matrix to compute the potential or vector field at given positions
    - :meth:`get_func`: return functions that compute the potential or vector field at given positions
    - :meth:`validate_pos`: returns True for any positions where the basis function has been defined (optional)

    What these functions return is defined by setting the following attributes:

    - :attr:`potential`
    - :attr:`scalar`
    - :attr:`compute`
    """
    potential = True
    """If True the matrix/function is expected to map to the vector potential rather than vector field (ensures that the result is divergence-free)"""
    scalar = True
    """If True the matrix/function is expected to return only a single dimension o the vector field or potential rather than the full vector field or potential"""
    ndim = 3
    """dimensionality of the space in which the basis function is defined"""
    compute = ()
    """sequence of possible algorithms that can be used in the evaluation (e.g., is there a gpu or a matrix implementation?)"""

    def __init__(self, nparams):
        """Creates a new Basis function.

        :param nparams: number of parameters that will be passed to the basis function
        """
        self.nparams = nparams

    @property
    def scalar_nparams(self, ):
        """Number of parameters used to define a single dimension of the output vector field
        """
        if not self.scalar or (self.potential and self.ndim == 2):
            return self.nparams
        else:
            if (self.nparams % self.ndim) != 0:
                raise ValueError("For scalar basis function number of parameters (%i) should be divisible by the number of dimensions (%i)" % (self.nparams, self.ndim))
            return self.nparams // self.ndim

    def get_partial_mat(self, positions, derivative=-1):
        """Computes the matrix used to map parameters to the vector field or potential at the given positions

        Should be overwritten by sub-classes. What it exactly calculates depends on the `potential` and `scalar` flags:

        - if the :attr:`potential` flag is set (default) then the matrix should compute the vector potential rather than fiber orientations
        - if the :attr:`scalar` flag is set return one value per position rather than :attr:`ndim`

        :param positions: (npos, 3) array with the positions of interest
        :param derivative: which spatial derivative to compute (default: no derivative)
        :return: (npos, nparams // 3) array mapping the parameters to the positions
        """
        raise NotImplementedError("%s can not compute matrices" % type(self))

    def stack_matrices(self, positions):
        """Returns the (npos, nparams) matrix that maps the parameters to the n-dimensional field.

        Combines the sub-matrices returned by `get_partial_mat` on the given positions.

        :param positions: (npos, ndim) array with the locations where the field should be evaluated
        :return: (npos, nparams) matrix mapping the parameters to the vector field
        """
        def stack_matrices(to_stack, axis=0):
            """Stacks to_stack across the axis."""
            assert len(to_stack) == self.ndim
            if (axis == 1) or (axis == -1):
                return stack_matrices([m.T if m is not 0 else 0 for m in to_stack], axis=0).T
            if axis != 0 and axis != -2:
                raise ValueError('axis should be -2, -1, 0, or 1')
            sample_mat = [m for m in to_stack if m is not 0][0]
            shape = list(sample_mat.shape)
            shape[0] *= self.ndim
            if sparse.issparse(sample_mat):
                full_row, full_col, full_data = [], [], []
                for idx_mat, sub_mat in enumerate(to_stack):
                    if sub_mat is not 0:
                        coo_mat = sub_mat.tocoo()
                        full_row.append(coo_mat.row * self.ndim + idx_mat)
                        full_col.append(coo_mat.col)
                        full_data.append(coo_mat.data)
                return sparse.coo_matrix((sp.concatenate(full_data), (sp.concatenate(full_row), sp.concatenate(full_col))),
                                         shape=shape)
            else:
                arr = sp.zeros(shape)
                for dim, sub_mat in enumerate(to_stack):
                    if sub_mat is not 0:
                        arr[dim::self.ndim, :] = sub_mat
                return arr
        if self.potential:
            if self.ndim == 3:
                if self.scalar:
                    der_mat = []
                    for der in range(3):
                        der_mat.append(self.get_partial_mat(positions, derivative=der))
                    if self.nparams % self.ndim != 0:
                        raise ValueError("Number of parameters (%i) not divisible by dimensions" % self.nparams)
                    line_mat = []
                    for dim_field in range(3):
                        mat = [0] * 3
                        dim1 = (dim_field + 1) % 3
                        dim2 = (dim_field + 2) % 3
                        mat[dim1] = -der_mat[dim2]
                        mat[dim2] = der_mat[dim1]
                        line_mat.append(stack_matrices(mat, 1))
                    mat = stack_matrices(line_mat, 0)
                    assert mat.shape == (positions.size, self.nparams)
                    return mat
                else:
                    raise ValueError("Non-scalar potential for 3 dimensions has not been implemented yet")
            else:
                matrices = (-self.get_partial_mat(positions, derivative=1), self.get_partial_mat(positions, derivative=0))
                if sparse.issparse(matrices[0]):
                    row, col, data = [], [], []
                    for mat in matrices:
                        coo_matrix = mat.tocoo()
                        row.append(coo_matrix.row * self.ndim)
                        col.append(coo_matrix.col)
                        data.append(coo_matrix.data)
                    return sparse.coo_matrix((sp.concatenate(data), (sp.concatenate(row), sp.concatenate(col))),
                                             shape=(positions.shape[0], self.nparams))
                else:
                    return sp.stack(matrices, 1).reshape(positions.size, self.nparams)
        else:
            if self.scalar:
                matrix = self.get_partial_mat(positions)
                return stack_matrices((matrix, ) * self.ndim)
            else:
                return self.get_partial_mat(positions)

    def get_full_mat(self, req):
        """Returns the matrix used to __call__ the given request.

        :param req: Defines where the field should be evaluated
        :type req: request.FieldRequest
        :return: (npos, nparams) matrix mapping the parameters to the vector field
        """
        if not isinstance(req, request.FieldRequest):
            req = request.PositionRequest(req)
        mat = None
        for positions, weights in req.split():
            w = sp.asarray(weights)
            if w.ndim == 1:
                w = w[:, None]
            unweighted = self.stack_matrices(positions)
            if (w == 1).all():
                new_mat = unweighted
            elif sparse.issparse(unweighted):
                new_mat = unweighted.multiply(w)
            else:
                new_mat = w * unweighted

            if mat is None:
                mat = new_mat
            else:
                mat += new_mat
        return mat

    def get_func(self, positions, method):
        """Creates a function that evaluates the requested field at the `positions`.

        Should be overwritten by sub-classes. What it exactly calculates depends on the `potential` and `scalar` flags:
        - if the `potential` flag is set (default) then the matrix should compute the vector potential rather than fiber orientations
        - if the `scalar` flag is set return one value per position rather than `ndim`

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (field derivative to parameter derivative if inverse else param to field)
        :raises: NotImplementedError if the basis function is not defined for a specific method
        """
        raise NotImplementedError("Algorithm %s not implemented for %s" % (method, type(self)))

    def get_evaluator(self, req, method=None):
        """Computes the function used to __call__ the :mod:`request <../request>` for a given :mod:`method <../algorithm>`.

        raises NotImplementedError if the method has not been implemented for this basis function and request.

        :param req: defines where the field should be evaluated
        :type req: request.FieldRequest
        :param method: the preferred algorithm to use when evaluating the field at the requested positions
        :type method: algorithm.Algorithm or string
        :return: an :class:`RequestEvaluator <.evaluator.RequestEvaluator>` that has been set up to quickly evaluate the field
            at the requested positions given a vector field.
        """
        from .evaluator import MultEvaluator
        with algorithm.set(method=method, override=True):
            return MultEvaluator(self, req)

    def param_evaluator(self, parameters, method=None, extent=0, nsim=2**12):
        """Returns a function that evaluates the field at a given location

        Used for streamline evaluation.

        :param parameters: set of parameters for which the field should be evaluated at different positions
        :param method: algorithm to use when computing field
        :param extent: maximum extent of request to accept (keep at zero for tractography)
        :param nsim: maximum number of positions to evaluate simultaneously
        :return: function to map the positions to the field
        """
        from .evaluator import MultEvaluator
        self.precompute_evaluator(extent)
        with algorithm.set(store_matrix=False, method=method):
            sim_evaluator = MultEvaluator(self, request.PositionRequest(np.zeros((nsim, self.ndim))))
            if sim_evaluator.use_mat:
                warn("Running tractography with pre-computed matrix is very slow")

        def evaluate(req):
            """Evaluate the field at the requested positions

            :param req: (npos, ndim) array or object defining where the field should be evaluated
            :type req: request.FieldRequest
            """
            if isinstance(req, request.FieldRequest):
                all_pos = []
                all_weights = []
                for pos, weights in req.split():
                    all_pos.append(pos)
                    all_weights.append(weights)
            else:
                all_pos = req
            all_pos = np.asanyarray(all_pos)
            flat_all_pos = all_pos.reshape((-1, self.ndim))
            flat_res = np.zeros(flat_all_pos.shape)
            for idx in range(0, flat_all_pos.shape[0], nsim):
                set_pos = flat_all_pos[idx:idx+nsim, :]
                sim_evaluator.update_pos(set_pos)
                part_res = sim_evaluator(parameters, inverse=False)
                assert part_res.shape == set_pos.shape
                if sim_evaluator.use_cuda and hasattr(part_res, 'get'):
                    part_res = part_res.get()
                flat_res[idx:idx+nsim] = part_res
            res = flat_res.reshape(all_pos.shape)
            if not isinstance(req, request.FieldRequest):
                return res
            else:
                total_res = np.sum([weight * r for weight, r in zip(all_weights, res)], 0)
                return total_res
        return evaluate

    def validate(self, req):
        """Checks for which voxels/vertices/positions in the request the basis function is defined.

        :param req: defines where the basis function should be evaluated
        :type req: request.FieldRequest
        :return: boolean array which is True, wherever the request can be safely evaluated
        """
        use = sp.ones(req.npos, dtype='bool')
        for positions, _ in req.split():
            use &= self.validate_pos(positions)
        return use

    def validate_pos(self, positions):
        """Whether the summed basis function can be computed at the provided positions

        :param positions: (npos, 3) array with the positions of interest
        :return: (npos, ) boolean array, which is True if the positions are within range of all basis functions
        """
        return sp.ones(positions.shape[0], dtype='bool')

    def save(self, filename, solution=()):
        """Saves the basis function with the solution to an HDF5 file

        :param filename: HDF5 (.h5) file to store the basis function & solution in
        :param solution: array with the best-fit parameters
        """
        with h5py.File(filename, 'w') as f:
            f['solution'] = solution
            self.to_hdf5(f.create_group('basis'))

    def precompute_evaluator(self, extent=0):
        """
        Prepares the evaluator generator to quickly generate results for different requests

        Called before doing tractography

        :param extent: maximum size of the request
        """
        pass

    def to_hdf5(self, group: h5py.Group):
        """
        Stores the basis function in the provided HDF5 group

        :param group: HDF5 group, where the basis function will be stored
        """
        raise NotImplementedError("Basisfunc subclass {} does not know how to write itself to HDF5".format(self.__class__.__name__))

    @classmethod
    def from_hdf5(cls, group: h5py.Group):
        """
        Reads the basis function from the provided HDF5 group

        :param group: HDF5 group, where the basis function is stored
        :return: newly read basis function
        """
        raise NotImplementedError("Basisfunc subclass {} does not know how to read itself from HDF5".format(cls.__name__))


class SumBase(UserList):
    """Combination of multiple basis functions, which are added together to create a single net vector field

    After creation the user will typically call one of two methods:

    - :meth:`get_evaluator` which returns a :class:`MultRequestEvaluator <.evaluator.MultEvaluator>` that maps parameters to the net field at a fixed position (used during field optimisation)
    - :meth:`param_evaluator` which returns a function that maps positions to the net field for a fixed set of parameters (used during streamline tractography).

    If not all basis functions should be updated during the optimisation, those can be fixed to a set of parameters
    using :meth:`fix` or :meth:`fix_all`. They can later be released using :meth:`release`.
    """
    def __init__(self, initial_list=None):
        """Combines the fields from one or more Basis functions.

        :param initial_list: list of sub-classes of BasisFunc (or SumBase)
        """
        super(SumBase, self).__init__(initial_list)
        for bf in self:
            if bf.ndim != self.ndim:
                raise ValueError("All basis functions should expect the same number of dimensions (ndim)")
        self._fixed = {}

    @property
    def compute(self, ):
        """
        A sequence of algorithms that can be used to define *all* the basis functions comprising the net field
        """
        res = set(self[0].compute)
        for bf in self:
            res.intersection_update(bf.compute)
        return res

    @property
    def nparams(self, ):
        """Total number of parameters required to supply all the basis functions
        """
        return sp.sum([elem.nparams for index, elem in enumerate(self) if index not in self._fixed],
                      dtype='i4')

    @property
    def ndim(self, ):
        """Dimensionality of the space in which the basis functions have been defined
        """
        return self[0].ndim

    def fix(self, index, params):
        """Fixes a basis function with the given parameters

        Fixed basis functions will only compute the resulting vector field once (for the given parameters) and then consistently return that output

        :param index: integer index indicating which basis function will be fixed
        :param params: array with the parameters used to fix the vector field of the given basis function
        """
        if index < 0:
            index += len(self)
        if self[index].nparams is not None and self[index].nparams != params.size:
            raise ValueError("Parameter array has the wrong size to fix basis function")
        self._fixed[index] = params

    def fix_all(self, params):
        """Fixes all basis function with the given parameters

        Fixed basis functions will only compute the resulting vector field once (for the given parameters) and then consistently return that output

        :param params: (nparams, ) array with the parameters used to fix the vector field of the given basis function
        """
        if params.size != self.nparams:
            raise ValueError("Expected {:d} parameters, but got {:d}".format(self.nparams, params.size))
        for idx in range(len(self)):
            if idx not in self._fixed:
                self.fix(idx, params[:self[idx].nparams])
                params = params[self[idx].nparams:]
        assert params.size == 0

    def release(self, index):
        """Removes the fix from a given basis function

        Fixed basis functions will consistently return the same vector field without getting any parameters
        """
        del self._fixed[index]

    def release_all(self, ):
        """Removes the fix from all the basis functions

        Fixed basis functions will consistently return the same vector field without getting any parameters
        """
        self._fixed = {}

    def param_evaluator(self, parameters, method=None, extent=0, nsim=2**12):
        """Returns a function that evaluates the basis function for arbitrary positions given a fixed parameter array

        Used to __call__ streamlines

        :param parameters: (nparams, ) array defining the parameters for which the field will be evaluated
        :param method: Algorithm used to __call__ the basis functions
        :param extent: maximum extent of request to accept (keep at zero for tractography)
        :param nsim: maximum number of positions to evaluate simultaneously
        :return: function that maps positions to vector field
        """
        funcs = []
        idx_param = 0
        for idx_elem, elem in enumerate(self):
            if idx_elem in self._fixed:
                funcs.append(elem.param_evaluator(self._fixed[idx_elem], extent=extent, method=method, nsim=nsim))
            else:
                funcs.append(elem.param_evaluator(parameters[idx_param:idx_param + elem.nparams],
                                                  extent=extent, method=method, nsim=nsim))
                idx_param += elem.nparams

        def evaluate(positions):
            """Evaluates the vector field at given positions

            :param positions: (npos, ndim) array with the positions of interest
            :return: (npos, ndim) array with the vector field at these positions
            """
            results = [f(positions) for f in funcs]
            return sp.sum(results, 0)
        return evaluate

    def validate_pos(self, positions):
        """Whether the summed basis function can be computed at the provided positions

        :param positions: (npos, 3) array with the positions of interest
        :return: (npos, ) boolean array, which is True if the positions are within range of all basis functions
        """
        return sp.all([bf.validate_pos(positions) for bf in self], 0)

    def save(self, filename, solution=()):
        """Saves the basis functions with the solution to an HDF5 file

        :param filename: HDF5 (.h5) file to store the basis function & solution in
        :param solution: array with the best-fit parameters
        """
        with h5py.File(filename, 'w') as f:
            f['solution'] = solution
            self.to_hdf5(f.create_group('basis'))

    def get_evaluator(self, req, method=None):
        """Computes the function used to __call__ the `request` for a given `method`.

        raises NotImplementedError if the method has not been implemented for this basis function and request.

        :param req: defines where the field should be evaluated
        :type req: request.FieldRequest
        :param method: method used to __call__ the request
        :type method: Algorithm
        """
        from .evaluator import MultEvaluator
        with algorithm.set(method=method):
            return MultEvaluator(self, req)

    def to_hdf5(self, group: h5py.Group):
        """
        Stores the basis functions in the provided HDF5 group

        :param group: HDF5 group, where the basis function will be stored
        """
        group.attrs['name'] = 'SumBase'
        group.attrs['Nbasis'] = len(self)
        fixed_group = group.create_group('_fixed')
        for name, value in self._fixed.items():
            fixed_group[str(name)] = value
        for idx, bf in enumerate(self):
            bf.to_hdf5(group.create_group('basis{}'.format(idx)))

    @classmethod
    def from_hdf5(cls, group: h5py.Group):
        """
        Reads the basis functions from the provided HDF5 group

        :param group: HDF5 group, where the basis function is stored
        :return: newly read basis function
        """
        bf_list = []
        for idx in range(group.attrs['Nbasis']):
            bf_group = group['basis{}'.format(idx)]
            bf_cls = get_basisfunc(bf_group.attrs['name'])
            bf_list.append(bf_cls.from_hdf5(bf_group))
        res = cls(bf_list)
        for name, value in group['_fixed'].items():
            res.fix(int(name), np.array(value))
        return res


def read(filename):
    """Loads the basis function and best-fit parameters from a pickle file.

    :param filename: file to load the basis function & solution from
    """
    with h5py.File(filename, 'r') as f:
        solution = np.array(f['solution'])
        bf_group = f['basis']
        basis_function = get_basisfunc(bf_group.attrs['name']).from_hdf5(bf_group)
    return basis_function, solution


def read_pickle(filename):
    """Loads the basis function and best-fit parameters from a pickle file.

    Pickle format is now outdated, use HDF5 instead

    :param filename: file to load the basis function & solution from
    """
    with get_filetype(filename)(filename, 'rb') as file:
        basis_function, solution = pickle.load(file)
    return basis_function, solution


def get_basisfunc(name):
    """
    Gets the basis function from the class name

    :param name: name of a subclass of Basisfunc or SumBase
    :return: class with the name
    :raise: KeyError if subclass does not exist (or has not been imported yet)
    """

    def all_subclasses(cls):
        """
        Recursively find everything derived from cls

        Code from https://stackoverflow.com/questions/3862310/how-to-find-all-the-subclasses-of-a-class-given-its-name
        """
        sub = cls.__subclasses__()
        res = {c.__name__: c for c in sub}
        for c in sub:
            res.update(all_subclasses(c))
        return res

    if name == 'SumBase':
        return SumBase
    else:
        return all_subclasses(BasisFunc)[name]
