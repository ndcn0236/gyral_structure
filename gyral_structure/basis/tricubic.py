"""
This module is no longer actively maintained

Tricubic interpolation based on the vector potential and jacobians at the grid points.
"""

from .grid import Grid
import scipy as sp
from numpy import linalg
from scipy import special, sparse


class Tricubic(Grid):
    """Tricubic interpolation based on the vector potential and the jacobian defined at the voxel corners.
    """
    scalar = True
    potential = True

    @property
    def params_per_grid(self, ):
        return 24 if self.alldiff else 12

    def __init__(self, mask, affine=None, alldiff=False):
        """Creates a grid at least covering the voxels in the mask.

        The field will be computable in every voxel centered on the locations of the non-zero values in `mask`

        :param mask: True where the field should be computable
        :param affine: conversion from voxel to mm space (default: no transformation)
        :param alldiff:
        """
        self.alldiff = alldiff
        super(Tricubic, self).__init__(mask, affine)

    def get_partial_mat(self, positions, derivative=-1):
        """Returns the sparse matrix that computes the derivative of a scalar potential at the given positions

        Note that the resulting derivatives are in voxel-space, not mm-space (`get_full_mat` converts them to mm-space).

        :param positions: (npos, ndim) float array of positions of interest in mm
        :param derivative: -1 to compute the potential, (0, 1, 2) to compute the derivative in respectively (x, y, z)
        """
        idx_params = self.contributors(positions)[..., None] + sp.arange(8 if self.alldiff else 4) * self.nparams // self.params_per_grid
        _, rel_pos = self.to_indices(positions, return_rel_pos=True)
        nder = sp.array(self._nder)
        if derivative != -1:
            nder[derivative] += 1
        kernel = grid_to_pos_scalar(rel_pos, nder)
        if not self.alldiff:
            kernel = kernel[..., :4]
        use = abs(kernel) > 1e-10
        wuse = sp.where(use)
        row = wuse[0]
        col = idx_params[use]
        data = kernel[use]
        return sparse.coo_matrix((data, (row, col)), shape=(positions.shape[0], self.scalar_nparams))


poly_coeffs = sp.mgrid[:4, :4, :4].reshape(3, 64).T
cell_coords = (sp.mgrid[:2, :2, :2] - 0.5).reshape(3, 8).T


def poly_to_pos_scalar(rel_pos, nder=(0, 0, 0)):
    """Maps the polynomial parameters to the derivative of a scalar field.

    :param rel_pos: (..., 3) array with the relative position wrt voxel center (between -0.5 and 0.5)
    :param nder: power of the derivative in each spatial dimension
    :return: (..., 64) array mapping the 32 polynomial parameters to the scalar field derivatives
    """
    if rel_pos.shape[-1] != 3:
        raise ValueError("position should be 3-dimensional")
    res = sp.zeros(rel_pos.shape[:-1] + (64, ))
    power = poly_coeffs.copy()
    power -= sp.asarray(nder)
    norm_lower = sp.prod(special.factorial(power), 1)
    use = norm_lower != 0
    norm = sp.prod(special.factorial(poly_coeffs[use]), 1) / norm_lower[use]
    res[..., use] = norm * sp.prod(rel_pos[..., None, :] ** power[use], -1)
    return res


def _poly_to_grid_scalar():
    """Maps the polynomial parameters to the grid points.

    The choice of derivatives is based on equation 6 from Lekien & Marsden (2005)

    :return: (2, 2, 2, 8, 64) array with
     - first 3 dimensions determining which corner of the voxel is considered
     - 3rd dimension determines the derivative (0: no derivative, 1: x, 2: y, 3: z, 4: xy, 5: xz, 6: yz, 7: xyz)
     - 4th dimension is the index of the polynomial parameter
    """
    i, j, k = sp.around(cell_coords.T + 0.5).astype('i4')
    res = sp.zeros((2, 2, 2, 8, 64))
    for idxdiff, nder in enumerate([(0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1),
                                    (1, 1, 0), (1, 0, 1), (0, 1, 1), (1, 1, 1)]):
        res[i, j, k, idxdiff] = poly_to_pos_scalar(cell_coords, nder)
    return res

poly_to_grid_scalar = _poly_to_grid_scalar()


def _grid_to_poly_scalar():
    """Maps the polynomial parameters to the grid points.

    :return: (64, 2, 2, 2, 8) array with
     - 1st dimension is the index of the polynomial parameter
     - next 3 dimensions determining which corner of the voxel is considered
     - 4th dimension determines the derivative (0: no derivative, 1: x, 2: y, 3: z)
    """
    return linalg.inv(poly_to_grid_scalar.reshape(64, 64)).reshape(64, 2, 2, 2, 8)

grid_to_poly_scalar = _grid_to_poly_scalar()


def grid_to_pos_scalar(rel_pos, nder=(0, 0, 0)):
    """Maps the scalar/derivative of the scalar potential from the voxel corners to any points within the voxel

    :param rel_pos: (..., 3) array with the relative position wrt voxel center (between -0.5 and 0.5)
    :param nder: power of the derivative in each spatial dimension
    :return: (..., 2, 2, 2, 8) array with
     - first 3 dimensions determining which corner of the voxel is considered
     - 4th dimension determines the derivative (0: no derivative, 1: x, 2: y, 3: z)
    """
    return sp.tensordot(poly_to_pos_scalar(rel_pos, nder), grid_to_poly_scalar, axes=1)






