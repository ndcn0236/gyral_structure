"""
This module is no longer actively maintained
"""
from gyral_structure.algorithm import Algorithm
import scipy as sp
from .core import BasisFunc
from numpy import linalg
import itertools
from scipy import sparse
from .evaluator import FuncRequestEvaluator, MatRequestEvaluator


class Grid(BasisFunc):
    """Basis function defined on a grid.

    Every grid element should only affect the field in the direct neighbourhood.
    """
    ndim = 3
    _nder = (0, 0, 0)
    params_per_grid = None

    def __init__(self, mask, affine=None):
        """Creates a grid at least covering the voxels in the mask.

        The field will be computable in every voxel centered on the locations of the non-zero values in `mask`

        :param mask: True where the field should be computable
        :param affine: conversion from voxel to mm space (default: no transformation)
        """
        # set/fix the affine
        if affine is None:
            affine = sp.eye(4)
        affine[-1, :] = 0
        affine[-1, -1] = 1

        self.flipped = linalg.det(affine) < 0

        self.idx_field = -sp.ones(mask.shape, dtype='i4')
        self.idx_field[mask] = sp.arange(mask.sum())
        self.affine_field = affine

        mask_params = sp.zeros(sp.array(mask.shape) + 1, dtype='bool')
        for select in itertools.product([slice(None, -1), slice(1, None)], repeat=3):
            mask_params[select] |= mask
        idx_params = -sp.ones(mask_params.shape)
        idx_params[mask_params] = sp.arange(mask_params.sum())

        self.field_to_param = -sp.ones((mask.sum(), 2, 2, 2), dtype='i4')
        for idx, select in zip(itertools.product((0, 1), repeat=3),
                               itertools.product([slice(None, -1), slice(1, None)], repeat=3)):
            self.field_to_param[(slice(None), ) + idx] = idx_params[select][mask]

        assert self.field_to_param.max() + 1 == mask_params.sum()
        self.param_to_field = -sp.ones((self.field_to_param.max() + 1, 2, 2, 2), dtype='i4')
        for idx_offset in itertools.product(range(2), repeat=self.ndim):
            idxp = self.field_to_param[(slice(None), ) + idx_offset]
            self.param_to_field[(idxp, ) + idx_offset] = sp.arange(self.field_to_param.shape[0])

    @property
    def nparams(self, ):
        """Total number of parameters describing the vector potential"""
        return self.param_to_field.shape[0] * self.params_per_grid

    def to_indices(self, positions, return_rel_pos=False):
        """Returns the voxel indices that the `positions` are on the `self.idx_field` grid

        :param positions: (npos, ndim) array of positions of interest in mm
        :param return_rel_pos: if True, returns the position relative to the voxel center (between -0.5 and 0.5) as well
        :return: (npos, ndim) int array of voxel indices.
            Also returns an (npos, ndim) float array of offsets from the voxel center.
        """
        affine = linalg.inv(self.affine_field)
        vox_pos = sp.dot(affine[:-1, :-1], positions.T).T + affine[:-1, -1]
        ind = sp.around(vox_pos).astype('i4')
        if return_rel_pos:
            return ind, vox_pos - ind
        return ind

    def validate_pos(self, positions):
        """Tests whether the field is defined at the positions

        :param positions: (npos, ndim) array of positions of interest in mm
        :return: (npos, ) bool array, which is True if the position is on the mask
        """
        ind = self.to_indices(positions)
        valid = ((ind >= 0) & (ind < self.idx_field.shape)).all(-1)
        valid[valid] = self.idx_field[tuple(ind[valid].T)] != -1
        return valid

    def contributors(self, positions):
        """Returns the indices of the parameter indices contributing to defining the field at `positions`

        :param positions: (npos, ndim) float array of positions of interest in mm
        :return: (npos, 2, 2, 2) int array defining the parameter indices describing the potential in the (2x2x2) neighbourhood around the `positions`
        """
        spatial_indices = self.to_indices(positions)
        if (spatial_indices < 0).any() or (spatial_indices >= self.idx_field.shape).any():
            raise ValueError("Some of the provided positions are outside of the image")
        idxf = self.idx_field[tuple(spatial_indices.T)]
        if (idxf == -1).any():
            raise ValueError("Some of the provided positions are outside of the mask")
        res = self.field_to_param[idxf]
        return res

    def get_full_mat(self, positions):
        """Returns the (npos, nparams) matrix that maps the parameters to the 3-dimensional field in mm space.

        Combines the sub-matrices returned by `get_partial_mat` on the positions.

        The function in `BasisFunc` is overriden so that the field affine transformation can be applied to map the derivative in voxel space to the derivative in mm space

        :param positions: (npos, ndim) positions where the field should be evaluated
        """
        in_mat = super(Grid, self).get_full_mat(positions).tocoo()
        row, col, data = [], [], []
        for in_dim in range(3):
            use = (in_mat.row % 3) == in_dim
            for out_dim in range(3):
                if self.affine_field[out_dim, in_dim] != 0:
                    row.append(in_mat.row[use] + out_dim - in_dim)
                    col.append(in_mat.col[use])
                    data.append(in_mat.data[use] * self.affine_field[out_dim, in_dim])
        row, col, data = [sp.concatenate(arr) for arr in (row, col, data)]
        res_mat = sparse.coo_matrix((data, (row, col)), shape=in_mat.shape)
        return res_mat

    def get_full_func(self, positions, method):
        """Combines the evaulations provided by `get_func` to create a function that maps the parameters to the full field at `positions`

        The function in `BasisFunc` is overriden so that the field affine transformation can be applied to map the derivative in voxel space to the derivative in mm space

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse) -> output (derfield to derparam if inverse else param to field)
        """
        func = super(Grid, self).get_full_func(positions, method)
        def affine(parameters, inverse=False):
            """Apply an affine transformation.
            """
            if inverse:
                parameters = self.apply_affine(parameters, inverse=True)
            res = func(parameters, inverse=inverse)
            if inverse:
                return res
            return self.apply_affine(res, inverse=False)
        return affine

    def apply_affine(self, orientations, inverse=False):
        """Converts orientations in voxel-space to mm-space (by default) or from mm-space to voxel-space (if `inverse` is True)

        NOTE: fiber orientations are expected to be in mm-space not voxel-space

        :param orientations: (norient, ndim) array defining the orientations in voxel space (or mm space if inverse is True)
        :param inverse: if set convert from mm to voxel space rather than the other way around
        :return: (norient, ndim) array defining the orientations in mm space (or voxel space if inverse is True)
        """
        if not inverse:
            return sp.dot(self.affine_field[:3, :3], orientations.T).T
        else:
            return sp.dot(sp.linalg.inv(self.affine_field[:3, :3]), orientations.T).T


class GridFuncRequestEvaluator(FuncRequestEvaluator):
    def evaluate_results(self, params, inverse=False):
        if inverse:
            params = self.basis.apply_affine(params, inverse=True)
        super(GridFuncRequestEvaluator, self).evaluate_results(params, inverse=inverse)

    def combine_results(self, inverse=False):
        res = super(GridFuncRequestEvaluator, self).combine_results(inverse=inverse)
        if inverse:
            return res
        return self.basis.apply_affine(res, inverse=False)
