"""
This module is no longer actively maintained

Implements a basis function that defines the potential on a trilinear grid.
"""
from gyral_structure.algorithm import Algorithm
import scipy as sp
from numpy import linalg
from scipy import special, sparse
from .grid import Grid


class TriLinear(Grid):
    """Defines the potential as a trilinear interpolation from a regular parameter grid.
    """
    params_per_grid = 3
    compute = ('numba', 'matrix', 'matrix_cuda')

    def get_partial_mat(self, positions, derivative=-1):
        """Returns the sparse matrix that computes the derivative of a scalar potential at the given positions

        Note that the resulting derivatives are in voxel-space, not mm-space (`get_full_mat` converts them to mm-space).

        :param positions: (npos, ndim) float array of positions of interest in mm
        :param derivative: -1 to compute the potential, (0, 1, 2) to compute the derivative in respectively (x, y, z)
        """
        idx_params = self.contributors(positions)
        _, rel_pos = self.to_indices(positions, return_rel_pos=True)
        nder = sp.array(self._nder)
        if derivative != -1:
            nder[derivative] += 1
        kernel = grid_to_pos_scalar(rel_pos, nder)
        use = abs(kernel) > 1e-10
        row = sp.where(use)[0]
        col = idx_params[use]
        data = kernel[use]
        return sparse.coo_matrix((data, (row, col)), shape=(positions.shape[0], self.scalar_nparams))

    def get_func(self, positions, method):
        """Creates a function that evaluates the derivative of a scalar potential at the `positions`.

        Note that the resulting derivatives are in voxel-space, not mm-space (`get_full_func` converts them to mm-space).

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (derfield to derparam if inverse else param to field)
        """
        idx_field = self.idx_field
        field_to_param = self.field_to_param
        affine = sp.array(linalg.inv(self.affine_field))
        nder = self._nder
        tmp_neigh = sp.zeros((2, 2, 2))
        if method == Algorithm.numba:
            import numba
            @numba.jit(nopython=True)
            def matrix_mult_numba(result, parameters, tmp_neigh, derivative=-1, inverse=False):
                """Computes the derivative of the potential at the point of interest (or the inverse)"""
                derx = nder[0]
                dery = nder[1]
                derz = nder[2]
                if derivative == 0:
                    derx += 1
                elif derivative == 1:
                    dery += 1
                elif derivative == 2:
                    derz += 1
                if (derx >= 2) or (dery >= 2) or (derz >= 2):
                    for idx_res in range(result.shape[0]):
                        result[idx_res] = 0.
                    return
                for idx_pos in range(positions.shape[0]):
                    # convert to voxel coordinates
                    xpos = 0.
                    ypos = 0.
                    zpos = 0.
                    for dim in range(4):
                        if dim == 3:
                            pos = 1
                        else:
                            pos = positions[idx_pos, dim]
                        xpos += affine[0, dim] * pos
                        ypos += affine[1, dim] * pos
                        zpos += affine[2, dim] * pos
                    ip = round(xpos)
                    jp = round(ypos)
                    kp = round(zpos)
                    idxf = idx_field[ip, jp, kp]
                    xv = xpos - ip
                    yv = ypos - ip
                    zv = zpos - ip

                    if not inverse:
                        for x in (0, 1):
                            for y in (0, 1):
                                for z in (0, 1):
                                    tmp_neigh[x, y, z] = parameters[field_to_param[idxf, x, y, z]]
                                if derz == 0:
                                    tmp_neigh[x, y, 0] = ((0.5 - zv) * tmp_neigh[x, y, 0] +
                                                          (0.5 + zv) * tmp_neigh[x, y, 1])
                                else:
                                    tmp_neigh[x, y, 0] = tmp_neigh[x, y, 1] - tmp_neigh[x, y, 0]
                            if dery == 0:
                                tmp_neigh[x, 0, 0] = ((0.5 - yv) * tmp_neigh[x, 0, 0] +
                                                      (0.5 + yv) * tmp_neigh[x, 1, 0])
                            else:
                                tmp_neigh[x, 0, 0] = tmp_neigh[x, 1, 0] - tmp_neigh[x, 0, 0]
                        if derx == 0:
                            tmp_neigh[0, 0, 0] = ((0.5 - xv) * tmp_neigh[0, 0, 0] +
                                                  (0.5 + xv) * tmp_neigh[1, 0, 0])
                        else:
                            tmp_neigh[0, 0, 0] = tmp_neigh[1, 0, 0] - tmp_neigh[0, 0, 0]
                        result[idx_pos] = tmp_neigh[0, 0, 0]
                    else:
                        tmp_neigh[0, 0, 0] = parameters[idx_pos]
                        if derx == 0:
                            tmp_neigh[1, 0, 0] = (0.5 + xv) * tmp_neigh[0, 0, 0]
                            tmp_neigh[0, 0, 0] = (0.5 - xv) * tmp_neigh[0, 0, 0]
                        else:
                            tmp_neigh[1, 0, 0] = tmp_neigh[0, 0, 0]
                            tmp_neigh[0, 0, 0] = -tmp_neigh[0, 0, 0]
                        for x in (0, 1):
                            if dery == 0:
                                tmp_neigh[x, 1, 0] = (0.5 + yv) * tmp_neigh[x, 0, 0]
                                tmp_neigh[x, 0, 0] = (0.5 - yv) * tmp_neigh[x, 0, 0]
                            else:
                                tmp_neigh[x, 1, 0] = tmp_neigh[x, 0, 0]
                                tmp_neigh[x, 0, 0] = -tmp_neigh[x, 0, 0]
                            for y in (0, 1):
                                if derz == 0:
                                    tmp_neigh[x, y, 1] = (0.5 + zv) * tmp_neigh[x, 0, 0]
                                    tmp_neigh[x, y, 0] = (0.5 - zv) * tmp_neigh[x, 0, 0]
                                else:
                                    tmp_neigh[x, y, 1] = tmp_neigh[x, 0, 0]
                                    tmp_neigh[x, y, 0] = -tmp_neigh[x, 0, 0]
                                for z in (0, 1):
                                    result[field_to_param[idxf, x, y, z]] = tmp_neigh[x, y, z]

            def matrix_mult(parameters, derivative=-1, inverse=False):
                """Matrix multiplication using numba.

                Computes a scalar field at the given positions for a set of parameters.
                :param parameters: (nparams, ) array of the parameters setting the field
                :param derivative: set to 0, 1, or 2 to compute the derivative along that dimension (default: no derivative)
                :param inverse: use the matrix transpose for multiplication
                :return: (npos, ) array of the scalar field at the given `positions`
                """
                res = sp.zeros(self.scalar_nparams if inverse else positions.shape[0])
                matrix_mult_numba(res, parameters, tmp_neigh, derivative, inverse)
                return res
            return matrix_mult
        return super(TriLinear, self).get_func(positions, method)


poly_coeffs = sp.stack(sp.meshgrid(*(sp.arange(2), ) * 3, indexing='ij'), 0)
cell_coords = poly_coeffs - 0.5

def _poly_to_grid(alternative=False):
    """Creates the matrix mapping the linear coefficients to the scalar values on the grid

    Note that the grid values are assumed to be defined at (-0.5, +0.5) in every dimension (i.e. the origin is the cell centre)

    :return: (x, y, z, i, j, k) maps the value at (x - 0.5, y - 0.5, z - 0.5) to a_{ijk} x^i y^j z^k
    """
    return (cell_coords[:, :, :, :, None, None, None] ** poly_coeffs[:, None, None, None, :, :, :]).prod(0)
poly_to_grid = _poly_to_grid()


def _grid_to_poly():
    """Creates the matrix mapping the 2x2x2 neighbourhood to the linear coefficients.

    Note that the grid values are assumed to be defined at (-0.5, +0.5) in every dimension (i.e. the origin is the cell centre)

    :return: (i, j, k, x, y, z) maps the value at (x - 0.5, y - 0.5, z - 0.5) to a_{ijk} x^i y^j z^k
    """
    return linalg.inv(poly_to_grid.reshape(8, 8)).reshape((2, ) * 6)
grid_to_poly = _grid_to_poly()


def poly_to_pos_scalar(rel_pos, derivative=(0, 0, 0)):
    """Given a position relative to the voxel center computes the mapping from the polynomial parameter to the scalar.

    :param rel_pos: (npos, 3)-array like with positions in voxel coordinates relative to the voxel centre (betweeen -0.5 and +0.5)
    :param derivative: computes the derivative in the given dimension (default: no derivative)
    :return: (npos, 2, 2, 2) array which multiplied with the polynomial coefficients gives the scalar value.
    """
    rel_pos = sp.asarray(rel_pos)
    if (abs(rel_pos) > 0.5).any():
        raise ValueError("Can only compute the scalar within the voxel of interest")
    pc = poly_coeffs - sp.array(derivative, dtype='i4')[:, None, None, None]
    norm = special.factorial(pc)
    coeffs = sp.zeros(norm.shape)
    use = norm != 0
    coeffs[use] = special.factorial(poly_coeffs[use]) / norm[use]
    pc[~use] = 0
    return (coeffs * rel_pos[..., None, None, None] ** pc).prod(-4)


def grid_to_pos_scalar(rel_pos, derivative=(0, 0, 0)):
    """Given a position relative to the voxel center computes the mapping from the voxel corners to the point.

    :param rel_pos: (npos, 3)-array like with a position in voxel coordinates relative to the voxel centre (betweeen -0.5 and +0.5)
    :param derivative: computes the derivative in the given dimension (default: no derivative)
    :return: (2, 2, 2) array which multiplied with the scalars in the voxel corers gives the scalar value at the point of interest
    """
    return (grid_to_poly * poly_to_pos_scalar(rel_pos, derivative=derivative)[..., None, None, None]).sum((-4, -5, -6))


def grid_to_pos_field(rel_pos, derivative=(0, 0, 0), curl=True):
    """Computes the mapping from the field potential at the corners to the position of interest.

    :param rel_pos: (npos, 3) array with positions relative to the voxel center of point of interest (between -0.5 and 0.5)
    :param derivative: computes the derivative in the given dimension (default: no derivative)
    :param curl: if True computes the fiber field rather than the field potential at the point of interest
    :return: (npos, 3, 2, 2, 2, 3) array which when multiplied with the 3D scalar field in the 2x2x2 neighbourhood gives the field at the point of interest
        The vector potential at the voxel corners should be represented as a (2, 2, 2, 3) array
    """
    rel_pos = sp.asarray(rel_pos)
    sol_3d = sp.zeros(rel_pos.shape + (2, 2, 2, 3))
    if not curl:
        sol_1d = grid_to_pos_scalar(rel_pos, derivative=derivative)
        for idx in range(3):
            sol_3d[..., idx, :, :, :, idx] = sol_1d
    else:
        sol_1d = []
        der = sp.asarray(derivative)
        for dim in range(3):
            der_curl = der.copy()
            der_curl[dim] += 1
            sol_1d.append(grid_to_pos_scalar(rel_pos, der_curl))
        for idx1 in range(3):
            idx2 = (idx1 + 1) % 3
            idx3 = (idx1 + 2) % 3
            sol_3d[..., idx1, :, :, :, idx3] = sol_1d[idx2]
            sol_3d[..., idx1, :, :, :, idx2] = -sol_1d[idx3]
    return sol_3d

