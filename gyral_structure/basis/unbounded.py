"""
Provides several basis functions:

- :class:`ChargeDistribution`
- :class:`Gradient`
- :class:`Fourier`
- :class:`Polynomial`
"""
from .core import BasisFunc, wrap_numba
from ..algorithm import Algorithm
import scipy as sp
import numpy as np
import h5py


class Fourier(BasisFunc):
    """A set of Fourier basis functions in `ndim` space.
    """
    potential = True
    scalar = True
    compute = ('matrix', 'numba', 'matrix_cuda')

    def __init__(self, nparams, ndim=3, nfreq=8., basefreq=1.):
        """Creates a new Fourier basis function

        :param nparams: Number of parameters for each of the potential field components
        :param ndim: Number of physical dimensions
        :param nfreq: Maximum frequency along any dimension is `nfreq` times `basefreq`
        :param basefreq: lowest frequency
        """
        self.ndim = ndim
        super(Fourier, self).__init__(nparams=nparams)
        self.nfreq = nfreq
        self.basefreq = basefreq
        frequencies = sp.stack(sp.broadcast_arrays(*[self._get_freq(ixdim) for ixdim in range(ndim)]), -1).reshape(-1, ndim) * basefreq
        tot_freq = sp.sum(frequencies ** 2, -1)
        if self.scalar_nparams % 2 != 0:
            raise ValueError("Fourier basis function should have an even number of parameters, not %i" % self.scalar_nparams)
        self.frequencies = frequencies[sp.argsort(tot_freq), :][1:self.scalar_nparams // 2 + 1, :]
        if self.frequencies.shape[0] != self.scalar_nparams // 2:
            raise ValueError("Fourier basis function has less frequencies (%i) than half the number of parameters (%i)" % (self.frequencies.shape[0], self.scalar_nparams))

    def _get_freq(self, ixdim):
        """Helper funtion to return the possible frequencies across a dimension (relative to `basefreq`)

        :param ixdim: index of the dimension (between 0 and `ndim`-1)
        :return: `ndim`-dimensional array with the frequencies
        """
        nf = sp.arange(self.nfreq)
        select = [None] * self.ndim
        select[ixdim] = slice(None)
        return nf[tuple(select)]

    def get_partial_mat(self, positions, derivative=-1):
        """Computes the matrix used to map parameters to the potential scalar field at the given positions

        :param positions: (npos, 3) array with the positions of interest (shift and scaling have already been applied)
        :param derivative: which spatial derivative to compute (default: no derivative)
        :return: (npos, nparams // 3) array mapping the parameters to the positions
        """
        phase = 2 * sp.pi * (self.frequencies * positions[:, None, :]).sum(-1)
        if derivative == -1:
            return sp.stack([sp.sin(phase), sp.cos(phase)], -1).reshape(positions.shape[0], self.nparams)
        derphase = 2 * sp.pi * self.frequencies[:, derivative]
        return (derphase[:, None] * sp.stack([sp.cos(phase), -sp.sin(phase)], -1)).reshape(positions.shape[0], self.scalar_nparams)

    def get_func(self, positions, method):
        """Creates a function that evaluates the requested field at the `positions`.

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (field derivative to parameter derivative if inverse else param to field)
        :raises: NotImplementedError if the basis function is not defined for a specific method
        """
        if method == Algorithm.cuda:
            from .. import cuda
            code = cuda.wrap_get_element(r"""
            #include <math_constants.h>

            __device__ void get_element({dtype} *result, int idx_pos, int idx_param{params_in}) {{
                {dtype} phase = 0.0;
                for (int idx_dim = 0; idx_dim < {ndim}; idx_dim ++){{
                    phase += freq[idx_param / 2 * {ndim} + idx_dim] * pos[idx_pos * {ndim} + idx_dim];
                }}
                phase *= 2 * CUDART_PI_F;
                if ({derivative} == -1){{
                    if (idx_param % 2 == 0) *result = sin(phase); else *result = cos(phase);
                }} else {{
                    if (idx_param % 2 == 0) *result = cos(phase); else *result = -sin(phase);
                    *result *= 2 * CUDART_PI_F * freq[idx_param / 2 * {ndim} + {derivative}];
                }}
            }}
            """)
            def update_pos(params, new_positions):
                params['pos'][:new_positions.shape[0] * self.ndim] = new_positions.astype(cuda.dtype).flatten()
            return cuda.CudaMatrixMult(code, positions.shape[0], positions.shape[1], self.scalar_nparams,
                                       params={'freq': (cuda.dtype + ' *freq', self.frequencies.flatten()),
                                               'pos': (cuda.dtype + ' *pos', positions.flatten())}, scalar=True,
                                       update_pos=update_pos)
        elif method == Algorithm.numba:
            frequencies = self.frequencies
            from numba import jit
            @wrap_numba(positions.shape[0], positions.shape[0], self.scalar_nparams, scalar=True)
            @jit(nopython=True)
            def func(idx_pos, idx_param, derivative=-1):
                """Computes the factor by which parameter `idx_param` should be multiplied to compute its contribution at `positions`

                Note that this function has to be preceded by @numba.jit.

                :param idx_pos: (ndim, ) array selecting a point of interest
                :param idx_param: index of the parameter to consider
                :param derivative: whether to compute the derivative in a spatial dimension (set to 0, 1, or 2)
                """
                freq = idx_param // 2
                phase = 2 * np.pi * np.sum(frequencies[freq, :] * positions[idx_pos])
                if derivative == -1:
                    if idx_param % 2 == 0:
                        return np.sin(phase)
                    else:
                        return np.cos(phase)
                else:
                    derphase = 2 * np.pi * frequencies[freq, derivative]
                    if idx_param % 2 == 0:
                        return derphase * np.cos(phase)
                    else:
                        return -derphase * np.sin(phase)

            return func

        super(Fourier, self).get_func(positions, method)

    def to_hdf5(self, group: h5py.Group):
        """
        Stores the radial basis function in the provided HDF5 group

        :param group: HDF5 group, where the basis function will be stored
        """
        group.attrs['name'] = self.__class__.__name__
        group.attrs['nparams'] = self.nparams
        group.attrs['ndim'] = self.ndim
        group.attrs['nfreq'] = self.nfreq
        group.attrs['basefreq'] = self.basefreq

    @classmethod
    def from_hdf5(cls, group: h5py.Group):
        """
        Reads the radial basis function from the provided HDF5 group

        :param group: HDF5 group, where the basis function is stored
        :return: newly read radial basis function
        """
        assert group.attrs['name'] == cls.__name__
        return cls(group.attrs['nparams'], group.attrs['ndim'], group.attrs['nfreq'], group.attrs['basefreq'])


class ChargeDistribution(BasisFunc):
    """Computes the expected fiber orientations given a charge distribution.

    Uses the electrostatic model (1/r^2 in 3D and 1/r in 2D)
    The parameters set how many streamlines terminate at every location
    """
    potential = False
    scalar = False
    compute = ('cuda', 'numba', 'matrix', 'matrix_cuda')

    def __init__(self, charge_pos, charge_size=0.5):
        """Creates a new charge distribution that can be used to allow streamlines to terminate.

        :param charge_pos: Positions where the streamlines terminate (i.e. the locations of the charges)
        :param charge_size: radius of sphere over which the streamlines uniformly terminate
        """
        super(ChargeDistribution, self).__init__(charge_pos.shape[0])
        self.ndim = charge_pos.shape[1]
        self.charge_pos = charge_pos
        self.charge_size = charge_size

    def get_partial_mat(self, positions):
        """Computes the matrix used to map parameters to the vector field at the given positions

        :param positions: (npos, 3) array with the positions of interest (shift and scaling have already been applied)
        :return: (npos * 3, nparams) array mapping the positions to the parameters
        """
        offset = positions[:, :, None] - self.charge_pos.T
        distsq = sp.sum(offset ** 2, -2)[:, None, :]
        norm = distsq ** (-self.ndim / 2.) / (4 * sp.pi)
        norm[distsq <= self.charge_size ** 2] = 1 / (self.charge_size ** self.ndim * 4 * sp.pi)
        field_mat = offset * norm
        return field_mat.reshape(positions.size, self.nparams)

    def get_func(self, positions, method):
        """Creates a function that evaluates the requested field at the `positions`.

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (field derivative to parameter derivative if inverse else param to field)
        :raises: NotImplementedError if the basis function is not defined for a specific method
        """
        charge_pos = self.charge_pos
        size_squared = self.charge_size ** 2
        norm_internal = self.charge_size ** -self.ndim / (4 * sp.pi)
        if method == Algorithm.numba:
            from numba import jit
            if self.ndim == 3:
                @wrap_numba(positions.shape[0], self.ndim, self.nparams, scalar=False)
                @jit(nopython=True)
                def func(tmp_ndim, idx_pos, idx_param, derivative=-1):
                    x = positions[idx_pos, 0] - charge_pos[idx_param, 0]
                    y = positions[idx_pos, 1] - charge_pos[idx_param, 1]
                    z = positions[idx_pos, 2] - charge_pos[idx_param, 2]
                    distsq = (x ** 2 + y ** 2 + z ** 2)
                    if distsq <= size_squared:
                        norm = norm_internal
                    else:
                        norm = distsq ** (-1.5) / (4 * sp.pi)
                    tmp_ndim[0] = x * norm
                    tmp_ndim[1] = y * norm
                    tmp_ndim[2] = z * norm
            elif self.ndim == 2:
                @wrap_numba(positions.shape[0], self.ndim, self.nparams, scalar=False)
                @jit(nopython=True)
                def func(tmp_ndim, idx_pos, idx_param, derivative=-1):
                    x = positions[idx_pos, 0] - charge_pos[idx_param, 0]
                    y = positions[idx_pos, 1] - charge_pos[idx_param, 1]
                    distsq = (x ** 2 + y ** 2)
                    if distsq <= size_squared:
                        norm = norm_internal
                    else:
                        norm = 1 / (4 * sp.pi * distsq)
                    tmp_ndim[0] = x * norm
                    tmp_ndim[1] = y * norm

            return func
        if method == Algorithm.cuda:
            from .. import cuda
            if self.ndim == 2:
                line = "1 / distsq"
            else:
                line = "1 / (distsq * sqrt(distsq))"
            code = cuda.wrap_get_element(r"""
            #include <math_constants.h>

            __device__ void get_element({dtype} *result, int idx_pos, int idx_param{params_in}) {{
                {dtype} offset[{ndim}];
                {dtype} norm;
                {dtype} distsq = 0.0;
                for (int idx_dim = 0; idx_dim < {ndim}; idx_dim ++){{
                    offset[idx_dim] = pos[idx_pos * {ndim} + idx_dim] - charge_pos[idx_param * {ndim} + idx_dim];
                    distsq += offset[idx_dim] * offset[idx_dim];
                }}
                if (distsq <= {size_squared}) {{
                    norm = {norm_internal};
                }} else {{
                    norm = %s * {inv_4pi};
                }}
                for (int idx_dim = 0; idx_dim < {ndim}; idx_dim ++){{
                    result[idx_dim] = offset[idx_dim] * norm;
                }}
            }}
            """ % line)
            params = {'charge_pos': (cuda.dtype + ' *charge_pos', charge_pos.flatten()),
                      'pos': (cuda.dtype + ' *pos', positions.flatten())}
            values = {'size_squared': size_squared, 'norm_internal': norm_internal, 'inv_4pi': 1 / (4 * sp.pi)}
            def update_pos(params, new_positions):
                params['pos'][:new_positions.shape[0] * self.ndim] = new_positions.astype(cuda.dtype).flatten()
            return cuda.CudaMatrixMult(code, positions.shape[0], self.ndim, self.nparams, params=params,
                                       values=values, scalar=False, update_pos=update_pos)

        super(ChargeDistribution, self).get_func(positions, method)

    def to_hdf5(self, group: h5py.Group):
        """
        Stores the radial basis function in the provided HDF5 group

        :param group: HDF5 group, where the basis function will be stored
        """
        group.attrs['name'] = self.__class__.__name__
        group['charge_pos'] = self.charge_pos
        group['charge_size'] = self.charge_size

    @classmethod
    def from_hdf5(cls, group: h5py.Group):
        """
        Reads the radial basis function from the provided HDF5 group

        :param group: HDF5 group, where the basis function is stored
        :return: newly read radial basis function
        """
        assert group.attrs['name'] == cls.__name__
        return cls(np.array(group['charge_pos']), np.array(group['charge_size']))


class Polynomial(BasisFunc):
    """Describes the vector potential as a polynomial
    """
    potential = True
    scalar = True
    compute = ('matrix', 'numba', 'matrix_cuda')#, 'cuda')

    def __init__(self, order, shift=0, scaling=1, ndim=3):
        """Sets up a polynomial basis function with the required polynomial `order`

        :param order: order of the polynomial
        :param shift: (ndim, ) array subtracted from positions before calling `use_func`
        :param scaling: (ndim, ) array with which the positions are scaled before calling `use_func`
        :param ndim: number of spatial dimensions
        """
        self.order = order
        self.shift = shift
        self.scaling = scaling
        self.ndim = ndim

    def poly_coeffs(self, ):
        """Computes all the polynomial coefficients.

        :return: (scalar_nparams, ndim) array with the polynomial coefficients
        """
        coeff_arrs = sp.meshgrid(*((sp.arange(self.order + 1, dtype='i4'), ) * self.ndim), indexing='ij')
        return sp.stack(coeff_arrs, -1).reshape((self.order + 1) ** self.ndim, self.ndim)[1:, :]

    @property
    def nparams(self, ):
        """Returns the number of parameters needed to describe the field.
        """
        per_scalar = ((self.order + 1) ** self.ndim - 1)
        if self.ndim == 2:
            return per_scalar
        else:
            return self.ndim * per_scalar

    def get_partial_mat(self, positions, derivative=-1):
        """Maps the parameters describing a scalar field to the positions.

        :param positions: (npos, 3) array with the positions of interest (scaling & shift have already been applied to these positions)
        :param derivative: which spatial derivative to compute (default: no derivative)
        :return: (npos, scalar_nparams) array mapping the parameters to the positions
        """
        if derivative == -1:
            return sp.prod(positions[:, None, :] ** self.poly_coeffs()[None, :, :], -1)
        else:
            power = self.poly_coeffs()
            power[:, derivative] -= 1
            res = sp.zeros((positions.shape[0], self.scalar_nparams))
            use = (power != -1).all(-1)
            res[:, use] = sp.prod(positions[:, None, :] ** power[None, use, :], -1)
            res[:, use] *= (power[use, derivative] + 1)
            return res

    def get_func(self, positions, method):
        """Creates a function that evaluates the requested scalar potential at the `positions`.

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (derfield to derparam if inverse else param to field)
        """
        ndim = self.ndim
        pc = self.poly_coeffs()
        if positions.shape[1] != ndim:
            raise ValueError("Positions do not have the expected dimensionality")
        if method == Algorithm.numba:
            from numba import jit
            @wrap_numba(positions.shape[0], self.ndim, self.scalar_nparams, scalar=True)
            @jit(nopython=True)
            def func(idx_pos, idx_param, derivative=-1):
                """Computes the factor by which parameter `idx_param` should be multiplied to compute its contribution at `positions`

                Note that this function has to be preceded by @numba.jit.

                :param idx_pos: index of the position of interest
                :param idx_param: index of the parameter to consider
                :param derivative: whether to compute the derivative in a spatial dimension (set to 0, 1, or 2)
                """
                if derivative != -1 and pc[idx_param, derivative] == 0:
                    return 0.
                res = 1.
                for dim in range(ndim):
                    if derivative != dim:
                        res *= positions[idx_pos, dim] ** pc[idx_param, dim]
                    else:
                        res *= pc[idx_param, dim] * (positions[idx_pos, dim] ** (pc[idx_param, dim] - 1))
                return res
            return func
        if method == Algorithm.cuda and False:
            from .. import cuda
            code = cuda.wrap_get_element(r"""
            #include <math.h>

            __device__ void get_element({dtype} *result, int idx_pos, int idx_param{params_in}) {{
                {dtype} phase = 0.0;
                int power;
                if ({derivative} != -1) {{
                    if (poly_coeffs[idx_param * {ndim} + {derivative}] == 0) {{
                        *result = 0.;
                        return;
                    }}
                }}
                *result = 1.;
                for (int dim = 0; dim < {ndim}; dim ++){{
                    power = poly_coeffs[idx_param * {ndim} + dim];
                    if (dim == {derivative}) {{
                        power -= 1;
                    }}
                    *result *= pow(positions[idx_pos * {ndim} + dim], power);
                    if (dim == {derivative}) {{
                        *result *= (power + 1);
                    }}
                }}
            }}
            """)
            return cuda.CudaMatrixMult(code, positions.shape[0], positions.shape[1], self.scalar_nparams,
                                       params={'poly_coeffs': (cuda.dtype + ' *poly_coeffs', pc.flatten()),
                                               'positions': (cuda.dtype + ' *positions', positions.flatten())},
                                       scalar=True)
        super(Polynomial, self).get_func(positions, method)

    def to_hdf5(self, group: h5py.Group):
        """
        Stores the radial basis function in the provided HDF5 group

        :param group: HDF5 group, where the basis function will be stored
        """
        group.attrs['name'] = self.__class__.__name__
        group.attrs['order'] = self.order
        group.attrs['ndim'] = self.ndim
        group['shift'] = self.shift
        group['scaling'] = self.scaling

    @classmethod
    def from_hdf5(cls, group: h5py.Group):
        """
        Reads the radial basis function from the provided HDF5 group

        :param group: HDF5 group, where the basis function is stored
        :return: newly read radial basis function
        """
        assert group.attrs['name'] == cls.__name__
        return cls(group.attrs['order'], group['shift'], group['scaling'], group.attrs['ndim'])


class Gradient(BasisFunc):
    """Produces a gradient vector field

    The field is given by
    f = a + b x
    where a is a vector of length `ndim`, b is a (ndim x ndim) matrix
    To get a divergence-free field the trace of b should be zero
    """
    potential = False
    scalar = False
    compute = ('numpy', 'matrix', 'cuda', 'matrix_cuda')

    def __init__(self, order=1, ndim=3, method=()):
        """
        Produces a basic vector field consisting of a constant field + a gradient (latter only if order is 1)

        :param order: if 0 only include a constant field term, otherwise also include a gradient
        :param ndim: number of dimensions
        :param method: sorted list of preferred evaluation methods
        """
        if order not in (0, 1):
            raise ValueError("order must be 0 or 1")
        self.order = order
        self.ndim = ndim
        self.method = method

    @property
    def nparams(self, ):
        if self.order == 0:
            return self.ndim
        elif self.order == 1:
            return self.ndim + self.ndim ** 2 - 1
        raise ValueError('order must be in (0, 1)')

    @property
    def order(self, ):
        return self._order

    @order.setter
    def order(self, value):
        if value not in (0, 1):
            raise ValueError('order must be one of (0, 1, 2)')
        self._order = value

    @property
    def ndim(self, ):
        return self._ndim

    @ndim.setter
    def ndim(self, value):
        if value not in (2, 3):
            raise ValueError('ndim must be 2 or 3-dimensional')
        self._ndim = value

    def get_a(self, params):
        if params.size != self.nparams:
            raise ValueError('Incorrect parameter size')
        return params[:self.ndim]

    def invert_a(self, dparams):
        return dparams

    def get_b(self, params):
        if params.size != self.nparams:
            raise ValueError('Incorrect parameter size')
        vals = params[self.ndim:(self.ndim + self.ndim ** 2 -1)]
        if self.ndim == 2:
            return sp.append(vals, -vals[0]).reshape(2, 2)
        else:
            return sp.append(vals, -vals[0] - vals[4]).reshape(3, 3)

    def invert_b(self, dparams):
        res = dparams.flatten()[:-1].copy()
        res[0] -= dparams[-1, -1]
        if self.ndim == 3:
            res[4] -= dparams[-1, -1]
        return res

    def get_partial_mat(self, positions):
        """Computes the matrix used to map parameters to the vector field at the given positions

        :param positions: (npos, 3) array with the positions of interest
        :return: (npos * 3, nparams) array mapping the parameters to the positions
        """
        res = sp.zeros((positions.size, self.nparams))
        for dim in range(self.ndim):
            res[dim::self.ndim, dim] = 1
        if self.order == 1:
            bmat = sp.zeros((positions.size, self.ndim, self.ndim))
            for dim in range(self.ndim):
                bmat[dim::self.ndim, :, dim] = positions
            for dim in range(self.ndim - 1):
                bmat[:, dim, dim] -= bmat[:, -1, -1]
            res[:, self.ndim:] = bmat.reshape(positions.size, self.ndim ** 2)[:, :-1]
        return res

    def get_func(self, positions, method):
        """Creates a function that evaluates the requested scalar potential at the `positions`.

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (derfield to derparam if inverse else param to field)
        """
        if method == Algorithm.numpy:
            def matrix_mult(parameters, derivative=-1, inverse=False):
                """Matrix multiplication using numba.

                Computes a scalar field at the given positions for a set of parameters.
                :param parameters: (nparams, ) array of the parameters setting the field
                :param derivative: set to 0, 1, or 2 to compute the derivative along that dimension (default: no derivative)
                :param inverse: use the matrix transpose for multiplication
                :return: (npos, ) array of the scalar field at the given `positions`
                """
                if inverse:
                    adiff = parameters.sum(0)
                    if self.order == 0:
                        return self.invert_a(adiff)
                    bdiff = np.tensordot(positions, parameters, axes=(0, 0))
                    return sp.append(self.invert_a(adiff), self.invert_b(bdiff))
                else:
                    a = self.get_a(parameters)
                    if self.order == 0:
                        return sp.stack([a] * positions.shape[0], 0)
                    b = self.get_b(parameters)
                    return a + sp.dot(positions, b)
            return matrix_mult
        elif method == Algorithm.cuda:
            from .. import cuda
            code = cuda.wrap_get_element(r"""
            __device__ void get_element({dtype} *result, int idx_pos, int idx_param{params_in}) {{
                for (int idx_dim = 0; idx_dim < {ndim}; idx_dim ++){{
                    result[idx_dim] = 0;
                }}
                if (idx_param < {ndim}){{
                    result[idx_param] = 1;
                }} else {{
                    int i = (idx_param - {ndim}) % {ndim};
                    int j = (idx_param - {ndim} - i) / {ndim};
                    result[i] = pos[idx_pos * {ndim} + j];
                    if (i == j){{
                        result[{ndim} - 1] = -pos[idx_pos * {ndim} + {ndim} - 1];
                    }}
                }}
            }}
            """)
            return cuda.CudaMatrixMult(code, positions.shape[0], positions.shape[1], self.nparams,
                                       params={'pos': (cuda.dtype + ' *pos', positions.flatten())}, scalar=False)
        super(Gradient, self).get_func(positions, method)

    def to_hdf5(self, group: h5py.Group):
        """
        Stores the radial basis function in the provided HDF5 group

        :param group: HDF5 group, where the basis function will be stored
        """
        group.attrs['name'] = self.__class__.__name__
        group.attrs['order'] = self.order
        group.attrs['ndim'] = self.ndim

    @classmethod
    def from_hdf5(cls, group: h5py.Group):
        """
        Reads the radial basis function from the provided HDF5 group

        :param group: HDF5 group, where the basis function is stored
        :return: newly read radial basis function
        """
        assert group.attrs['name'] == cls.__name__
        return cls(group.attrs['order'], group.attrs['ndim'])
