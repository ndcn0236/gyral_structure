"""
Defines a basis function based on compactly supported radial basis function.

Two options are provided to define the shape of the radial basis function:

- :func:`wendland`
- :func:`buhmann`

The function :func:`hcp_packing` can be used to define an hexagonal packing configuration for the RBF centroids.

A choice of centroids and RBF shape are needed to create the basis function :class:`RadialBasis`.

"""
import scipy as sp
import sympy
from scipy import sparse, spatial
from .core import BasisFunc, wrap_numba
from .evaluator import RequestEvaluator
from ..algorithm import Algorithm
import numpy as np
import itertools
from .. import request, cuda
import h5py
from warnings import warn


def hcp_packing(bounding_box, distance=1):
    """Proposes an hexagonal packing for the centroids of a radial basis function.

    :param bounding_box: ((min(x), max(x)), (max(y), max(y)), (max(z), max(z))) covering the full data-set
    :param distance: minimum distance between any two centroids
    :return: (npos, 3) array with the proposed centroids
    :rtype: sp.ndarray
    """
    radius = distance / 2
    i, j, k = sp.meshgrid(*(sp.arange((r[1] - r[0]) / (step * radius)) for r, step in
                            zip(bounding_box, [2, sp.sqrt(3), 2 * sp.sqrt(6) / 3])), indexing='ij')
    res = sp.stack([2 * i + ((j + k) % 2),
                    sp.sqrt(3) * (j + (k % 2) / 3),
                    2 * sp.sqrt(6) / 3 * k], -1).reshape(-1, 3) * radius
    res += sp.array(bounding_box).mean(1) - res.mean(0)
    return res


class RadialBasis(BasisFunc):
    """Basis function for compactly supported any radial basis functions

    The radial basis function used is the one provided by `mapping`.
    """

    potential = False
    """RBFs directly calculate the full vector field, not the vector field potential"""
    scalar = False
    """The mapping from parameters to vector field is not independent across dimensions"""
    compute = ('cuda', 'matrix_cuda', 'matrix', 'numba')
    """RBFs have been implemented for the GPU and CPU and both matrix and on-the-fly format"""

    def __init__(self, mapping, centroids, size=1.):
        """Creates a new set of basis functions.

        :param mapping: callable mapping the offsets from the centroids to fields (from :func:`buhmann` or :func:`wendland`)
        :type mapping: CompactRBFSympy
        :param centroids: (nparams, ndim) array of the centroids of the radial basis functions (e.g., from :func:`hcp_packing`)
        :param size: (nparams, ) array or scalar; total extent of the radial basis functions
        """
        self.centroids = sp.asarray(centroids)
        self.tree = spatial.cKDTree(self.centroids)
        self.size = sp.asarray(size)
        if self.size.ndim == 0:
            self.size = sp.zeros(self.centroids.shape[0]) + self.size
        if self.size.size != self.centroids.shape[0]:
            raise ValueError("No unique size for every centroid of the radial basis functions")
        self.mapping = mapping
        self._precomputed_grids = None

    def get_full_mat(self, req):
        """Returns the matrix used to __call__ the given request.

        :param req: Defines where the field should be evaluated
        :type req: request.FieldRequest
        :return: (npos * ndim, nparams) matrix mapping the parameters to the vector field
        """
        if not isinstance(req, request.FieldRequest):
            req = request.PositionRequest(req)
        idx_req, idx_centroids = self.within_range(req)
        all_centroids = self.centroids[idx_centroids]
        sz = self.size[idx_centroids][:, None]
        data = sp.zeros((idx_centroids.size, self.ndim, self.ndim))
        for pos, weight in req.split():
            norm_offset = (all_centroids - pos[idx_req]) / sz
            radius = sp.sqrt((norm_offset ** 2).sum(-1))
            use = radius <= 1
            effect_size = self.mapping(norm_offset[use], radius=radius[use])
            if sp.asarray(weight).ndim == 0:
                data[use] += weight * effect_size
            else:
                data[use] += weight[idx_req][use][:, None, None] * effect_size
        idx_req_full = idx_req[:, None, None] * self.ndim + sp.arange(self.ndim)[None, None, :]
        idx_centroids_full = idx_centroids[:, None, None] + sp.arange(self.ndim)[None, :, None] * self.centroids.shape[0]
        row, col, _ = sp.broadcast_arrays(idx_req_full, idx_centroids_full, data)
        use = data != 0
        return sparse.coo_matrix((data[use], (row[use], col[use])),
                                 shape=(req.npos * req.ndim, self.centroids.size))

    def get_partial_mat(self, positions, derivative=-1):
        """Computes the matrix used to map parameters to the vector field or potential at the given positions

        :param positions: (npos, 3) array with the positions of interest (shift and scaling have already been applied)
        :param derivative: which spatial derivative to compute (default: no derivative)
        :return: (npos, nparams) array mapping the parameters to the positions
        """
        return self.get_full_mat(request.PositionRequest(positions))

    def get_func(self, positions, method):
        """Creates a function that evaluates the requested field at the `positions`.

        :param positions: (npos, ndim) array defining where the result should be computed
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (field derivative to parameter derivative if inverse else param to field)
        :raises: NotImplementedError if the basis function is not defined for a specific method
        """
        if method == Algorithm.numba:
            sub_func = self.mapping.get_func(positions, self.centroids, self.size, method)
            idx_req, idx_centroid = self.within_range(request.PositionRequest(positions))
            idx_pos = sp.concatenate([idx_req] * self.ndim)
            idx_param = sp.concatenate([idx_centroid + dim * self.centroids.shape[0] for dim in range(3)])
            return wrap_numba(positions.shape[0], self.ndim, self.nparams, scalar=False,
                              sparse=(idx_pos, idx_param))(sub_func)
        super(RadialBasis, self).get_func(positions, method)

    @property
    def nparams(self, ):
        """Number of parameters expected"""
        return self.centroids.size

    @property
    def ndim(self, ):
        """Dimensionality of the space in which the RBF is embedded"""
        return self.centroids.shape[-1]

    def within_range(self, req, return_compressed=False):
        """
        Return for every centroid which elements of the request are within range.

        For speed during tractography intersections can be precomputed on a grid using `precompute_grid`

        :param req: describes where the basis function should be evaluated
        :type req: request.FieldRequest
        :param return_compressed: return the request indices in a compressed format
        :return: tuple with the request and centroid indices in compressed format
        """
        if self._precomputed_grids is not None and np.amax(req.radius()) <= self._precomputed_grids[0]:
            if not hasattr(self, '_ref_list_of_lists') or req.npos > self._ref_list_of_lists.size:
                self._ref_list_of_lists = np.zeros(req.npos, dtype='object')
                empty_arr = np.zeros(0, dtype='i4')
                for idx in range(req.npos):
                    self._ref_list_of_lists[idx] = empty_arr
            list_of_lists = self._ref_list_of_lists[:req.npos].copy()
            _, affine, intersects = self._precomputed_grids
            voxels = np.floor(affine[:3, :3].dot(req.center().T).T + affine[:-1, -1]).astype('i4')
            use = (voxels >= 0).all(-1) & (voxels < intersects.shape).all(-1)
            list_of_lists[use] = intersects[tuple(voxels[use].T)]
            list_of_lists = list(list_of_lists)
            idx_centroid = sp.concatenate(list_of_lists)
            assert idx_centroid.dtype == 'i4'
        else:
            tree2 = spatial.cKDTree(req.center())
            size1 = sp.asarray(self.size)
            max_size = size1.max() + req.radius().max()
            list_of_lists = tree2.query_ball_tree(self.tree, r=max_size)
            idx_centroid = sp.fromiter(itertools.chain(*list_of_lists), dtype='i4')
        if len(idx_centroid) == 0:
            warn("Only evaluating positions out of range of the dipole basis functions")
        idx_req_compressed = sp.append(0, sp.cumsum([len(l) for l in list_of_lists]))
        if return_compressed:
            return idx_req_compressed, idx_centroid
        idx_req = sp.digitize(sp.arange(idx_req_compressed[-1]), idx_req_compressed[1:])
        return idx_req, idx_centroid

    def bounding_box(self, extent=0):
        """
        Computes a bounding box around all the centroids where requests might be affected

        :param extent: float with the maximum size of the request
        :return: ((min(x), max(x)), (min(y), max(y)), (min(z), max(z))) covering the full dataset
        """
        lower = (self.centroids - self.size[:, None] - extent).min(0)
        upper = (self.centroids + self.size[:, None] + extent).max(0)
        return tuple(zip(lower, upper))

    def precompute_evaluator(self, extent=0, resolution=None):
        """
        Precomputes which dipoles intersect with a grid

        The result is stored internally and will be used when calling :py:meth:`within_range`.

        :param extent: maximum size of the request
        :param resolution: resolution of the grid on which the intersections will be precomputed
        """
        if resolution is None:
            resolution = (sp.asarray(self.size).max() + extent) / 4
        if self.ndim == 2:
            return
        max_size = extent + sp.asarray(self.size).max() + resolution * np.sqrt(3.) / 2.
        bb = self.bounding_box(max_size)
        affine = np.eye(4)
        affine[[0, 1, 2], [0, 1, 2]] = resolution
        affine[:-1, -1] = [lower for lower, _ in bb]
        centers = [np.arange(lower + resolution / 2, upper + resolution / 2, resolution) for lower, upper in bb]
        coords_center = np.array(np.meshgrid(*centers, indexing='ij'))
        center_tree = spatial.cKDTree(coords_center.reshape(3, -1).T)
        intersects = np.zeros(np.prod(coords_center.shape[1:]), dtype='object')
        for idx, neighbours in enumerate(center_tree.query_ball_tree(self.tree, r=max_size)):
            intersects[idx] = np.array(neighbours, dtype='i4')

        self._precomputed_grids = (
            extent, sp.linalg.inv(affine), intersects.reshape(coords_center.shape[1:])
        )

    def to_hdf5(self, group: h5py.Group):
        """
        Stores the radial basis function in the provided HDF5 group

        :param group: HDF5 group, where the basis function will be stored
        """
        group.attrs['name'] = self.__class__.__name__
        group['centroids'] = self.centroids
        group['size'] = self.size
        group.attrs['radial_func'] = str(self.mapping.raw_func)

    @classmethod
    def from_hdf5(cls, group: h5py.Group):
        """
        Reads the radial basis function from the provided HDF5 group

        :param group: HDF5 group, where the basis function is stored
        :return: newly read radial basis function
        """
        assert group.attrs['name'] == cls.__name__
        raw_func = sympy.sympify(group.attrs['radial_func'])
        return cls(CompactRBFSympy(raw_func), np.array(group['centroids']), np.array(group['size']))


def wendland(smoothness=2):
    """
    Compactly-supported radial basis functions from Wendland (1995)

    :param smoothness: Sets how smooth the radial basis function is:
        0: (1 - r)+^2
        1: (1 - r)+^4 (4 r + 1)
        2: (1 - r)+^6 (35 r^2 + 18 r + 3)
        3: (1 - r)+^8 (32 r^3 + 25 r^3 + 8r + 1)
    :return: object that evaluates the radial basis function
    :rtype: CompactRBFSympy
    """
    radius = sympy.var('r')
    if smoothness not in (0, 1, 2, 3):
        raise ValueError("Smoothness should be one of (0, 1, 2, 3)")
    additional = (1 if smoothness == 0 else
                  (4 * radius + 1 if smoothness == 1 else
                   (35 * radius ** 2 + 18 * radius + 3 if smoothness == 2 else
                    (32 * radius ** 3 + 25 * radius ** 2 + 8 * radius + 1))))
    return CompactRBFSympy((1 - radius) ** ((smoothness + 1) * 2) * additional)


def buhmann(n=3):
    """
    Compactly-supported radial basis functions from Buhmann (2001)

    :param n: Chooses from the RBFs explicitly stated by Buhmann:
        1: 1/18 - r^2 + 4/9 r^3 + 1/2 r^4 - 4/3 r^3 log(r) (twice differentiable)
        2: 2 r^4 log r - 7/2 r^4 + 16/3 r^3 - 2 r^2 + 1/6 (twice differentiable)
        3: 112 / 45 r^(9/2) + 16 / 3 r^(7/2) - 7 r^4 - 14 / 15 r^2 + 1/9 (thrice differentiable)
    :return: object that evaluates the radial basis function
    :rtype: CompactRBFSympy
    """
    radius = sympy.var('r')
    if n == 1:
        expr = 1/18 - radius ** 2 + 4/9 * radius ** 3 + 1/2 * radius ** 4 - 4/3 * radius ** 3 * sympy.log(radius)
    elif n == 2:
        expr = 2 * radius ** 4 * sympy.log(radius) - 7/2 * radius ** 4 + 16/3 * radius ** 3 - 2 * radius ** 2 + 1/6
    elif n == 3:
        expr = 112/45 * radius ** 4.5 + 16/3 * radius ** 3.5 - 7 * radius ** 4 - 14/15 * radius ** 2 + 1/9
    else:
        raise ValueError('n should be one of (1, 2, 3)')
    return CompactRBFSympy(expr)


class CompactRBFSympy(object):
    """Sympy representations of compactly supported radial basis function
    """
    def __init__(self, expr):
        """Creates a new RBF from the given sympy expression

        :param expr: sympy expression defining density as a function of 'r'
        """
        self.raw_func = expr
        self.rad_first_der_eq = sympy.diff(self.raw_func, 'r')
        self.rad_first_der = sympy.lambdify(('r', ), self.rad_first_der_eq)
        self.rad_second_der_eq = sympy.diff(self.rad_first_der_eq, 'r')
        self.rad_second_der = sympy.lambdify(('r', ), self.rad_second_der_eq)

    def __call__(self, offset, radius=None):
        """Evaluates the basis function

        :param offset: (N, 3) array with the offset between the centroids and the requested positions
        :param radius: (N, ) with the size of the offset. Will be computed if not provided
        :return: (N, ndim, ndim) array with the effect size of any given parameter on the field
        """
        if radius is None:
            radius = sp.sqrt((offset ** 2).sum(-1))
        ndim = offset.shape[-1]
        if ndim not in (2, 3):
            raise ValueError('matrix-valued RBF only defined in 2- or 3-dimensional space')

        df_dr = self.rad_first_der(radius)
        d2f_dr2 = self.rad_second_der(radius)

        dr_dx = offset / radius[:, None]
        comp_second_der = dr_dx[..., None] * dr_dx[..., None, :]
        comp_first_der = -comp_second_der / radius[..., None, None]
        comp_first_der[..., range(ndim), range(ndim)] += 1 / radius[..., None]
        full_derivative = (df_dr[..., None, None] * comp_first_der +
                           d2f_dr2[..., None, None] * comp_second_der)
        res = full_derivative.copy()
        if ndim == 3:
            for idx in range(3):
                idx1 = (idx + 1) % 3
                idx2 = (idx + 2) % 3
                res[:, idx, idx] = -(full_derivative[:, idx1, idx1] + full_derivative[:, idx2, idx2])
        else:
            res[:, 0, 0] = -full_derivative[:, 1, 1]
            res[:, 1, 1] = -full_derivative[:, 0, 0]
        res[radius == 0] = 0
        for idx in range(ndim):
            res[radius == 0, idx, idx] = (1 - ndim) * d2f_dr2[radius == 0]
        return res

    def get_func(self, positions, centroids, size, method):
        """Creates a function that evaluates the requested field at the `positions`.

        :param positions: (npos, ndim) array defining where the result should be computed
        :param centroids: (ncen, ndim) array defining where the radial basis functions are located
        :param size: (ncen, ) array with the extent of the radial basis functions
        :param method: which algorithm to use in the computation
        :return: function that maps (input, inverse, derivative) -> output (field derivative to parameter derivative if inverse else param to field)
        :raises: NotImplementedError if the basis function is not defined for a specific method
        """
        if method == Algorithm.numba:
            from numba import jit
            rad_first_der = jit(self.rad_first_der, nopython=True)
            rad_second_der = jit(self.rad_second_der, nopython=True)
            ndim = centroids.shape[1]

            @jit(nopython=True)
            def func(tmp_ndim, idx_pos, idx_param, derivate=-1):
                idx_centroid = idx_param % centroids.shape[0]
                offset = (positions[idx_pos, :] - centroids[idx_centroid, :]) / size[idx_centroid]
                rad = 0.
                for dim in range(ndim):
                    rad += offset[dim] ** 2
                if rad > 1:
                    for dim in range(ndim):
                        tmp_ndim[dim] = 0.
                    return
                rad = np.sqrt(rad)
                df_dr = rad_first_der(rad)
                d2f_dr2 = rad_second_der(rad)
                idx_component = idx_param // centroids.shape[0]
                tmp_ndim[idx_component] = 0
                for dim1 in range(ndim):
                    if dim1 != idx_component:
                        tmp_ndim[dim1] = (d2f_dr2 * offset[dim1] * offset[idx_component] / rad ** 2 -
                                          df_dr * offset[dim1] * offset[idx_component] / rad ** 3)
                        tmp_ndim[idx_component] -= (d2f_dr2 * offset[dim1] * offset[dim1] / rad ** 2 +
                                                    df_dr * (-offset[dim1] * offset[dim1] / rad ** 3 + 1 / rad))
            return func

    def __getstate__(self):
        return str(self.raw_func)

    def __setstate__(self, raw_func):
        self.__init__(sympy.sympify(raw_func))


class _RadialGPUArrays(object):
    def __init__(self):
        self.arrays = {}
        self.is_set = set()

    def register(self, component):
        if component not in self.arrays:
            self.arrays[component] = cuda.gpuarray.zeros(
                    component.nparams if isinstance(component, RadialBasis) else (component.npos, component.ndim),
                    dtype=cuda.dtype
            )

    def get(self, component):
        return self.arrays[component]

    def set(self, component, array):
        if component in self.is_set:
            return
        self.arrays[component].set(array)
        self.is_set.add(component)

    def clean(self, ):
        for arr in self.arrays.values():
            arr.fill(0.)
        self.is_set = set()

    def delete(self, component):
        if component in self.arrays:
            del self.arrays[component]

    def __contains__(self, item):
        return item in self.arrays


RadialGPUArrays = _RadialGPUArrays()
"""Singleton object with the Radial GPU arrays used in the evaluator"""


class RadialBasisCudaEvaluator(RequestEvaluator):
    """
    Specialised evaluator to compute the radial basis functions on the GPU
    """
    global_cuda_params = {}
    use_cuda = True
    use_mat = False
    _main_comp = lambda self, dim, dim2: """
    tmp = offset[%(dim2)i] * offset[%(dim2)i] * rsq;
    tomult = offset[%(dim)i] * offset[%(dim2)i] * rsq * (d2f_dr2 - df_dr * r);
    diag[%(dim)i] -= d2f_dr2 * tmp + df_dr * (1 - tmp) * r;
    output[%(dim)i] += weight * input[%(dim2)i] * tomult;
    output[%(dim2)i] += weight * input[%(dim)i] * tomult;
    tmp = offset[%(dim)i] * offset[%(dim)i] * rsq;
    diag[%(dim2)i] -= d2f_dr2 * tmp + df_dr * (1 - tmp) * r;
""" % locals()
    _code_comp = """
#include <math.h>
#include <stdio.h>

__device__ void main_evaluator({dtype}* output, {dtype}* __restrict__ input, {dtype}* __restrict__ offset, const {dtype} weight) {{
    {dtype} r, rsq, tmp;
    {dtype} diag[{ndim}];
    {dtype} tomult;
    rsq = 0.0;

    #pragma unroll
    for (int dim=0; dim < {ndim}; dim++) {{
        diag[dim] = 0.;
        rsq += offset[dim] * offset[dim];
    }}
    if (rsq > 1) return;
    r = sqrt(rsq);
    {dtype} df_dr = {df_dr};
    {dtype} d2f_dr2 = {d2f_dr2};
    r = 1 / r;
    rsq = 1 / rsq;
    %s

    #pragma unroll
    for (int dim=0; dim < {ndim}; dim++) {{
        output[dim] += weight * input[dim] * diag[dim];
    }}
}} 
""" 
    _main_code = """ 
__global__ void matrix_mult_nd({dtype}* __restrict__ field, {dtype}* __restrict__ parameters, int* __restrict__ idx_positions, int* __restrict__ idx_centroids, {dtype}* __restrict__ centroids, {dtype}* __restrict__ inv_size, {draw_in})
{{
    int thread_id = threadIdx.x + blockDim.x * blockIdx.x;
    int idx_pos = thread_id / 32;
    int lane = thread_id - (idx_pos * 32);
    {dtype} value[{ndim}];
    {dtype} centroid_pos[{ndim}];
    {dtype} pos[{ndim}];
    {dtype} params[{ndim}];
    {dtype} weight = 1.0 / {nsplit};
    {dtype} state[{nstate}];
    {dtype} is;
    if (idx_pos < {npos}){{
        int start = idx_positions[idx_pos];
        int end = idx_positions[idx_pos + 1];

        #pragma unroll
        for (int dim=0; dim < {ndim}; dim++) {{
            value[dim] = 0.0;
        }}
        set_state(state, idx_pos, {draw_out});
        for (int idx=start + lane; idx < end; idx+=32) {{
            int idx_centroid = idx_centroids[idx];
            is = inv_size[idx_centroid];
            for (int dim=0; dim < {ndim}; dim++) {{
                params[dim] = parameters[dim * {ncentroids} + idx_centroid];
                centroid_pos[dim] = centroids[idx_centroid * {ndim} + dim];
            }}
            for (int idx_split=0; idx_split < {nsplit}; idx_split ++) {{
                draw_pos(pos, &weight, state, idx_split, {draw_out});

                #pragma unroll
                for (int dim=0; dim < {ndim}; dim++) {{
                    pos[dim] -= centroid_pos[dim];
                    pos[dim] *= is;
                }}
                main_evaluator(value, params, pos, weight);
            }}
        }}

        #pragma unroll
        for (int dim=0; dim < {ndim}; dim++) {{
            value[dim] += __shfl_down(value[dim], 16);
            value[dim] += __shfl_down(value[dim], 8);
            value[dim] += __shfl_down(value[dim], 4);
            value[dim] += __shfl_down(value[dim], 2);
            value[dim] += __shfl_down(value[dim], 1);
            if (lane == 0) field[idx_pos * {ndim} + dim] += value[dim];
        }}
    }}
}}

__global__ void matrix_mult_nd_invert({dtype} *derparam, {dtype} *derfield, int *idx_centroids, int *idx_positions, {dtype} *centroids, {dtype} *inv_size, {draw_in})
{{
    int thread_id = threadIdx.x + blockDim.x * blockIdx.x;
    int idx_centroid = thread_id / 32;
    int lane = thread_id - (idx_centroid * 32);
    {dtype} value[{ndim}];
    {dtype} centroid_pos[{ndim}];
    {dtype} pos[{ndim}];
    {dtype} df[{ndim}];
    {dtype} weight = 1.0 / {nsplit};
    {dtype} state[{nstate}];
    {dtype} is;
    if (idx_centroid < {ncentroids}){{
        int start = idx_centroids[idx_centroid];
        int end = idx_centroids[idx_centroid + 1];
        for (int dim=0; dim < {ndim}; dim++) {{
            value[dim] = 0.0;
            centroid_pos[dim] = centroids[idx_centroid * {ndim} + dim];
        }}
        is = inv_size[idx_centroid];
        for (int idx=start + lane; idx < end; idx+=32) {{
            int idx_pos = idx_positions[idx];
            set_state(state, idx_pos, {draw_out});
            for (int dim=0; dim < {ndim}; dim++) {{
                df[dim] = derfield[idx_pos * {ndim} + dim];
            }}
            for (int idx_split=0; idx_split < {nsplit}; idx_split ++) {{
                draw_pos(pos, &weight, state, idx_split, {draw_out});
                for (int dim=0; dim < {ndim}; dim++) {{
                    pos[dim] -= centroid_pos[dim];
                    pos[dim] *= is;
                }}
                main_evaluator(value, df, pos, weight);
            }}
        }}
        for (int dim=0; dim < {ndim}; dim++) {{
            value[dim] += __shfl_down(value[dim], 16);
            value[dim] += __shfl_down(value[dim], 8);
            value[dim] += __shfl_down(value[dim], 4);
            value[dim] += __shfl_down(value[dim], 2);
            value[dim] += __shfl_down(value[dim], 1);
            if (lane == 0) derparam[dim * {ncentroids} + idx_centroid] += value[dim];
        }}
    }}
}}
"""

    def __init__(self, basis, request, method=None):
        """CUDA evaluator for radial basis functions.

        Maps parameters to vector field or the inverse

        :param basis: Radial basis function
        :type basis: RadialBasis
        :param request: Any single request
        :type request: request.FieldRequest
        :param method: overrides the Computational method used
        """
        if method != Algorithm.cuda:
            raise ValueError("RadialBasisCudaEvaluator can only evaluate using cuda")
        self.basis = basis
        self.request = request
        self.stream = cuda.drv.Stream()

        req_code, nstate, req_params = self.request.split_cuda()
        params = {'npos': request.npos,
                  'ndim': request.ndim,
                  'ncentroids': basis.centroids.shape[0],
                  'nsplit': request.total_split,
                  'dtype': cuda.dtype,
                  'nstate': nstate}
        draw_names = list(req_params.keys())
        params['draw_in'] = ', '.join([req_params[name][0] for name in draw_names])
        params['draw_out'] = ', '.join(draw_names)
        params['df_dr'] = sympy.ccode(basis.mapping.rad_first_der_eq)
        params['d2f_dr2'] = sympy.ccode(basis.mapping.rad_second_der_eq)

        code_comp = self._code_comp % (self._main_comp(0, 1) if self.ndim == 2 else
                           self._main_comp(0, 1) + self._main_comp(0, 2) + self._main_comp(1, 2))
        self.code = (code_comp + req_code + self._main_code).format(**params)
        req_params_cuda = tuple(cuda.to_gpu_correct(req_params[name][1].astype('f8')) for name in draw_names)
        self.req_params_cuda_names = draw_names
        self.global_cuda_params[self.request] = req_params_cuda
        basis_params_cuda = (cuda.to_gpu_correct(basis.centroids.flatten().astype('f8')),
                             cuda.to_gpu_correct(1. / basis.size))
        self.global_cuda_params[self.basis] = basis_params_cuda
        self.source_module = cuda.cached_source_module(self.code)
        self.forward = cuda.cached_func(self.code, 'matrix_mult_nd')
        self.backward = cuda.cached_func(self.code, 'matrix_mult_nd_invert')

        self.forward_idx = ()
        self.backward_idx = ()

        self.update_indices()

        RadialGPUArrays.register(self.basis)
        RadialGPUArrays.register(self.request)

    def update_indices(self, request=None, only_forward=False):
        del self.forward_idx
        if request is None:
            request = self.request
        if only_forward:
            forward_compressed, forward_idx = self.basis.within_range(request, return_compressed=True)
        else:
            idx_req, idx_centroids = self.basis.within_range(request)
            forward_compressed = sp.append(0, sp.cumsum(sp.bincount(idx_req, minlength=request.npos)))
            forward_idx = idx_centroids  # [sp.argsort(idx_req)]; idx_req is always sorted
        if forward_compressed.size < self.request.npos + 1:
            forward_compressed = np.append(forward_compressed, np.full(self.request.npos + 1 - forward_compressed.size, forward_compressed[-1], dtype=forward_compressed.dtype))
        self.forward_idx = (cuda.to_gpu_correct(forward_compressed),
                            cuda.to_gpu_correct(forward_idx))
        if only_forward:
            return

        del self.backward_idx
        backward_compressed = sp.append(0, sp.cumsum(sp.bincount(idx_centroids, minlength=request.npos)))
        backward_idx = idx_req[sp.argsort(idx_centroids)]
        self.backward_idx = (cuda.to_gpu_correct(backward_compressed),
                             cuda.to_gpu_correct(backward_idx))

    def evaluate_results(self, params, inverse=False):
        """Runs the basis function for all positions in the request without accessing the results

        The results of this can be obtained from combined_results after running evaluate_results

        :param params: (npos, dim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        if inverse:
            RadialGPUArrays.set(self.request, params)
            args = ((RadialGPUArrays.get(self.basis), RadialGPUArrays.get(self.request)) +
                    self.backward_idx +
                    self.global_cuda_params[self.basis] +
                    self.global_cuda_params[self.request])
            self.backward(*args, block=(cuda.nthreads, 1, 1),
                          grid=(int(sp.ceil(32 * self.basis.centroids.shape[0] / cuda.nthreads)), 1, 1))
        else:
            RadialGPUArrays.set(self.basis, params)
            args = ((RadialGPUArrays.get(self.request), RadialGPUArrays.get(self.basis)) +
                    self.forward_idx +
                    self.global_cuda_params[self.basis] +
                    self.global_cuda_params[self.request])
            self.forward(*args, block=(cuda.nthreads, 1, 1),
                         grid=(int(sp.ceil(32 * self.request.npos / cuda.nthreads)), 1, 1))

    def combine_results(self, inverse=False):
        """Retrieves the vector field at the requested positions as computed by evaluate_results

        evaluate_results should be called with the same parameters before running this command

        :param params: (npos, dim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        return None

    def clean_results(self, ):
        RadialGPUArrays.clean()

    def update_pos(self, new_request):
        new_arr = RadialGPUArrays.get(self.request)
        if hasattr(self, 'hidden_request'):
            del RadialGPUArrays.arrays[self.hidden_request]
        if new_request.npos < self.request.npos:
            new_arr = new_arr[:new_request.npos]
        RadialGPUArrays.arrays[new_request] = new_arr
        self.hidden_request = new_request
           
        idx = self.req_params_cuda_names.index('all_pos')
        self.global_cuda_params[self.request][idx][:new_request.positions.size] = new_request.positions.astype(cuda.dtype).flatten()
        self.update_indices(new_request, only_forward=True)

