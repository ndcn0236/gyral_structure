"""
Defines a set of basis functions that map from input parameters to a vector field at the requested positions

The locations where the field will be evaluated will be defined by the requests (see :py:mod:`request <.request>`).

Most of these basis functions produce divergence-free vector fields (i.e., no terminating streamlines),
although some (e.g. :py:class:`ChargeDistribution <unbounded.ChargeDistribution>`) are explicitly designed to
model the termination of streamlines in selected gray matter regions.

Multiple basis functions can be added together using :py:class:`SumBase <core.SumBase>`.

New basis functions should be derived from :py:class:`BasisFunc <core.BasisFunc>`.

Concrete implementations of basis functions (in order of decreasing usefulness) are:

- :class:`RadialBasis <radial.RadialBasis>`: set of matrix-valued, radial basis function with limited extent
- :class:`ChargeDistribution <unbounded.ChargeDistribution>`: vector field generated for charges generating streamline endpoints in the gray matter
- :class:`Gradient <unbounded.Gradient>`: A smooth distribution of the vector field potential
- :class:`Fourier <unbounded.Fourier>`: Periodic vector field potential
- :class:`Polynomial <unbounded.Polynomial>`: vector field potential given by polynomial

Excellent results can usually be obtained by using :class:`ChargeDistribution <unbounded.ChargeDistribution>`
to generate a reasonable initialisation of the field, which can be further updated using one or more
:class:`RadialBasis <radial.RadialBasis>`.
"""
from .core import BasisFunc, SumBase, read
from .radial import RadialBasis, wendland, buhmann, hcp_packing
from .trilinear import TriLinear
from .tricubic import Tricubic
from .unbounded import Fourier, ChargeDistribution, Polynomial, Gradient
