"""
This module defines the mapping from parameters to vector field given a basis function (from :mod:`basis <gyral_structure.basis>`)
and a request defining where the field should be evaluated (from :mod:`request <gyral_structure.request>`).

For a basis function and request a new evaluator can be obtained using :func:`get_single_evaluator`. The result
will be a subclass of :class:`RequestEvaluator`.
"""
from . import core
from .. import request, cuda, algorithm
from ..algorithm import Algorithm
import scipy as sp
from scipy import sparse


def get_single_evaluator(basis, req):
    """Main function to call to get an evaluator of a req

    The appropriate evaluator is chosen based on the first match in the following rules:

    - ParamRequest -> :class:`IdentityEvaluator`
    - Multiple requests or basis functions -> :class:`MultEvaluator`

    After this we iterate through the available methods to find a valid algorithm (in order we check the methods
    provided when calling get_evaluator, the methods provided to the req, the methods provided to the basis
    function, the possible algorithms for the basis function):

    - first valid algorithm is matrix or matrix_cuda -> :class:`MatRequestEvaluator`
    - otherwise -> :class:`FuncRequestEvaluator`

    :param basis: basis function mapping parameters to field
    :type basis: core.BasisFunc
    :param req: defines the positions/voxels/vertices where the field should be evaluated
    :type req: req.FieldRequest
    :return: Callable object that converts parameters into the field at the requested position (or the opposite for
        field derivatives)
    """
    if req is request.ParamRequest:  # req does not care about the basis function
        return IdentityEvaluator(basis, req)

    method = list(algorithm.full_chain(basis.compute))[0]
    if method in (Algorithm.matrix_cuda, Algorithm.cuda):
        import pycuda.autoinit
    if method in (Algorithm.matrix_cuda, Algorithm.matrix):
        return MatRequestEvaluator(basis, req, method)
    from . import radial
    if method == Algorithm.cuda and isinstance(basis, radial.RadialBasis):
        return radial.RadialBasisCudaEvaluator(basis, req, method)
    from . import grid
    if isinstance(basis, grid.Grid):
        return grid.GridFuncRequestEvaluator(basis, req, method)
    return FuncRequestEvaluator(basis, req, method)


class RequestEvaluator(object):
    """
    Evaluates the basis function at the requested positions

    The user interaction with this object will usually consist of calling it:

    - evaluator(params) maps from the parameters to the vector field
    - evaluator(field_derivatives, inverse=True) maps from the field derivatives to the parameter derivatives

    For efficiency this will call under the hood:

    - :meth:`evaluate_results`: initializes all the calculations
    - :meth:`combine_results`: combines the results of the calculations
    - :meth:`clean_results`: cleans the previous results
    """

    def __init__(self, basis, req, method):
        """Creates a new basis function to :meth:`__call__` the request

        :param basis: basis function mapping parameters to field
        :type basis: core.BasisFunc
        :param req: defines the positions/voxels/vertices where the field should be evaluated
        :type req: request.FieldRequest
        :param method: defines the algorithm used to __call__ the basis function
        :type method: Algorithm
        """
        if req.ndim != basis.ndim:
            raise ValueError("Request and basis function expect a different dimensionality")
        self.basis = basis
        self.request = req
        self.method = method

    def __call__(self, params, inverse=False):
        """
        Converts a set of parameters to the vector field at the requested positions (or inverse of that)

        :param params: (npos, ndim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        :return: (nparams, ) array if inverse else (npos, ndim) array
        """
        params = sp.asanyarray(params)
        if inverse and params.shape != (self.npos, self.ndim):
            raise ValueError("Expected field derivatives of shape %s, but got %s" %
                             ((self.npos, self.ndim), params.shape))
        if not inverse and params.size != self.nparams:
            raise ValueError("Expected %i parameters, but got %i" % (self.nparams, params.size))
        self.evaluate_results(params, inverse=inverse)
        res = self.combine_results(inverse=inverse)
        self.clean_results()
        return res

    def evaluate_results(self, params, inverse=False):
        """Runs the basis function for all positions in the request without accessing the results

        The results of this can be obtained from combined_results after running evaluate_results

        :param params: (npos, dim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        raise NotImplementedError()

    def combine_results(self, inverse=False):
        """Retrieves the vector field at the requested positions as computed by evaluate_results

        evaluate_results should be called with the same parameters before running this command

        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        raise NotImplementedError()

    def clean_results(self):
        """Cleans previously evaluated results

        Can be used to free the memory used up by the previous results
        """
        pass

    @property
    def scalar(self, ):
        """Maps to :attr:`self.basis.scalar <gyral_structure.basis.core.BasisFunc.scalar>`"""
        return self.basis.scalar

    @property
    def potential(self, ):
        """Maps to :attr:`self.basis.potential <gyral_structure.basis.core.BasisFunc.potential>`"""
        return self.basis.potential

    @property
    def ndim(self, ):
        """Maps to :attr:`self.basis.ndim <gyral_structure.basis.core.BasisFunc.ndim>`"""
        return self.basis.ndim

    @property
    def npos(self):
        """Maps to :attr:`self.request.npos <gyral_structure.request.FieldRequest.npos>`"""
        return self.request.npos

    @property
    def nparams(self):
        """Maps to :attr:`self.basis.nparams <gyral_structure.basis.core.BasisFunc.nparams>`"""
        return self.basis.nparams

    @property
    def use_mat(self):
        """If True pre-generate a matrix rather than evaluating it on the fly"""
        return self.method in (Algorithm.matrix, Algorithm.matrix_cuda)

    @property
    def use_cuda(self):
        """If True evaluates the field on the GPU rather than CPU"""
        return self.method in (Algorithm.cuda, Algorithm.matrix_cuda)

    def update_pos(self, new_request):
        raise ValueError(f"Updating positions not implemented for {type(self)}")


class IdentityEvaluator(RequestEvaluator):
    """Returns the parameters irrespective of the basis function
    """
    use_cuda = False

    def __init__(self, basis, req, method=None):
        """Creates a new basis function to :meth:`__call__` the request

        :param basis: basis function mapping parameters to field
        :type basis: core.BasisFunc
        :param req: defines the positions/voxels/vertices where the field should be evaluated
        :type req: request.FieldRequest
        :param method: defines the algorithm used to __call__ the basis function
        :type method: Algorithm
        """
        self.basis = basis
        self.request = req

    def evaluate_results(self, params, inverse=False):
        self._params = params

    def combine_results(self, inverse=False):
        return self._params

    def clean_results(self):
        del self._params

    def __call__(self, params, inverse=False):
        """Returns the input parameters
        """
        return params

    def update_pos(self, new_request):
        self.request = new_request


class FuncRequestEvaluator(RequestEvaluator):
    """Evaluates the basis function using on-the-fly evaluation of the matrix on the CPU or GPU

    The user interaction with this object will usually consist of calling it:

    - evaluator(params) maps from the parameters to the vector field
    - evaluator(field_derivatives, inverse=True) maps from the field derivatives to the parameter derivatives

    For efficiency this will call under the hood:

    - :meth:`evaluate_results`: initializes all the calculations
    - :meth:`combine_results`: combines the results of the calculations
    - :meth:`clean_results`: cleans the previous results
    """

    def __init__(self, basis, req, method):
        """Creates a new basis function to :meth:`__call__` the request

        :param basis: basis function mapping parameters to field
        :type basis: core.BasisFunc
        :param req: defines the positions/voxels/vertices where the field should be evaluated
        :type req: request.FieldRequest
        :param method: defines the algorithm used to __call__ the basis function
        :type method: Algorithm
        """
        super(FuncRequestEvaluator, self).__init__(basis, req, method)
        self.partial_func = [(w, self.basis.get_func(pos, self.method)) for pos, w in self.request.split()]
        self.results = {}

    def func(self, params, inverse=False, derivative=-1):
        """Calls the basis function for all positions in the request
        """
        return [(w, f(params, inverse=inverse, derivative=derivative)) for w, f in self.partial_func]

    def evaluate_results(self, params, inverse=False):
        """Runs the basis function for all positions in the request without accessing the results

        The results of this can be obtained from combined_results after running evaluate_results

        :param params: (npos, dim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        if self.potential:
            if self.ndim == 3:
                for dim in range(3):
                    for der in range(3):
                        if dim != der:
                            self.results[dim, der] = self.func(params.reshape((params.size, ))[dim::self.ndim],
                                                                                 inverse=inverse, derivative=der)
            else:
                if inverse:
                    self.results[1] = self.func(params[:, 0], inverse=True, derivative=1)
                    self.results[0] = self.func(params[:, 1], inverse=True, derivative=0)
                else:
                    self.results[1] = self.func(params, inverse=False, derivative=1)
                    self.results[0] = self.func(params, inverse=False, derivative=0)
        else:
            if self.scalar:
                for dim in range(self.ndim):
                    self.results[dim] = self.func(params.flatten()[dim::self.ndim], inverse=inverse)
            else:
                self.results[()] = self.func(params, inverse=inverse)

    def combine_results(self, inverse=False):
        """Retrieves the vector field at the requested positions as computed by evaluate_results

        evaluate_results should be called with the same parameters before running this command

        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        results = {}
        for key, res in self.results.items():
            arr = res[0][0] * res[0][1]
            for weight, part_res in res[1:]:
                arr += weight * part_res
            if not inverse and arr.shape[0] > self.request.npos:
                arr = arr[:self.request.npos, :]
            results[key] = arr

        if self.potential:
            if self.ndim == 3:
                if self.scalar:
                    arr = (results[2, 1] - results[1, 2],
                           results[0, 2] - results[2, 0],
                           results[1, 0] - results[0, 1])
                    if self.use_cuda:
                        arr = [a.get() for a in arr]
                    if inverse:
                        return -sp.stack(arr, -1).flatten()
                    else:
                        return sp.stack(arr, -1)
                else:
                    if self.use_cuda:
                        results = [res.get() for res in results]
                    def func(i1, i2, i3):
                        return results[i1, i2].flatten()[i3::self.ndim]

                    arr = (func(2, 1, 0) - func(1, 2, 0),
                           func(0, 2, 1) - func(2, 0, 1),
                           func(1, 0, 2) - func(0, 1, 2))
                    if inverse:
                        return -sp.stack(arr, -1).flatten()
                    else:
                        return sp.stack(arr, -1)
            else:
                if inverse:
                    return -results[1] + results[0]
                else:
                    if self.use_cuda:
                        results = (results[0].get(), results[1].get())
                    return sp.stack([-results[1], results[0]], -1)

        else:
            if self.scalar:
                arr = sp.stack([results[dim] for dim in range(self.ndim)])
                return arr.reshape((self.npos, self.ndim)) if inverse else arr
            else:
                return results[()]

    def clean_results(self, ):
        """Cleans previously evaluated results

        Can be used to free the memory used up by the previous results
        """
        self.results = {}

    def update_pos(self, new_request):
        self.request = new_request
        for _, partial_func in self.partial_func:
            if not hasattr(partial_func, 'update_pos'):
                self.partial_func = [(1, self.basis.get_func(new_request.positions, self.method))]
                break
            partial_func.update_pos(new_request.positions)


class MatRequestEvaluator(RequestEvaluator):
    """Evaluates the basis function with a pre-defined matrix on CPU or GPU

    The user interaction with this object will usually consist of calling it:

    - evaluator(params) maps from the parameters to the vector field
    - evaluator(field_derivatives, inverse=True) maps from the field derivatives to the parameter derivatives

    For efficiency this will call under the hood:

    - :meth:`evaluate_results`: initializes all the calculations
    - :meth:`combine_results`: combines the results of the calculations
    - :meth:`clean_results`: cleans the previous results
    """

    def __init__(self, basis, req, method):
        """Creates a new basis function to :meth:`__call__` the request

        :param basis: basis function mapping parameters to field
        :type basis: core.BasisFunc
        :param req: defines the positions/voxels/vertices where the field should be evaluated
        :type req: request.FieldRequest
        :param method: defines the algorithm used to __call__ the basis function
        :type method: Algorithm
        """
        super(MatRequestEvaluator, self).__init__(basis, req, method)
        self.mat = basis.get_full_mat(req)

        if self.use_cuda:
            if sparse.issparse(self.mat):
                self.forward_f = cuda.sparse_mult_prepare(self.mat)
                self.inverse_f = cuda.sparse_mult_prepare(self.mat.T)
            else:
                self.forward_f = cuda.dense_mult_prepare(self.mat)
                self.inverse_f = cuda.dense_mult_prepare(self.mat.T)

    def evaluate_results(self, params, inverse=False):
        """Runs the matrix multiplication and temporarily stores the result

        The results of this can be obtained from combined_results after running evaluate_results

        :param params: (npos, dim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        if not self.use_cuda:
            if inverse:
                result = self.mat.T.dot(params.flatten())
            else:
                result = self.mat.dot(params)
        else:
            if inverse:
                result = self.inverse_f(params)
            else:
                result = self.forward_f(params)
        self.result = result

    def combine_results(self, inverse=False):
        """Retrieves the vector field at the requested positions as computed by evaluate_results

        evaluate_results should be called with the same parameters before running this command

        :param params: (npos, dim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        """
        if self.use_cuda:
            from pycuda.autoinit import context
            context.synchronize()
        if not inverse:
            return self.result.reshape((self.npos, self.ndim))
        return self.result

    def clean_results(self, ):
        """Cleans previously evaluated results

        Can be used to free the memory used up by the previous results
        """
        del self.result

    def wrap_qp(self, qp):
        return self.request.wrap_qp(qp, {self.request: self.mat})

    def update_pos(self, new_request):
        self.request = new_request
        self.mat = self.basis.get_full_mat(self.request)

        if self.use_cuda:
            if sparse.issparse(self.mat):
                self.forward_f = cuda.sparse_mult_prepare(self.mat)
                self.inverse_f = cuda.sparse_mult_prepare(self.mat.T)
            else:
                self.forward_f = cuda.dense_mult_prepare(self.mat)
                self.inverse_f = cuda.dense_mult_prepare(self.mat.T)


class MultEvaluator(object):
    """
    Evaluates multiple basis functions/requests

    The user interaction with this object will usually consist of calling it:

    - evaluator(params) maps from the parameters to the vector field
    - evaluator(field_derivatives, inverse=True) maps from the field derivatives to the parameter derivatives

    For efficiency this will call under the hood:

    - :meth:`evaluate_results`: initializes all the calculations
    - :meth:`combine_results`: combines the results of the calculations
    - :meth:`clean_results`: cleans the previous results
    """
    fixed_field = None
    """Total field from all the basis functions, whose parameters have been fixed"""

    def __init__(self, sum_basis, full_request):
        """
        Creates a new basis function/request combination

        :param sum_basis: basis function (optionally sum of multiple)
        :param full_request: request (optionally combination of multiple)
        """
        if not isinstance(full_request, request.FieldRequest):
            full_request = request.PositionRequest(full_request)
        self.sum_basis = sum_basis
        self.full_request = full_request
        self.gpu_input = {}
        self.basis_list, self.nparams, fixed_field = self.loop_basis(self.sum_basis)
        assert self.nparams == self.sum_basis.nparams
        self.request_list = list(set(self.full_request.flatten()))

        self.fixed_field_evaluators = {req: [(bf.get_evaluator(req), params) for bf, params in fixed_field]
                                       for req in self.request_list}
        if len(fixed_field) != 0:
            self.fixed_field = {
                req: sp.sum([evaluator(params) for evaluator, params in self.fixed_field_evaluators[req]], 0)
                for req in self.request_list
            }

        self.evaluators = sp.zeros((len(self.basis_list), len(self.request_list)), dtype='object')
        for idxb, (basis, _) in enumerate(self.basis_list):
            for idxr, req in enumerate(self.request_list):
                self.evaluators[idxb, idxr] = get_single_evaluator(basis, req)

    def __call__(self, params, inverse=False):
        """
        Converts a set of parameters to the vector field at the requested positions (or inverse of that)

        :param params: (npos, ndim) array if inverse else (nparams, ) array
        :param inverse: set to true to convert a vector field derivative into parameter derivatives
        :return: (nparams, ) array if inverse else (npos, ndim) array
        """
        params = sp.asanyarray(params)
        if not inverse and params.size != self.nparams:
            raise ValueError("Expected %i parameters, but got %i" % (self.nparams, params.size))
        self.evaluate_results(params, inverse=inverse)
        res = self.combine_results(inverse=inverse)
        self.clean_results()
        return res

    @staticmethod
    def loop_basis(basis_func, current=0):
        """
        Loops over all the basis functions to find those that need to be evaluated and the fixed field

        :param basis_func: SumBasis collection of basis function or any individual basis function
        :param current: keep this at zero to get an accurate number of expected parameters
        :return: Tuple with:

            - list of (basis function, slice of parameters that should enter basis function)
            - integer with total number of expected parameters
            - (npos, ndim) array with the field for the fixed basis functions
        """
        if isinstance(basis_func, core.SumBase):
            fixed_field = []
            res = []
            for idx, sub_basis in enumerate(basis_func):
                if idx not in basis_func._fixed:
                    pr, current, sub_fixed_field = MultEvaluator.loop_basis(sub_basis, current)
                    fixed_field.extend(sub_fixed_field)
                    res.extend(pr)
                else:
                    fixed_field.append((sub_basis, sp.asanyarray(basis_func._fixed[idx])))
            return res, current, fixed_field
        else:
            res = [(basis_func, slice(current, current + basis_func.nparams))]
        return res, current + basis_func.nparams, []

    def get_evaluator(self, basis, request):
        return self.evaluators[self.basis_list.index(basis),
                               self.request_list.index(request)]

    def evaluate_results(self, params, inverse=False):
        """
        Call evaluate_results on all evaluators with the correct parameters
        """
        self._params = params
        if inverse:
            der_field = self.full_request.field_to_dict(params)
            der_field_gpu = {}
            for req, evaluators in zip(self.request_list, self.evaluators.T):
                if any(eval.use_cuda for eval in evaluators):
                    der_field_gpu[req] = cuda.to_gpu_correct(der_field[req])
            if self.use_cuda:
                from pycuda.autoinit import context
                context.synchronize()
            for _, evaluators in zip(self.basis_list, self.evaluators):
                for req, eval in zip(self.request_list, evaluators):
                    eval.evaluate_results(
                            der_field_gpu[req] if eval.use_cuda else der_field[req], 
                            inverse=True)
        else:
            params_gpu = {}
            for (bf, slc), evaluators in zip(self.basis_list, self.evaluators):
                if any(eval.use_cuda for eval in evaluators):
                    params_gpu[bf] = cuda.to_gpu_correct(params[slc])
            if self.use_cuda:
                from pycuda.autoinit import context
                context.synchronize()

            from .radial import RadialGPUArrays
            for req, evaluators in zip(self.request_list, self.evaluators.T):
                for (bf, slc), eval in zip(self.basis_list, evaluators):
                    eval.evaluate_results(
                            params_gpu[bf] if eval.use_cuda else params[slc], 
                            inverse=False)

    def combine_results(self, inverse=True):
        from .radial import RadialGPUArrays
        if self.use_cuda:
            from pycuda.autoinit import context
            context.synchronize()
        if inverse:
            der_params = sp.zeros(self.nparams)
            for (bf, slc), evaluators in zip(self.basis_list, self.evaluators):
                der_params_gpu = None
                for _, eval in zip(self.request_list, evaluators):
                    res_eval = eval.combine_results(inverse=True)
                    if res_eval is None:
                        continue
                    if cuda.available and isinstance(res_eval, cuda.gpuarray.GPUArray):
                        if der_params_gpu is None:
                            der_params_gpu = res_eval
                        else:
                            der_params_gpu += res_eval
                    else:
                        der_params[slc] += res_eval
                if der_params_gpu is not None:
                    der_params[slc] += der_params_gpu.get()
                if bf in RadialGPUArrays:
                    der_params[slc] += RadialGPUArrays.get(bf).get()
            return der_params
        else:
            field = {}
            for req, evaluators in zip(self.request_list, self.evaluators.T):
                field_gpu = None
                if req == request.ParamRequest:
                    field[req] = self._params
                else:
                    field[req] = sp.zeros((req.npos, req.ndim))
                    for eval in evaluators:
                        res_eval = eval.combine_results(inverse=False)
                        if res_eval is None:
                            continue
                        if cuda.available and isinstance(res_eval, cuda.gpuarray.GPUArray):
                            if field_gpu is None:
                                field_gpu = res_eval
                            else:
                                field_gpu += res_eval
                        else:
                            field[req] += res_eval
                    if self.fixed_field is not None and req in self.fixed_field:
                        field[req] += self.fixed_field[req]
                    if field_gpu is not None:
                        field[req] += field_gpu.get()
                    if req in RadialGPUArrays:
                        field[req] += RadialGPUArrays.get(req).get()
            return self.full_request.dict_to_field(field)

    def clean_results(self, ):
        for eval in self.evaluators.flatten():
            eval.clean_results()

    def mat(self):
        if len(self.basis_list) > 1:
            raise NotImplementedError("matrix concatenation for multiple basis functions not implemented")
        return {req: evaluator[0].mat.tocsr() if sparse.issparse(evaluator[0].mat) else evaluator[0].mat
                for req, evaluator in zip(self.request_list, self.evaluators.T)}

    def wrap_qp(self, qp):
        mat = self.mat()
        return self.full_request.wrap_qp(qp, mat)

    @property
    def use_cuda(self, ):
        return any(eval.use_cuda for eval in self.evaluators.flatten())

    @property
    def use_mat(self, ):
        return any(eval.use_mat for eval in self.evaluators.flatten())

    def update_pos(self, new_positions):
        if len(self.request_list) > 1:
            raise ValueError("Can't update the positions of multiple requests")
        new_request = request.PositionRequest(new_positions)
        for evaluator in self.evaluators.flat:
            evaluator.update_pos(new_request)
        for evaluator in self.fixed_field_evaluators[self.request_list[0]]:
            evaluator.update_pos(new_request)
        self.fixed_field_evaluators[new_request] = self.fixed_field_evaluators[self.request_list[0]]
        del self.fixed_field_evaluators[self.request_list[0]]
        self.request_list[0] = new_request
        self.full_request = new_request
        if self.fixed_field:
            self.fixed_field = {
                req: sp.sum([evaluator(params) for evaluator, params in self.fixed_field_evaluators[req]], 0)
                for req in self.request_list
            }
