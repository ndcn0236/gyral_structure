Scripts included in gyral_structure
===================================

.. toctree::

An example of combining these scripts to model the fibre configuration in the gyral white matter can be found
:ref:`here <cli-interface>`.

.. _gs_mask:

gs_mask: Creates a mask of the gyral white matter
-------------------------------------------------
.. literalinclude:: ../gyral_structure/scripts/gs_mask
    :language: python

.. _gs_fit:

gs_fit: Fits vector field inside gyral white matter
---------------------------------------------------
.. literalinclude:: ../gyral_structure/scripts/gs_fit
    :language: python

.. _gs_deform_surface:

gs_deform_surface: Moves white/gray matter boundary to deep white matter
------------------------------------------------------------------------
.. literalinclude:: ../gyral_structure/scripts/gs_deform_surface
    :language: python

.. _gs_dyad_vol:

gs_dyad_vol: Evaluates vector field fibre density and orientation in a volume
-----------------------------------------------------------------------------
.. literalinclude:: ../gyral_structure/scripts/gs_dyad_vol
    :language: python

.. _gs_dyad_surf:

gs_dyad_surf: Evaluates vector field fibre density and orientation on a surface
-------------------------------------------------------------------------------
.. literalinclude:: ../gyral_structure/scripts/gs_dyad_surf
    :language: python

.. _gs_track:

gs_track: Streamline tractography through vector field
------------------------------------------------------
.. literalinclude:: ../gyral_structure/scripts/gs_track
    :language: python
