gyral\_structure\.basis: defining the basis functions
=====================================================

.. automodule:: gyral_structure.basis
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.basis\.core module
------------------------------------

.. automodule:: gyral_structure.basis.core
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.basis\.evaluator module
-----------------------------------------

.. automodule:: gyral_structure.basis.evaluator
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.basis\.grid module
------------------------------------

.. automodule:: gyral_structure.basis.grid
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.basis\.radial module
--------------------------------------

.. automodule:: gyral_structure.basis.radial
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.basis\.tricubic module
----------------------------------------

.. automodule:: gyral_structure.basis.tricubic
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.basis\.trilinear module
-----------------------------------------

.. automodule:: gyral_structure.basis.trilinear
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.basis\.unbounded module
-----------------------------------------

.. automodule:: gyral_structure.basis.unbounded
    :members:
    :undoc-members:
    :show-inheritance:


