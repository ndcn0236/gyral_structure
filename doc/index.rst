Welcome to documentation for gyral_structure!
=============================================

.. automodule:: gyral_structure



Documentation overview
======================
.. toctree::
   :maxdepth: 3

   tutorial
   gyral_structure

