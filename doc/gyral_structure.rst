gyral\_structure API
====================

.. automodule:: gyral_structure
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::

    gyral_structure.basis
    gyral_structure.cost
    gyral_structure.scripts

gyral\_structure\.algorithm: how to compute the cost function
-------------------------------------------------------------

.. automodule:: gyral_structure.algorithm
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.cuda: GPU support
------------------------------------

.. automodule:: gyral_structure.cuda
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.request: define which voxels/vertices to evaluate
-------------------------------------------------------------------

.. automodule:: gyral_structure.request
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.streamline: streamline tractography through vector field
--------------------------------------------------------------------------

.. automodule:: gyral_structure.streamline
    :members:
    :undoc-members:
    :show-inheritance:

gyral\_structure\.utils: several utilities
------------------------------------------

.. automodule:: gyral_structure.utils
    :members:
    :undoc-members:
    :show-inheritance:


