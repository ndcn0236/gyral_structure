#!/usr/bin/env python

from setuptools import setup, find_packages, Command
import os.path as op
import shutil


basedir = op.dirname(__file__)


class doc(Command):
    """
    Build the API documentation.
    """

    user_options = []

    def initialize_options(self):
          pass

    def finalize_options(self):
          pass

    def run(self):
        import sphinx.cmd.build as sphinx_build

        docdir  = op.join(basedir, 'doc')
        destdir = op.join(docdir, 'html')

        if op.exists(destdir):
              shutil.rmtree(destdir)

        print('Building documentation [{}]'.format(destdir))

        sphinx_build.main([docdir, destdir])


setup(name='gyral_structure',
      version='0.2',
      description='Model of a continuous fiber orientation distribution function',
      author='Michiel Cottaar',
      author_email='Michiel.Cottaar@ndcn.ox.ac.uk',
      url='https://git.fmrib.ox.ac.uk/ndcn0236/gyral_structure',
      packages=find_packages(),
      cmdclass={'doc': doc},
      install_requires=['nibabel', 'scipy', 'mcot.surface', 'sympy', 'h5py', 'numexpr'],
      scripts=[
          'gyral_structure/scripts/gs_track',
          'gyral_structure/scripts/gs_dyad_vol',
          'gyral_structure/scripts/gs_dyad_surf',
          'gyral_structure/scripts/gs_deform_surface',
          'gyral_structure/scripts/gs_mask',
          'gyral_structure/scripts/gs_fit',
          'gyral_structure/scripts/gs_qc',
      ]
      )
