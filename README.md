Describes white matter structure as a continuous vector field. 
This vector field defines at every point the local fibre orientation and density.

This model is particularly useful in describing the gyral white matter,
where it allows both for constraints on the fibre orientation (i.e., match the observed fibre orientation from diffusion MRI)
and for constraints on the fibre density (i.e., uniform across the surface).


Installation
------------
The requirements can be installed using conda and pip:
```sh
conda install -c conda-forge nibabel scipy sympy
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/gyral_structure.git
```

Documentation
-------------
Documentation on the code can be found 
[here](https://users.fmrib.ox.ac.uk/~ndcn0236/doc/gyral_structure/).

